/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  basePath: process.env.NEXT_PUBLIC_BASEPATH || "",
  env: {
    apiBasePath: process.env.NEXT_PUBLIC_API || "",
  },
  trailingSlash: true,
};
