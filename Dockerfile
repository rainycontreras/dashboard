# Install dependencies only when needed
FROM node:16.13.0-alpine3.12 as runner

USER node

RUN mkdir -p /home/node/src/dashboard/
RUN mkdir -p /home/node/src/dashboard/node_modules
RUN mkdir -p /home/node/src/dashboard/out

WORKDIR /home/node/src/dashboard/

COPY --chown=node:node .yarn ./.yarn
COPY --chown=node:node .yarnrc.yml package.json yarn.lock ./

RUN yarn install --frozen-lockfile

COPY --chown=node:node . .

ARG BUILD_VERSION=local
RUN echo ${BUILD_VERSION} >> /home/node/src/dashboard/public/version.txt
