import { NextPage } from 'next';
import { CreditScoreFactorsLayout, ExplanationFooter } from 'src/components/forPages/CreditScoreFactors';
import { Box, Stack, Typography } from '@mui/material';
import CreditScoreFactorDetail from 'src/components/CreditScoreFactorDetail';
import { CreditScoreFactorDisplayType, ScoringFactorImpact } from 'src/utils/type';
import { useUserContext } from 'src/hooks';
import useTotalAccounts from 'src/hooks/totalAcounts';
import { formatNumber } from 'src/utils/number';
import { AppButton } from 'src/components';

const TOTAL_ACCOUNTS_TIERS = [
  {
    label: '0-5',
    min: 0,
    max: 6,
    color: '#E66262',
  },
  {
    label: '6-10',
    min: 6,
    max: 11,
    color: '#ECA336',
  },
  {
    label: '11-20',
    min: 11,
    max: 21,
    color: '#EBC346',
  },

  {
    label: '21+',
    min: 21,
    max: Number.MAX_SAFE_INTEGER,
    color: '#3ECE80',
  },
];

/**
 * Renders "Total Accounts" page
 * url: /credit-factors/total-accounts | /#/ta
 */
const AgeCreditHistory: NextPage = () => {
  const userContext = useUserContext();
  const totalAccounts = useTotalAccounts(userContext);

  return (
    <CreditScoreFactorsLayout>
      <Stack spacing={2.5}>
        <CreditScoreFactorDetail
          calculationDescriptionTitle="How is it Calculated?"
          calculationDescription="Your total accounts is found by adding your current open accounts and closed accounts"
          factorDescriptionTitle="What’s included in this number?"
          factorDescription="Lenders generally like seeing several (and varying) accounts on your report because it shows that other lenders have trusted you with credit."
          impact={ScoringFactorImpact.Medium}
          displayType={CreditScoreFactorDisplayType.ABSOLUTE}
          showColoredBar
          tiers={TOTAL_ACCOUNTS_TIERS}
          title="Total Accounts"
          valueDescription="Total accounts"
          value={totalAccounts?.total ?? 0}
          formattedValue={formatNumber(totalAccounts?.total)}
          valueDescriptionLabel1="Open accounts"
          valueDescriptionLabel2="Closed accounts"
          valueDescriptionLabelResult="Total accounts"
          valueDescriptionOperator1={formatNumber(totalAccounts?.open)}
          valueDescriptionOperator2={formatNumber(totalAccounts?.closed)}
          valueDescriptionResult={formatNumber(totalAccounts?.total)}
        >
          <ExplanationFooter>
            <Stack
              spacing={1}
              sx={{
                border: '1.5px solid',
                borderColor: 'app.blueBorder',
                borderRadius: '0.375rem',
                backgroundColor: 'app.lightBlue',
                marginTop: '1.5rem',
                padding: '1.25rem',
              }}
            >
              <Typography variant="label1">Account Details</Typography>
              <Typography variant="body">
                Check out a full list of your report accounts and recommendations specific to your gradelines
              </Typography>
              <Box sx={{ paddingTop: '0.5rem' }}>
                <AppButton endIcon="arrowRight">Accounts</AppButton>
              </Box>
            </Stack>
          </ExplanationFooter>
        </CreditScoreFactorDetail>
      </Stack>
    </CreditScoreFactorsLayout>
  );
};

export default AgeCreditHistory;
