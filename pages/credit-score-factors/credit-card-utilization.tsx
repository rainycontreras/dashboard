import { Stack, Typography } from '@mui/material';
import AccountDetail from 'src/components/AccountDetail';
import type { NextPage } from 'next';
import { useUserContext, useCreditCardUtilization } from 'src/hooks';
import CreditScoreFactorDetail from 'src/components/CreditScoreFactorDetail';
import { CreditScoreFactorDisplayType, ScoringFactorImpact } from 'src/utils/type';
import { formatMoney } from 'src/utils/number';
import { CreditCardAccountHeader, CreditScoreFactorsLayout } from 'src/components/forPages/CreditScoreFactors';

const CREDIT_CARD_UTILIZATION_TIERS = [
  {
    label: '0.9%',
    min: 0,
    max: 10.29,
    color: '#E66262', //TODO: Move colors out this file
  },
  {
    label: '10.29%',
    min: 10.29,
    max: 30.49,
    color: '#ECA336',
  },
  {
    label: '30.49%',
    min: 30.49,
    max: 50.74,
    color: '#EBC346',
  },

  {
    label: '50.74%',
    min: 50.74,
    max: 75,
    color: '#AFC949',
  },

  {
    label: '75%+',
    min: 75,
    max: 100,
    color: '#3ECE80',
  },
];

/**
 * Renders "Credit Card Utilization" page
 * url: /credit-factors/credit-card-utilization | /#/ccu
 */
const CreditCardUtilization: NextPage = () => {
  const userContext = useUserContext();
  const creditCardUtilization = useCreditCardUtilization(userContext);

  return (
    <CreditScoreFactorsLayout>
      <Stack spacing={2.5}>
        <CreditScoreFactorDetail
          calculationDescriptionTitle="How is it Calculated?"
          calculationDescription="Your credit card utilization is found by dividing your total credit card balance by your total credit card limit"
          displayType={creditCardUtilization?.displayType}
          factorDescriptionTitle="How does this affect my score?"
          factorDescription="Lenders generally like to see that you aren’t using too much of your available credit: The more credit you use, the harder it may to be pay back"
          impact={ScoringFactorImpact.High}
          tiers={CREDIT_CARD_UTILIZATION_TIERS}
          title="Credit Card Utilization"
          valueDescription="Available Credit"
          value={
            creditCardUtilization?.displayType === CreditScoreFactorDisplayType.PERCENTAGE
              ? creditCardUtilization?.utilization
              : creditCardUtilization?.balance
          }
          valueDescriptionLabel1="Credit Card Balance"
          valueDescriptionLabel2="Credit Card Limit"
          valueDescriptionLabelResult="Credit Card Utilization"
          valueDescriptionOperator1={formatMoney(creditCardUtilization?.creditLimit)}
          valueDescriptionOperator2={formatMoney(creditCardUtilization?.balance)}
          valueDescriptionResult={`${creditCardUtilization?.utilization?.toFixed(2) ?? ''}%`}
        />
        <Stack
          spacing={2.5}
          sx={{
            border: '1px solid',
            borderColor: 'app.border',
            borderRadius: '0.375rem',
            padding: '1.25rem 1.5rem ',
          }}
        >
          <Typography variant="label1">Accounts</Typography>
          {creditCardUtilization?.accounts &&
            creditCardUtilization.accounts.map((account) => (
              <AccountDetail header={CreditCardAccountHeader} key={account.number} account={account} />
            ))}
        </Stack>
      </Stack>
    </CreditScoreFactorsLayout>
  );
};

export default CreditCardUtilization;
