import { Stack, useMediaQuery, Theme, Typography } from '@mui/material';
import type { NextPage } from 'next';
import { useCreditReport, useCustomerProfile, useUserContext } from 'src/hooks';
import Layout from '@/components/layout/Simple/Layout';
import {
  Collections,
  MixBorrowing,
  PersonalInformation,
  PublicRecords,
} from '@/components/forPages/CreditScoreDetails';
import { AppIcon } from '@/components';
import HardInquiries from '@/components/forPages/CreditScoreDetails/HardInquiries';
import AccountBalances from '@/components/forPages/CreditScoreDetails/AccountBalances';
import EquifaxLogo from '@/components/icons/other/EquifaxLogo';

/**
 * Render CreditReport details page
 * Figma design: https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=0%3A1
 */
const CreditFactors: NextPage = () => {
  const smScreen = useMediaQuery((theme: Theme) => theme.breakpoints.up('sm'));
  const userContext = useUserContext();
  const customerProfile = useCustomerProfile(userContext);
  const creditReport = useCreditReport(userContext);
  return (
    <Layout profile={customerProfile}>
      <Stack
        spacing={3}
        sx={{
          paddingLeft: smScreen ? '1.875rem' : '1rem',
          paddingRight: smScreen ? '1.875rem' : '1rem',
          paddingTop: '1.5rem',
        }}
      >
        <Stack direction="row" justifyContent="space-between">
          <Typography variant="h2">Equifax Report Details</Typography>
          <EquifaxLogo/>
        </Stack>
        <PersonalInformation personalInformation={creditReport?.personalInformation} />
        <PublicRecords />
        <Collections />
        <HardInquiries enquiries={creditReport?.enquiries} />
        <MixBorrowing mixBorrowing={creditReport?.mixBorrowing} />
        <AccountBalances accounts={creditReport?.accounts} />
      </Stack>
    </Layout>
  );
};
export default CreditFactors;
