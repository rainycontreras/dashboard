import { NextPage } from 'next';
import { CreditCardAccountHeader, CreditScoreFactorsLayout } from 'src/components/forPages/CreditScoreFactors';
import { Stack, Typography } from '@mui/material';
import CreditScoreFactorDetail from 'src/components/CreditScoreFactorDetail';
import { ScoringFactorImpact } from 'src/utils/type';
import { useUserContext } from 'src/hooks';
import AccountDetail from 'src/components/AccountDetail';
import useAgeCreditHistory from 'src/hooks/ageCreditHistory';
import { formatDuration } from 'src/utils/date';

const AGE_HISTORY_TIERS = [
  {
    label: '<2 yrs',
    min: 0,
    max: 24,
    color: '#E66262',
  },
  {
    label: '2-4 yrs',
    min: 24,
    max: 60,
    color: '#ECA336',
  },
  {
    label: '5-6 yrs',
    min: 60,
    max: 84,
    color: '#EBC346',
  },

  {
    label: '7-8 yrs',
    min: 84,
    max: 108,
    color: '#AFC949',
  },

  {
    label: '9%+',
    min: 108,
    max: 600, //50 yrs
    color: '#3ECE80',
  },
];

/**
 * Renders "Age of Credit History" page
 * url: /credit-factors/age-of-credit-history | /#/ach
 */
const AgeOfCreditHistory: NextPage = () => {
  const userContext = useUserContext();
  const ageCreditHistory = useAgeCreditHistory(userContext);

  return (
    <CreditScoreFactorsLayout>
      <Stack spacing={2.5}>
        <CreditScoreFactorDetail
          calculationDescriptionTitle="How is it Calculated?"
          calculationDescription="We calculated your age of credit history by averaging the ages of your open credit accounts."
          factorDescriptionTitle="How does this affect my score?"
          factorDescription="The longer you responsibly manage credit, the more you demonstrate your creditworthiness to lenders."
          impact={ScoringFactorImpact.Medium}
          showColoredBar
          tiers={AGE_HISTORY_TIERS}
          title="Age of Credit History"
          valueDescription="Avg. age of credit history"
          value={ageCreditHistory?.ageCreditHistory}
          formattedValue={formatDuration(ageCreditHistory?.ageCreditHistory)}
        />
        <Stack
          spacing={2.5}
          sx={{
            border: '1px solid',
            borderColor: 'app.border',
            borderRadius: '0.375rem',
            padding: '1.25rem 1.5rem ',
          }}
        >
          <Typography variant="label1">Accounts</Typography>
          {ageCreditHistory?.accounts &&
            ageCreditHistory.accounts.map((account) => <AccountDetail header={CreditCardAccountHeader} key={account.number} account={account} />)}
        </Stack>
      </Stack>
    </CreditScoreFactorsLayout>
  );
};

export default AgeOfCreditHistory;
