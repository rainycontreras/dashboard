import { NextPage } from 'next';
import { CreditScoreFactorsLayout } from '@/components/forPages/CreditScoreFactors';
import { Stack } from '@mui/material';
import useAgeCreditHistory from '@/hooks/ageCreditHistory';
import { useUserContext } from '@/hooks';
import CreditScoreFactorDetail from '@/components/CreditScoreFactorDetail';
import { ScoringFactorImpact } from '@/utils/type';
import { APP_COLOR_STYLES } from '@/theme/AppColorStyles';

const NEGATIVE_STATUS_ACCOUNTS_TIERS = [
  {
    label: '4+',
    min: 4,
    max: 100, // Number.MAX_SAFE_INTEGER,
    color: APP_COLOR_STYLES.colorBar.veryBad,
  },
  {
    label: '2-3',
    min: 2,
    max: 3.9999999999999999,
    color: APP_COLOR_STYLES.colorBar.bad,
  },
  {
    label: '1',
    min: 1,
    max: 1.9999999999999999,
    color: APP_COLOR_STYLES.colorBar.neutral,
  },
  {
    label: '0',
    min: 0,
    max: 0.9999999999999999,
    color: APP_COLOR_STYLES.colorBar.veryGood,
  },
];

/**
 * Renders "Negative Status Accounts" page
 * url: /credit-factors/negative-status-accounts | /#/nsa
 */
const NegativeStatusAccountsPage: NextPage = () => {
  const userContext = useUserContext();
  const ageCreditHistory = useAgeCreditHistory(userContext);

  return (
    <CreditScoreFactorsLayout>
      <Stack spacing={2.5}>
        <CreditScoreFactorDetail
          calculationDescriptionTitle="How is it calculated?"
          calculationDescription="Your negative account status is found by adding your total open accounts in collections and any negative public records"
          factorDescriptionTitle="What’s included in this number?"
          factorDescription="Negative Status Accounts may include accounts in collections, bankruptcy, foreclosure or tax liens and can severely impact your score."
          impact={ScoringFactorImpact.High}
          showColoredBar
          tiers={NEGATIVE_STATUS_ACCOUNTS_TIERS}
          title="Negative Status Accounts"
          valueDescription="Negative status accounts"
          value={1.9}
          valueDescriptionLabel1="Open accounts"
          valueDescriptionLabel2="Negative public records"
          valueDescriptionLabelResult="Total derogatory marks"
          valueDescriptionOperator1={'2'}
          valueDescriptionOperator2={'0'}
          valueDescriptionResult={'2'}
        />
      </Stack>
    </CreditScoreFactorsLayout>
  );
};

export default NegativeStatusAccountsPage;
