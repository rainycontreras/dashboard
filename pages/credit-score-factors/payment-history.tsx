import { Stack, Typography } from '@mui/material';
import AccountDetail from 'src/components/AccountDetail';
import type { NextPage } from 'next';
import { useUserContext, useCreditCardUtilization } from 'src/hooks';
import CreditScoreFactorDetail from 'src/components/CreditScoreFactorDetail';
import { CreditScoreFactorDisplayType, ScoringFactorImpact } from 'src/utils/type';
import { CreditCardAccountHeader, CreditScoreFactorsLayout } from 'src/components/forPages/CreditScoreFactors';
import usePaymentHistory from 'src/hooks/paymentHistory';

const PAYMENT_HISTORY_TIERS = [
  {
    label: '< 97%',
    min: 0,
    max: 97.0,
    color: '#E66262', //TODO: Move colors out this file
  },
  {
    label: '97%',
    min: 97,
    max: 97,
    color: '#ECA336',
  },
  {
    label: '98%',
    min: 98,
    max: 98,
    color: '#EBC346',
  },

  {
    label: '99%',
    min: 99,
    max: 99,
    color: '#AFC949',
  },

  {
    label: '100%',
    min: 100,
    max: 100,
    color: '#3ECE80',
  },
];

/**
 * Payment History page
 */
const CreditCardUtilization: NextPage = () => {
  const userContext = useUserContext();
  const paymentHistory = usePaymentHistory(userContext);

  return (
    <CreditScoreFactorsLayout>
      <Stack spacing={2.5}>
        <CreditScoreFactorDetail
          calculationDescriptionTitle="How is it Calculated?"
          calculationDescription="Your percentage for on time payments is found by dividing your number of on-time payments by your total payments"
          displayType={CreditScoreFactorDisplayType.PERCENTAGE}
          factorDescriptionTitle="How does this affect my score?"
          factorDescription="Lenders like to see that you are paying your current obligations on time, as that helps them evaluate how likely you are to pay them back on time."
          impact={ScoringFactorImpact.High}
          tiers={PAYMENT_HISTORY_TIERS}
          title="Payment History"
          valueDescription="Payments on Time"
          value={paymentHistory?.paymentHistory}
          formattedValue={`${String(paymentHistory?.paymentHistory)}%`}
          valueDescriptionLabel1="On-Time Payments"
          valueDescriptionLabel2="Total Payments"
          valueDescriptionLabelResult="Percentage on Time"
          valueDescriptionOperator1={String(paymentHistory?.onTimePayments)}
          valueDescriptionOperator2={String(paymentHistory?.totalPayments)}
          valueDescriptionResult={`${paymentHistory?.paymentHistory ?? ''}%`}
        />
        <Stack
          spacing={2.5}
          sx={{
            border: '1px solid',
            borderColor: 'app.border',
            borderRadius: '0.375rem',
            padding: '1.25rem 1.5rem ',
          }}
        >
          <Typography variant="label1">Accounts</Typography>
          {paymentHistory?.accounts &&
            paymentHistory.accounts.map((account) => <AccountDetail key={account.number} header={CreditCardAccountHeader} account={account} />)}
        </Stack>
      </Stack>
    </CreditScoreFactorsLayout>
  );
};

export default CreditCardUtilization;
