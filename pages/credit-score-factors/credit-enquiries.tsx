import { NextPage } from 'next';
import { CreditScoreFactorsLayout } from 'src/components/forPages/CreditScoreFactors';
import { Stack, Typography } from '@mui/material';
import CreditScoreFactorDetail from 'src/components/CreditScoreFactorDetail';
import { CreditScoreFactorDisplayType, ScoringFactorImpact } from 'src/utils/type';
import { useUserContext } from 'src/hooks';
import useCreditEnquiries from 'src/hooks/creditEnquiries';
import Enquiry from 'src/components/forPages/CreditEnquiries';

const CREDIT_ENQUIRIES_TIERS = [
  {
    label: '0',
    min: 0,
    max: 1,
    color: '#3ECE80',
  },
  {
    label: '1-2',
    min: 1,
    max: 2,
    color: '#AFC949',
  },
  {
    label: '3-4',
    min: 3,
    max: 4,
    color: '#EBC346',
  },

  {
    label: '5-8',
    min: 5,
    max: 8,
    color: '#ECA336',
  },

  {
    label: '9+ yrs',
    min: 9,
    max: Number.MAX_SAFE_INTEGER,
    color: '#E66262',
  },
];

/**
 * Renders "Credit Enquiries" page
 * url: /credit-factors/credit-enquiries | /#/nce
 */
const CreditEnquiries: NextPage = () => {
  const userContext = useUserContext();
  const creditEnquiries = useCreditEnquiries(userContext);

  return (
    <CreditScoreFactorsLayout>
      <Stack spacing={2.5}>
        <CreditScoreFactorDetail
          calculationDescriptionTitle="How is it Calculated?"
          calculationDescription="These are your hard inquiries. Soft inquiries, like the kind used by My Money Karma, don’t impact your score and aren’t included here."
          coloredBarRtl
          factorDescriptionTitle="How does this affect my score?"
          factorDescription="Applying for a new line of credit will generally result in a hard inquiry. A lot of hard inquiries on your report may suggest that you're desperate for credit or aren't getting approved by other lenders."
          impact={ScoringFactorImpact.Medium}
          displayType={CreditScoreFactorDisplayType.ABSOLUTE}
          showColoredBar
          tiers={CREDIT_ENQUIRIES_TIERS}
          title="Credit Inquiries"
          valueDescription="Hard inquiries"
          value={creditEnquiries?.totalCreditEnquiries}
          formattedValue={creditEnquiries?.totalCreditEnquiries.toString() ?? ''}
        />
        <Stack
          spacing={2.5}
          sx={{
            border: '1px solid',
            borderColor: 'app.border',
            borderRadius: '0.375rem',
            padding: '1.25rem 1.5rem ',
          }}
        >
          <Typography variant="label1">Inquiries</Typography>
          {creditEnquiries?.enquiries &&
            creditEnquiries.enquiries.map((enquiries) => <Enquiry key={enquiries.id} enquiry={enquiries} />)}
        </Stack>
      </Stack>
    </CreditScoreFactorsLayout>
  );
};

export default CreditEnquiries;
