import { Box, Grid, Stack, Typography, useMediaQuery, Theme } from '@mui/material';
import ColloredBar from 'src/components/ColoredBar';
import { getScoreValueColor, getScoreValueMessage, SCORING_TIERS } from 'src/components/dashboard/ScoringUtils';
import Section from 'src/components/AppSection';
import type { NextPage } from 'next';
import { useEffect, useState } from 'react';
import { ScoringFactorImpact } from 'src/utils/type';
import { useCreditScoreFactors, useCustomerProfile, useUserContext } from 'src/hooks';
import Layout from '@/components/layout/Simple/Layout';
import CreditScoreFactor from '@/components/CreditScoreFactor';
import { formatDuration } from '@/utils/date';
import { formatInt, formatMoney, formatPercentage } from '@/utils/number';

/**
 * Credit Factors page
 */
const CreditFactors: NextPage = () => {
  const smScreen = useMediaQuery((theme: Theme) => theme.breakpoints.up('sm'));
  const userContext = useUserContext();
  const customerProfile = useCustomerProfile(userContext);
  const [scoreColor, setScoreColor] = useState<string | undefined>('primary.main');
  const [scoreMessage, setScoreMessage] = useState<string | undefined>(undefined);
  const {
    creditCardUtilization,
    paymentHistory,
    negativeStatusAccounts,
    ageCreditHistory,
    creditInquires,
    numberOfAccounts,
  } = useCreditScoreFactors(userContext);
  useEffect(() => {
    if (customerProfile?.creditScore) {
      setScoreMessage(getScoreValueMessage(customerProfile.creditScore));
      setScoreColor(getScoreValueColor(SCORING_TIERS, customerProfile.creditScore, 'primary.main'));
    }
  }, [customerProfile]);
  return (
    <Layout page="CREDIT_SCORE_FACTORS" profile={customerProfile}>
      <Stack
        sx={{
          paddingBottom: '2.5rem',
          paddingLeft: smScreen ? '1.875rem' : '1rem',
          paddingRight: smScreen ? '1.875rem' : '1rem',
          paddingTop: '1.5rem',
        }}
      >
        <Section title="Your Credit Score">
          <Stack sx={{ paddingLeft: '1.5rem', paddingBottom: '1.5rem', paddingRight: '1.5rem' }}>
            <Typography variant="h1" sx={{ fontSize: '3.5rem', lineHeight: '4.5rem', paddingTop: '1rem' }}>
              {customerProfile?.creditScore}
            </Typography>
            <Stack direction="row" spacing={1}>
              {customerProfile && (
                <>
                  <Typography variant="label2" sx={{ color: scoreColor }}>
                    {scoreMessage}
                  </Typography>
                  <Typography variant="text" sx={{ fontSize: '0.875rem', fontWeight: 500 }}>
                    -
                  </Typography>
                  <Typography variant="text" sx={{ fontSize: '0.875rem', fontWeight: 500 }}>
                    Updated: {customerProfile?.dateOfLatestCreditScore.toLocaleDateString('hi')}
                  </Typography>
                </>
              )}
            </Stack>
            <ColloredBar
              distribution="3.5fr 1.3fr 1fr 1.8fr" //TODO: refactor this based on tiers values
              tiers={SCORING_TIERS}
              displayLabel
              displayValueRange={false}
              height={6}
              value={customerProfile?.creditScore}
            />
          </Stack>
        </Section>
        <Box sx={{ paddingTop: '1.875rem', paddingBottom: '1.5rem' }}>
          <Typography variant="h1" sx={{ fontSize: '1rem' }}>
            What Can Affect My Score?
          </Typography>
        </Box>
        <Grid container spacing={3}>
          {creditCardUtilization && (
            <Grid item xs={12} sm={12} md={6} lg={4}>
              <CreditScoreFactor
                color="app.red"
                description="The amount of your total available credit that you have used."
                impact={ScoringFactorImpact.High}
                link="/credit-score-factors/credit-card-utilization"
                title="Credit Card Utilization"
                value={
                  creditCardUtilization.displayType === 'PERCENTAGE'
                    ? formatPercentage(creditCardUtilization.value)
                    : formatMoney(creditCardUtilization.value)
                }
              />
            </Grid>
          )}
          {paymentHistory && (
            <Grid item xs={12} sm={12} md={6} lg={4}>
              <CreditScoreFactor
                color="app.red"
                description="The percentage of payments that you have made on time."
                impact={ScoringFactorImpact.High}
                link="/credit-score-factors/payment-history"
                title="Payment History"
                value={formatPercentage(paymentHistory.value)}
              />
            </Grid>
          )}

          {negativeStatusAccounts && (
            <Grid item xs={12} sm={12} md={6} lg={4}>
              <CreditScoreFactor
                color="app.red"
                description="All accounts that the lender has reported as written off, suit filed or settled"
                impact={ScoringFactorImpact.High}
                link="/credit-score-factors/negative-status-accounts"
                title="Negative Status Accts."
                value={formatInt(negativeStatusAccounts.value, '0')}
              />
            </Grid>
          )}
          {ageCreditHistory && (
            <Grid item xs={12} sm={12} md={6} lg={4}>
              <CreditScoreFactor
                color="app.red"
                description="The average length of time your current loan and credit card accounts have been open."
                impact={ScoringFactorImpact.Medium}
                link="/credit-score-factors/age-of-credit-history"
                title="Age of Credit History"
                value={formatDuration(ageCreditHistory.value)}
              />
            </Grid>
          )}

          {creditInquires && (
            <Grid item xs={12} sm={12} md={6} lg={4}>
              <CreditScoreFactor
                color="app.red"
                description="The number of “hard pulls” on your credit report by banks and other lenders."
                impact={ScoringFactorImpact.Medium}
                link="/credit-score-factors/credit-enquiries"
                title="Credit Inquiries"
                value={formatInt(creditInquires.value, '0')}
              />
            </Grid>
          )}
          {numberOfAccounts && (
            <Grid item xs={12} sm={12} md={6} lg={4}>
              <CreditScoreFactor
                color="app.red"
                description="Total number of open and closed accounts on your credit report."
                impact={ScoringFactorImpact.Low}
                link="/credit-score-factors/total-accounts"
                title="Total Accounts"
                value={formatInt(numberOfAccounts.value, '0')}
              />
            </Grid>
          )}
        </Grid>
      </Stack>
    </Layout>
  );
};
export default CreditFactors;
