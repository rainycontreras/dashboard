import { NextPage } from 'next';
import { Box, Card, CardHeader, CardActions, CardContent, Grid, Typography } from '@mui/material';
import copyToClipboard from 'copy-to-clipboard';
import { AppButton, AppSection, AppIcon, AppIconButton } from 'src/components';
import { ICONS } from 'src/components/AppIcon/AppIcon';
import { SecondLevelTab, TopLevelTabs, TopLevelTab, SecondLevelTabs } from 'src/components/AppTabs';
import Layout from 'src/components/layout';

/**
 * "Live Demo" for reusable components and sections
 */
const DevPage: NextPage = () => {
  return (
    <Layout>
      <Grid container>
        <Grid item xs={12}>
          <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
            <TopLevelTabs sx={{ flexGrow: 1 }}>
              <TopLevelTab label="TopLevelTabs" to="dev" />
              <TopLevelTab label="CreditScore Factors" to="credit-score-factors" />
              {/* <TopTab label="CreditScore Details" to="credit-score-details" /> */}
            </TopLevelTabs>

            <AppButton variant="outline">Button After Tabs</AppButton>
          </Box>
        </Grid>

        <Grid item xs={12}>
          <br />
        </Grid>

        <Grid item xs={12}>
          <SecondLevelTabs areaLabel="different credit score factors by tabs">
            <SecondLevelTab label="SecondLevelTabs" to="dev" />
            <SecondLevelTab label="CreditCard Utilization" to="credit-card-utilization" />
            <SecondLevelTab label="Payment History" to="payment-history" />
            <SecondLevelTab label="Negative Status Accounts" to="negative-status-accounts" />
            <SecondLevelTab label="Age of Credit History" to="age-of-credit-history" />
            <SecondLevelTab label="Credit Enquiries" to="credit-enquiries" />
            <SecondLevelTab label="Total Accounts" to="total-accounts" />
          </SecondLevelTabs>
        </Grid>
        <Grid item xs={12}>
          <br />
        </Grid>
      </Grid>

      <Grid container spacing={1} sx={{ margin: 0.5 }}>
        <Grid item xs={12} md={6}>
          <AppSection title="Buttons">
            <AppButton label="Solid Large" size="large" variant="solid" />
            <AppButton variant="solid">Solid</AppButton>
            <AppButton text="Solid Small" size="small" variant="solid" />
            <AppButton endIcon="ArrowRight" startIcon="ArrowLeft" label="Icons" variant="solid" />
            <br />
            <AppButton label="Outline Large" size="large" variant="outline" />
            <AppButton variant="outline">Outline</AppButton>
            <AppButton text="Outline Small" size="small" variant="outline" />
            <AppButton endIcon="ArrowRight" startIcon="ArrowLeft" label="Icons" variant="outline" />
            <br />
            <AppButton label="Ghost Large" size="large" variant="ghost" />
            <AppButton text="Ghost Medium" size="medium" variant="ghost" />
            <AppButton variant="ghost">Ghost</AppButton>
            <AppButton endIcon="ArrowRight" startIcon="ArrowLeft" label="Icons" variant="ghost" />
          </AppSection>
        </Grid>

        <Grid item xs={12} md={6}>
          <AppSection title="Icons and IconButtons">
            <Typography variant="label2" sx={{ marginLeft: 3 }}>
              Click icon to copy JSX code
            </Typography>
            <br />
            {Object.keys(ICONS).map((icon) => (
              <AppIconButton
                key={icon}
                icon={icon}
                title={icon}
                onClick={() => {
                  copyToClipboard(`<AppIcon icon="${icon}" />`);
                }}
              />
            ))}
          </AppSection>
        </Grid>
      </Grid>
    </Layout>
  );
};

export default DevPage;
