import '../styles/globals.css';
import type { AppProps } from 'next/app';
import Head from 'next/head';
import ErrorBoundary from 'src/components/ErrorBoundary';
import { AppThemeProvider } from 'src/theme';

function DashboardApp({ Component, pageProps }: AppProps) {
  return (
    <ErrorBoundary name="Application">
      <Head>
        <title>Dashboard - MyMoneyKarma</title>
      </Head>
      <AppThemeProvider>
        <Component {...pageProps} />
      </AppThemeProvider>
    </ErrorBoundary>
  );
}

export default DashboardApp;
