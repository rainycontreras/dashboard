import { useRouter } from 'next/router';
import type { NextPage } from 'next';
import { useState } from 'react';
import { login } from 'src/api';
import { sessionStorageSet } from '@/utils/sessionStorage';

const Index: NextPage = () => {
  const router = useRouter();
  const [mobileNumber, setMobileNumber] = useState('');
  const [pinCode, setPinCode] = useState('');

  return (
    <>
      <form
        onSubmit={async (event) => {
          event.preventDefault();
          try {
            const response = await login(mobileNumber, pinCode);
            if (response?.sessionToken) {
              sessionStorageSet('session_token', response.sessionToken);
              sessionStorageSet('user_id', response.customerProfile.customerID);
            }
            router.push('/dashboard');
          } catch (error) {}
        }}
      >
        <input
          type="text"
          value={mobileNumber}
          onChange={(event) => {
            setMobileNumber(event.target.value);
          }}
        ></input>
        <input
          type="text"
          value={pinCode}
          onChange={(event) => {
            setPinCode(event.target.value);
          }}
        ></input>
        <button type="submit">Log in</button>
      </form>
    </>
  );
};
export default Index;
