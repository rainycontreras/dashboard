import Layout from "@/components/layout/Simple/Layout";
import {useCustomerProfile, useUserContext} from "@/hooks";
import {Box, ClickAwayListener, Divider, Grid, Stack, Theme, Typography, useMediaQuery} from "@mui/material";
import {AppButton, AppIconButton, AppLink} from "@/components";
import {APP_COLOR_STYLES} from "@/theme/AppColorStyles";
import ReferEarnSection from "@/components/dashboard/ReferEarnSection";
import {useCallback, useState} from "react";
import copyToClipboard from "copy-to-clipboard";
import {NextPage} from "next";
import ReferalCodeSharer from '@/components/ReferalCodeSharer';
import AppIcon from '@/components/AppIcon';


const ReferAndEarn: NextPage = () => {

    const userContext = useUserContext();
    const customerProfile = useCustomerProfile(userContext);

    return (
        <Layout page={'REFER_AND_EARN'} profile={customerProfile}>

            <Stack direction='row' justifyContent='space-between' alignItems='center' px={4} py={2}>
                <Typography variant='h2'>Refer & Earn</Typography>
                <AppButton variant='outline'>How it works ?</AppButton>
            </Stack>
            <Divider/>

            <Stack sx={{width: '100%'}} alignItems='center' p={4}>
                <Box sx={{
                    display: 'flex',
                    justifyContent: 'center',
                    'img': {
                        objectFit: 'contain',
                        height: '10.875rem',
                    }
                }}>
                    <img src={'/images/coinsStacked@2x.png'}/>
                </Box>

                <Typography variant='h1' sx={{textAlign: 'center', paddingTop: '2.5rem', lineHeight: '1.938rem'}}>Rs
                    80,000
                    cashback for helping your friends <br/> save on property loans</Typography>
                <Typography variant='text'
                            sx={{
                                fontSize: '1.25rem',
                                color: APP_COLOR_STYLES.grey,
                                marginTop: '1.75rem', textAlign: 'center', lineHeight: '1.75rem'
                            }} component='p'>
                    Refer your friends for the best deals on property loans. Both of you <br/> earn when loan is
                    disbursed.</Typography>


                <Box marginTop='2.75rem'>
                    <ReferalCodeSharer referralCode={customerProfile?.referralCode}/>
                </Box>


                <Stack direction={{xs: 'column', md: 'row'}}
                       sx={{
                           backgroundColor: APP_COLOR_STYLES.lightBlue,
                           padding: '1.5rem 1rem',
                           alignItems: 'center',
                           border: '0.063rem solid #D5E0F5',
                           borderRadius: '0.375rem',
                           maxWidth: '33.75rem',
                           width: '100%',
                           marginTop: '3.125rem'
                       }}>
                    <Box sx={{'& svg': {flexShrink: '0'}}}>
                        <AppIcon name='chart'/>
                    </Box>
                    <Typography variant='h2' marginLeft='1.375rem'>Rs. 0 earned of potential RS. 80,000</Typography>

                    <AppButton endIcon='arrowRight'
                               sx={{padding: '0.875rem 0.8rem', marginLeft: '2rem', flexShrink: '0'}}>

                        <Stack component='span' direction='row' alignItems='center'>
                            <Box sx={{
                                backgroundColor: 'white',
                                borderRadius: '50%',
                                width: '1rem',
                                height: '1rem',
                                display: 'flex',
                                marginRight: '0.5rem',

                            }} component='span'>
                                <AppIcon name='rupee' color={APP_COLOR_STYLES.primary}/>
                            </Box>
                            <Typography component='span' variant='h2' color='white' marginRight='1rem' noWrap>My
                                Earnings</Typography>
                        </Stack>
                    </AppButton>
                </Stack>
            </Stack>

        </Layout>
    );
}

export default ReferAndEarn;