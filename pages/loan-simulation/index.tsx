import { Box, Grid, Stack, Typography, useMediaQuery, Theme,} from '@mui/material';
import type { NextPage } from 'next';
import { useCustomerProfile, useCustomerSalary, useLoanSimulation, useUserContext } from 'src/hooks';
import Layout from '@/components/layout/Simple/Layout';
import Shield from 'public/images/Shield_perspective_matte 1.png';
import QuestionMark from 'public/images/question_mark.png';
import React, { useEffect,  useState } from 'react';
import { LoanExtraContribution, LoanSaving, SimulationCard } from '@/components/forPages/LoanSimulation';

import { AccountDetails, Simulation } from '@/utils/type';
import { AppButton } from '@/components';


/**
 * Render MMK Loan Simulation
 * https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=13%3A9
 */
const LoanSimulation: NextPage = () => {
  const lgScreen = useMediaQuery((theme: Theme) => theme.breakpoints.up('lg'));
  const userContext = useUserContext();
  const customerProfile = useCustomerProfile(userContext);
  const [loans,selectedLoan,setSelectedLoan,loanSimulations]= useLoanSimulation(userContext);
  const salary = useCustomerSalary(userContext);

  return (
    <Layout profile={customerProfile}>
      <Stack>
        <Stack
          alignItems="center"
          justifyContent="space-between"
          direction="row"
          sx={{
            paddingBottom: '1rem',
            paddingLeft: lgScreen ? '1.875rem' : '1rem',
            paddingRight: lgScreen ? '1.875rem' : '1rem',
            paddingTop: '1rem',
          }}
        >
          {
            //TODO: Make a layout for this kind of pages: Title + divider?
          }
          <Typography variant="text" sx={{ fontWeight: 600 }}>
            Interest Saving
          </Typography>
        </Stack>
        <Box
          sx={{
            borderBottom: '1px solid',
            borderBottomColor: 'app.border',
          }}
        />
        <Stack
          sx={{
            paddingBottom: '2.5rem',
            paddingLeft: lgScreen ? '1.875rem' : '1rem',
            paddingRight: lgScreen ? '1.875rem' : '1rem',
            paddingTop: '1.5rem',
          }}
          spacing={2.5}
        >
          <Typography variant="text" sx={{ fontWeight: '600' }}>
            Ready for Zero Debt with Interest Savings
          </Typography>
          <Stack>
            <Grid alignItems="stretch" container spacing={2}>
              <Grid item xs={12} lg={8}>
                <LoanSaving loan={selectedLoan} salary={salary} loans={loans} onChange={setSelectedLoan}/>
              </Grid>
              <Grid item xs={12} lg={4}>
                <Stack
                  alignItems="center"
                  sx={{
                    backgroundColor: 'app.lightBlue',
                    padding: '1.25rem 1.5rem 1.25rem 1.5rem',
                    border: '1.5px solid',
                    borderColor: 'app.blueBorder',
                    borderRadius: '.375rem',
                    height: '100%',
                  }}
                  spacing={1}
                >
                  <Box>
                    <img src={Shield.src} />
                  </Box>
                  <Stack alignItems="center" spacing={0.5}>
                    <Typography align="center" color="secondary" variant="text" sx={{ fontWeight: '600' }}>
                      We want you to succeed!
                    </Typography>
                    <Typography align="center" color="secondary" variant="tiny" sx={{ opacity: '0.6' }}>
                      We create these tools to help you make better financial decisions. We want to see more money in
                      your wallet, not ours.
                    </Typography>
                  </Stack>
                </Stack>
              </Grid>
            </Grid>
          </Stack>
          <Stack>
            <Typography
              variant="text"
              sx={{
                fontWeight: 600,
                fontSize: '1rem',
              }}
            >
              The more you pay now, the more you can save on interest and pay off your loan quicker
            </Typography>
            <Typography
              variant="body"
              sx={{
                opacity: 0.6,
                fontSize: '0.875rem',
              }}
            >
              We&apos;ve identified a couple of attractive options that might be for you
            </Typography>
          </Stack>
          <Box>
            <Grid container spacing={2}>
              {
                loanSimulations?.map((simulation:Simulation)=><Grid key={simulation.extraPaid} item xs={12} lg={4}><SimulationCard simulation={simulation}/></Grid>)
              }
            </Grid>
          </Box>
          <Stack>
            <Typography
              variant="text"
              sx={{
                fontWeight: 600,
                fontSize: '1rem',
              }}
            >
              Any extra contributions you can make now will help
            </Typography>
            <Typography
              variant="body"
              sx={{
                opacity: 0.6,
                fontSize: '0.875rem',
              }}
            >
              Customize your monthly payment to understand the impact of your additional contributions on your interest
              savings
            </Typography>
          </Stack>
          <Stack>
          <Grid alignItems="stretch" container spacing={2}>
            <Grid item xs={12} lg={8}>
              <LoanExtraContribution loan={selectedLoan} />
            </Grid>
            <Grid item xs={12} lg={4}>
                <Stack
                  alignItems="center"
                  sx={{
                    backgroundColor: 'app.ghostWhite',
                    padding: '1.25rem 1.5rem 1.25rem 1.5rem',
                    border: '1.5px solid',
                    borderColor: 'app.border',
                    borderRadius: '0.375rem',
                    height: '100%',
                  }}
                  spacing={1}
                >
                  <Box>
                    <img src={QuestionMark.src} />
                  </Box>
                  <Stack alignItems="center" spacing={1}>
                    <Typography align="center" color="secondary" variant="text" sx={{ fontWeight: '600',paddingTop:'0.75rem' }}>
                    Do you have bonus or spare cash that you want to pay your loan with?
                    </Typography>
                    <Typography align="center" color="secondary" variant="tiny" sx={{ opacity: '0.6',paddingTop:'0.5rem' }}>
                    Use our tool to find out when you can be debt free!
                    </Typography>
                    <Box sx={{paddingTop:'1.25rem'}}>
                    <AppButton sx={{borderRadius:'0.375rem'}}>
                      <Typography color="white" variant="label2" sx={{padding:'2rem'}}>
                      Let&apos;s Do It
                      </Typography>
                    </AppButton>
                    </Box>
                    
                  </Stack>
                </Stack>
             </Grid>
            </Grid>
          </Stack>
        </Stack>
      </Stack>
    </Layout>
  );
};
export default LoanSimulation;
