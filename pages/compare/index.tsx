import { Box, Grid, Stack, Typography, useMediaQuery, Theme } from '@mui/material';
import type { NextPage } from 'next';
import { useCreditReportCompare, useCustomerProfile, useUserContext } from 'src/hooks';
import Layout from '@/components/layout/Simple/Layout';
import { CompareScoringItem, CreditScoreFactorComparisonTable } from '@/components/forPages/Compare';
import { formatInt } from '@/utils/number';
import { AppButton, AppIcon } from '@/components';
import { useTheme, PaletteOptions } from '@mui/material/styles';
import { useMemo } from 'react';

/**
 * Render MMK Score Compare
 * https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=0%3A1
 */
const Compare: NextPage = () => {
  const lgScreen = useMediaQuery((theme: Theme) => theme.breakpoints.up('lg'));
  const userContext = useUserContext();
  const customerProfile = useCustomerProfile(userContext);
  const reports = useCreditReportCompare(userContext);
  const theme = useTheme();

  const scoreDiff = (reports?.newReport?.score ?? 0) - (reports?.oldReport?.score ?? 0);

  const indicatorColor = useMemo(() => {
    const palette = (theme.palette as PaletteOptions)?.app;
    if (palette) {
      if (scoreDiff > 0) {
        return palette.green;
      } else if (scoreDiff < 0) {
        return palette.red;
      }
    }
    return 'black';
  }, [theme, scoreDiff]);

  return (
    <Layout profile={customerProfile}>
      <Stack>
        <Stack
          alignItems="center"
          justifyContent="space-between"
          direction="row"
          sx={{
            paddingBottom: '1rem',
            paddingLeft: lgScreen ? '1.875rem' : '1rem',
            paddingRight: lgScreen ? '1.875rem' : '1rem',
            paddingTop: '1rem',
          }}
        >
          <Typography variant="text" sx={{ fontWeight: 600 }}>
            Compare
          </Typography>
          <AppButton
            variant="outline"
            size="small"
            text={
              (reports?.daysToUpdate ?? 0) < 0
                ? 'You Can Update Now'
                : `Next Update in ${reports?.daysToUpdate ?? 30} Day(s)`
            }
          />
        </Stack>
        <Box
          sx={{
            borderBottom: '1px solid',
            borderBottomColor: 'app.border',
          }}
        />
        <Stack
          sx={{
            paddingBottom: '2.5rem',
            paddingLeft: lgScreen ? '1.875rem' : '1rem',
            paddingRight: lgScreen ? '1.875rem' : '1rem',
            paddingTop: '1.5rem',
          }}
          spacing={2.5}
        >
          {
            //TODO:Change to VariantTypography control
          }
          <Typography variant="text" sx={{ fontWeight: '600' }}>
            What Has Changed In Your Credit Score
          </Typography>
          <Box sx={{ position: 'relative' }}>
            <Grid container spacing={1.25}>
              <Grid item xs={12} lg={6}>
                <CompareScoringItem
                  label="Old Report"
                  color="secondary"
                  position="left"
                  score={reports?.oldReport?.score}
                  scoreDate={reports?.oldReport?.scoreDate}
                />
              </Grid>
              <Grid item xs={12} lg={6}>
                <CompareScoringItem
                  label="New Report"
                  color="primary"
                  position="right"
                  score={reports?.newReport?.score}
                  scoreDate={reports?.newReport?.scoreDate}
                />
              </Grid>
            </Grid>

            <Stack
              alignItems="center"
              justifyContent="center"
              sx={{ position: 'absolute', top: 0, right: 0, left: 0, bottom: 0 }}
            >
              <Stack alignItems="center" justifyContent="center" sx={{ zIndex: 10 }}>
                {scoreDiff === 0 && (
                  <Typography textAlign="center" sx={{ fontWeight: '600', fontSize: '1.125rem', color: 'app.grey' }}>
                    No
                    <br />
                    Change
                  </Typography>
                )}
                {scoreDiff !== 0 && (
                  <>
                    <Stack spacing={0.5} alignItems="center" justifyContent="center" direction="row">
                      {scoreDiff < 0 && <AppIcon color={indicatorColor} name="backward" />}
                      <Typography
                        sx={{ fontWeight: '600', fontSize: '2rem', color: scoreDiff > 0 ? 'app.green' : 'app.red' }}
                      >
                        {scoreDiff > 0 ? '+' : ''}
                        {formatInt(scoreDiff)}
                      </Typography>
                      {scoreDiff > 0 && <AppIcon color={indicatorColor} name="forward" />}
                    </Stack>
                    <Typography variant="tiny" sx={{ opacity: 0.6 }}>
                      Points
                    </Typography>
                  </>
                )}
              </Stack>
            </Stack>
          </Box>
          <Stack sx={{ border: '1px solid', borderColor: 'app.border', borderRadius: '0.375rem' }}>
            <Box sx={{ padding: '1rem 1.25rem' }}>
              <Typography variant="label2">Credit Score Factors</Typography>
            </Box>
            <CreditScoreFactorComparisonTable reports={reports} />
          </Stack>
        </Stack>
      </Stack>
    </Layout>
  );
};
export default Compare;
