import ReferEarnSection from 'src/components/dashboard/ReferEarnSection';
import ScoringSection from 'src/components/dashboard/ScoringSection';
import Layout from 'src/components/layout/Simple/Layout';
import { Box, Grid, Stack, Typography } from '@mui/material';
import type { NextPage } from 'next';
import { useCustomerProfile, useUserContext } from 'src/hooks';

/**
 * Dashboard home page
 */
const Home: NextPage = () => {
  const userContext = useUserContext();
  const customerProfile = useCustomerProfile(userContext);
  return (
    <Layout page="DASHBOARD" profile={customerProfile}>
      <Stack sx={{ padding: '1.875rem' }} spacing={3}>
        <Box>
          <Typography variant="h1">Dashboard</Typography>
          <Typography variant="text">Good Morning, {customerProfile?.enteredFirstName}! 🌤</Typography>
        </Box>
        <Stack>
          <Grid container>
            <Grid item xs={12} sm={12} md={12} lg={8}>
              <ScoringSection
                score={customerProfile?.creditScore}
                updatedDate={customerProfile?.dateOfLatestCreditScore}
              />
            </Grid>
          </Grid>
        </Stack>
        <Stack>
          <Grid container>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <ReferEarnSection referralCode={customerProfile?.referralCode} />
            </Grid>
          </Grid>
        </Stack>
      </Stack>
    </Layout>
  );
};

export default Home;
