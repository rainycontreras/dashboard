import { useEffect, useState } from 'react';
import { addDays, differenceInDays } from 'date-fns';
import {
  calculateAgeCreditHistoryComparisonStatus,
  calculateCreditEnquiriesComparisonStatus,
  calculatePaymentHistoryComparisonStatus,
  CREDITREPORT_CURRENT,
  CREDITREPORT_LAST,
  fetchCreditScoreFactors,
} from 'src/utils/account';

import { UserContext, CreditReportCompare } from 'src/utils/type';

/**
 * Retrieve the customer's credit reports comparison
 * @param {UserContext} userContext - Customer's session data
 * @returns {CreditReportCompare | undefined} - Customer's credit reports LAST & CURRENT comparison
 */
export default function useCreditReportCompare(userContext?: UserContext): CreditReportCompare | undefined {
  const [creditReport, setCreditReport] = useState<CreditReportCompare | undefined>(undefined);
  useEffect(() => {
    async function load() {
      if (userContext?.sessionToken) {
        const reports = await Promise.all([
          fetchCreditScoreFactors(userContext, CREDITREPORT_LAST, true),
          fetchCreditScoreFactors(userContext, CREDITREPORT_CURRENT, true),
        ]);

        if (reports) {
          const oldReport = reports.length >= 1 ? reports[0] : undefined;
          const newReport = reports.length >= 2 ? reports[1] : undefined;

          const ageCreditHistoryVariation =
            (oldReport?.ageCreditHistory?.value ?? 0) - (oldReport?.ageCreditHistory?.value ?? 0);
          const ageCreditHistoryComparisonStatus = calculateAgeCreditHistoryComparisonStatus(ageCreditHistoryVariation);

          const creditEnquiriesVariation =
            (oldReport?.creditInquires?.value ?? 0) - (oldReport?.creditInquires?.value ?? 0);
          const creditEnquiriesComparisonStatus = calculateCreditEnquiriesComparisonStatus(creditEnquiriesVariation);

          const paymentHistoryVariation =
            (oldReport?.paymentHistory?.value ?? 0) - (oldReport?.paymentHistory?.value ?? 0);
          const paymentHistoryComparisonStatus = calculatePaymentHistoryComparisonStatus(paymentHistoryVariation);

          const numberOfAccountsVariation =
            (oldReport?.numberOfAccounts?.value ?? 0) - (oldReport?.numberOfAccounts?.value ?? 0);
          const numberOfAccountsComparisonStatus = calculatePaymentHistoryComparisonStatus(numberOfAccountsVariation);

          const now = new Date();
          const nextUpdate = addDays(newReport?.scoreDate ?? new Date(), 30);
          const daysToUpdate = differenceInDays(nextUpdate, now);
          setCreditReport({
            oldReport,
            newReport,
            daysToUpdate,
            status: {
              ageCreditHistory: ageCreditHistoryComparisonStatus,
              creditInquires: creditEnquiriesComparisonStatus,
              paymentHistory: paymentHistoryComparisonStatus,
              numberOfAccounts: numberOfAccountsComparisonStatus,
            },
          });
        }
      }
    }
    load();
  }, [userContext]);

  return creditReport;
}
