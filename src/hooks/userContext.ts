import { useEffect, useState } from 'react';
import { redirectToAuth } from 'src/api/utils';
import { sessionStorageGet } from 'src/utils/sessionStorage';
import { UserContext } from 'src/utils/type';

/**
 * Retrieve the customer's session data
 * @returns {UserContext} - Customer's session data
 */
export default function useUserContext(): UserContext {
  const [response, setResponse] = useState<UserContext>({});
  useEffect(() => {
    const sessionToken = sessionStorageGet('session_token');
    const userId = sessionStorageGet('user_id');
    if (sessionToken !== null) {
      setResponse({ sessionToken, userId });
    } else {
      redirectToAuth();
    }
  }, []);

  return response;
}
