import { differenceInMonths } from 'date-fns';
import { useEffect, useState } from 'react';
import { ACCOUNT_OPEN, parseAccountDetail } from 'src/utils/account';
import { fetchCreditScoreHistory } from 'src/api/customer';
import { UserContext, AgeCreditHistory } from 'src/utils/type';

/**
 * Retrieve the customer's age credit history
 * @param {UserContext} userContext - Customer's session data
 * @returns {AgeCreditHistory | undefined} - Customer's Age Credit History factor information
 */
export default function useAgeCreditHistory(userContext?: UserContext): AgeCreditHistory | undefined {
  const [ageCreditHistory, setAgeCreditHistory] = useState<AgeCreditHistory | undefined>(undefined);
  useEffect(() => {
    async function load() {
      if (userContext?.sessionToken) {
        const response = await fetchCreditScoreHistory(userContext);
        if (response?.ReportData?.AccountDetails?.Account) {
          const accounts = response.ReportData.AccountDetails.Account;

          const accountsDetail = accounts.filter(ACCOUNT_OPEN).map((account) => parseAccountDetail(account, true));
          const valueTodayDate = new Date();

          const age = accountsDetail.reduce((totalAge, account) => {
            return totalAge + differenceInMonths(valueTodayDate, account.openedDate);
          }, 0);

          const average = accountsDetail.length > 0 ? age / accountsDetail.length : 0;

          setAgeCreditHistory({
            ageCreditHistory: Math.round(average),
            accounts: accountsDetail,
          });
        }
      }
    }
    load();
  }, [userContext]);

  return ageCreditHistory;
}
