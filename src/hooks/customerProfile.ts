import { useEffect, useState } from 'react';
import { fetchCustomerInfo } from 'src/api/customer';
import { CustomerProfile, UserContext } from 'src/utils/type';

/**
 * Retrieve the customer's profile data
 * @param {UserContext} userContext - Customer's session data
 * @returns {CustomerProfile} - Customer's profile
 */
export default function useCustomerProfile(userContext?: UserContext): CustomerProfile | undefined {
  const [profile, setProfile] = useState<CustomerProfile | undefined>(undefined);
  useEffect(() => {
    // Fetch profile info
    async function load() {
      if (userContext?.sessionToken) {
        let response = await fetchCustomerInfo(userContext);
        if (response) {
          setProfile({
            customerID: response.customerID,
            enteredFirstName: response.enteredFirstName,
            enteredLastName: response.enteredLastName,
            enteredPanNumber: response.enteredPanNumber,
            creditScore: response.creditScore,
            emailAddress: response.emailAddress,
            personalLoanEligibility: response.personalLoanEligibility,
            dateOfLatestCreditScore: new Date(response.dateOfLatestCreditScore),
            referralCode: response.referralCode,
          });
        }
      }
    }
    load();
  }, [userContext]);

  return profile;
}
