import { useEffect, useState } from 'react';
import { UserContext, CustomerSalary } from 'src/utils/type';
import { apiSalaryAnnual } from '@/api/salary';
import { apiLoanLastPayment } from '@/api/loan';

/**
 * Retrieves customer's salary
 * @param {UserContext} userContext - Customer's session data
 * @returns {LoanSimulation | undefined} - Customer's credit reports LAST & CURRENT comparison
 */
export default function useCustomerSalary(userContext?: UserContext): number | undefined {
  const [customerSalary, setCustomerSalary] = useState<number | undefined>(undefined);
  useEffect(() => {
    async function load() {
      if (userContext?.sessionToken) {
        let salary = 0;
        const salaryAnnual = await apiSalaryAnnual(userContext);
        if (salaryAnnual && salaryAnnual.salaryAmount) {
          salary = salaryAnnual.salaryAmount / 12;
        } else {
          const lastPayment = await apiLoanLastPayment(userContext);
          if (lastPayment) {
            const payments =
              (lastPayment.otherloanLastPayment ?? 0) +
              (lastPayment.personalLastPayment ?? 0) +
              (lastPayment.homeloanLastPayment ?? 0) +
              (lastPayment.autoloanLastPayment ?? 0);
            salary = payments * 2;
          }
        }
        setCustomerSalary(salary);
      }
    }
    load();
  }, [userContext]);

  return customerSalary;
}
