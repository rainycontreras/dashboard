import { useEffect, useState } from 'react';
import { ACCOUNT_OPEN, parseAccountDetail, parseIntermediatePaymentHistory } from 'src/utils/account';
import { AccountDetails, UserContext, PaymentHistory } from 'src/utils/type';
import { fetchCreditScoreHistory } from 'src/api/customer';

/**
 * Retrieve the customer's credit card utilization and accounts
 * @param {UserContext} userContext - Customer's session data
 * @returns {PaymentHistory | undefined} - Customer's Payment History factor information
 */
export default function usePaymentHistory(userContext?: UserContext): PaymentHistory | undefined {
  const [paymentHistory, setPaymentHistory] = useState<PaymentHistory | undefined>(undefined);
  useEffect(() => {
    async function load() {
      if (userContext?.sessionToken) {
        const response = await fetchCreditScoreHistory(userContext);

        if (response?.ReportData?.AccountDetails?.Account) {
          const accounts = response.ReportData.AccountDetails.Account;
          const accountsOpened = accounts.filter(ACCOUNT_OPEN);

          const accountsDetail: AccountDetails[] = [];
          const { onTimePayments, totalPayments } = parseIntermediatePaymentHistory(accounts);
          let paymentHistory = 0;

          for (const account of accountsOpened) {
            const accountDetail = parseAccountDetail(account);

            accountsDetail.push(accountDetail);
          }

          if (totalPayments) {
            paymentHistory = (onTimePayments / totalPayments) * 100;
          }

          setPaymentHistory({
            onTimePayments,
            totalPayments,
            paymentHistory,
            accounts: accountsDetail,
          });
        }
      }
    }
    load();
  }, [userContext]);

  return paymentHistory;
}
