import { useEffect, useState } from 'react';
import { ACCOUNT_OPEN } from 'src/utils/account';
import { fetchCreditScoreHistory } from 'src/api/customer';
import { UserContext, TotalAccounts } from 'src/utils/type';

/**
 * Retrieve the customer's total accounts
 * @param {UserContext} userContext - Customer's session data
 * @returns {AgeCreditHistory | undefined} - Customer's total accounts
 */
export default function useTotalAccounts(userContext?: UserContext): TotalAccounts | undefined {
  const [totalAccounts, setTotalAccounts] = useState<TotalAccounts | undefined>(undefined);
  useEffect(() => {
    async function load() {
      if (userContext?.sessionToken) {
        const response = await fetchCreditScoreHistory(userContext);
        if (response?.ReportData?.AccountDetails?.Account) {
          const accounts = response.ReportData.AccountDetails.Account;
          const total = response.ReportData.AccountDetails.Account.length;
          const openAccounts = accounts.filter(ACCOUNT_OPEN);

          setTotalAccounts({
            open: openAccounts.length,
            closed: total - openAccounts.length,
            total,
          });
        }
      }
    }
    load();
  }, [userContext]);

  return totalAccounts;
}
