import { useEffect, useState } from 'react';
import { CreditScoreFactor, UserContext } from 'src/utils/type';
import { CREDITREPORT_CURRENT, fetchCreditScoreFactors } from 'src/utils/account';

/**
 * Retrieve the customer's creditscore factors
 * @param {UserContext} userContext - Customer's session data
 * @param {number} reportIndex - Credit report's index to look for
 * @returns {} - Credit score factors
 */
export default function useCreditScoreFactors(userContext?: UserContext, reportIndex: number = CREDITREPORT_CURRENT) {
  const [creditCardUtilization, setCreditCardUtilization] = useState<CreditScoreFactor | undefined>(undefined);
  const [paymentHistory, setPaymentHistory] = useState<CreditScoreFactor | undefined>(undefined);
  const [negativeStatusAccounts, setNegativeStatusAccounts] = useState<CreditScoreFactor | undefined>(undefined);
  const [ageCreditHistory, setAgeCreditHistory] = useState<CreditScoreFactor | undefined>(undefined);
  const [creditInquires, setCreditInquires] = useState<CreditScoreFactor | undefined>(undefined);
  const [numberOfAccounts, setNumberOfAccounts] = useState<CreditScoreFactor | undefined>(undefined);
  useEffect(() => {
    async function load() {
      if (userContext?.sessionToken) {
        const factors = await fetchCreditScoreFactors(userContext, reportIndex);
        setCreditCardUtilization(factors.creditCardUtilization);
        setPaymentHistory(factors.paymentHistory);
        setNegativeStatusAccounts(factors.negativeStatusAccounts);
        setAgeCreditHistory(factors.ageCreditHistory);
        setCreditInquires(factors.creditInquires);
        setNumberOfAccounts(factors.numberOfAccounts);
      }
    }
    load();
  }, [userContext]);

  return {
    creditCardUtilization,
    paymentHistory,
    negativeStatusAccounts,
    ageCreditHistory,
    creditInquires,
    numberOfAccounts,
  };
}
