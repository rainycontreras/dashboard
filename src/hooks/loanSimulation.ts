import { useEffect, useState } from 'react';
import { CREDITREPORT_CURRENT, parseAccountDetail } from 'src/utils/account';
import { UserContext, AccountDetails, Simulation } from 'src/utils/type';
import { fetchCreditScoreHistory } from '@/api/customer';
import { calculateLoanSimulations, fillEmiTerm } from '@/utils/loan';

/**
 * Generate multiple loan simulation
 * @param {UserContext} userContext - Customer's session data
 * @returns {[ AccountDetails[] | undefined, AccountDetails | undefined,(loan: AccountDetails) => void,Simulation[]]} - Customer's credit reports LAST & CURRENT comparison
 */
export default function useLoanSimulation(
  userContext?: UserContext
): [
  AccountDetails[] | undefined,
  AccountDetails | undefined,
  (loan: AccountDetails) => void,
  Simulation[] | undefined
] {
  const [loans, setLoans] = useState<AccountDetails[] | undefined>(undefined);
  const [selectedLoan, setSelectedLoan] = useState<AccountDetails | undefined>(undefined);
  const [loanSimulations, setLoanSimulations] = useState<Simulation[] | undefined>(undefined);
  useEffect(() => {
    async function load() {
      if (userContext?.sessionToken) {
        const response = await fetchCreditScoreHistory(userContext, CREDITREPORT_CURRENT);

        const openedAccounts = response?.ReportData.AccountDetails.Account.filter(
          (account) =>
            account?.Open?.toLowerCase() !== 'no' &&
            !(
              account?.AccountType?.toLowerCase() === 'credit card' ||
              account?.AccountType?.toLowerCase().replace(/\s/g, '') === 'corporatecreditcard'
            )
        ).map((account) => parseAccountDetail(account, false));

        if (!openedAccounts) {
          return;
        }

        const loans = (
          await Promise.all(openedAccounts.map(async (account) => await fillEmiTerm(account, userContext)))
        ).filter((account) => account.emi && account.term);

        setLoans(loans);

        const highestBalanceLoan = loans?.sort((a: AccountDetails, b: AccountDetails) =>
          a.balance < b.balance ? 1 : -1
        )[0];

        setSelectedLoan(highestBalanceLoan);
      }
    }
    load();
  }, [userContext]);

  useEffect(() => {
    async function loadSimulations() {
      if (userContext?.sessionToken && selectedLoan) {
        setLoanSimulations(await calculateLoanSimulations(selectedLoan, userContext));
      }
    }
    loadSimulations();
  }, [selectedLoan, userContext]);

  return [loans, selectedLoan, setSelectedLoan, loanSimulations];
}
