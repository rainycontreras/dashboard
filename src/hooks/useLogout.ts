import { useCallback } from 'react';
import { redirectToAuth } from 'src/api/utils';
import { sessionStorageDelete } from 'src/utils/sessionStorage';

/**
 * Logout hook. Remove session data and redirect to authentication page
 *
 */
const useLogout = () => {
  const handler = useCallback(() => {
    sessionStorageDelete('session_token');
    redirectToAuth();
  }, []);
  return handler;
};

export default useLogout;
