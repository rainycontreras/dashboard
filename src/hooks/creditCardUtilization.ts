import { parseISO } from 'date-fns';
import { useEffect, useState } from 'react';
import { buildPaymentCalendar, groupAccountPaymentHistoryByPaymentStatus, parseAccountDetail } from 'src/utils/account';
import { parseNumber } from 'src/utils/number';
import { fetchCreditScoreHistory } from 'src/api/customer';
import { AccountDetails, UserContext, CreditCardUtilization, CreditScoreFactorDisplayType } from 'src/utils/type';

/**
 * Retrieve the customer's credit card utilization and accounts
 * @param {UserContext} userContext - Customer's session data
 * @returns {CreditCardUtilization | undefined} - Customer's Credit Utilization factor information
 */
export default function useCreditCardUtilization(userContext?: UserContext): CreditCardUtilization | undefined {
  const [creditCardUtilization, setCreditCardUtilization] = useState<CreditCardUtilization | undefined>(undefined);
  useEffect(() => {
    async function load() {
      if (userContext?.sessionToken) {
        const response = await fetchCreditScoreHistory(userContext);
        if (response?.ReportData) {
          if (response.ReportData.AccountDetails?.Account) {
            const accounts = response.ReportData.AccountDetails.Account;
            const creditCardAccountsOpened = accounts.filter(
              (account) => account.AccountType?.toLowerCase() === 'credit card' && account.Open?.toLowerCase() === 'yes'
            );
            const accountsDetail: AccountDetails[] = [];
            let creditLimitMissed = false;
            let creditAccountsBalance = 0;
            let creditAccountsLimit = 0;
            let utilization = 0;

            for (const account of creditCardAccountsOpened) {
              const accountDetail = parseAccountDetail(account);

              if (accountDetail.displayType === CreditScoreFactorDisplayType.MONEY) {
                creditLimitMissed = true;
              }

              creditAccountsBalance += accountDetail.balance;
              creditAccountsLimit += accountDetail.creditLimit ?? 0;

              accountsDetail.push(accountDetail);
            }

            if (!creditLimitMissed && creditAccountsLimit > 0) {
              utilization = (creditAccountsBalance / creditAccountsLimit) * 100;
            }

            setCreditCardUtilization({
              balance: creditAccountsBalance,
              creditLimit: creditAccountsLimit,
              utilization,
              displayType: creditLimitMissed
                ? CreditScoreFactorDisplayType.MONEY
                : CreditScoreFactorDisplayType.PERCENTAGE,
              accounts: accountsDetail,
            });
          }
        }
      }
    }
    load();
  }, [userContext]);

  return creditCardUtilization;
}
