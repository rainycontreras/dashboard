import { parseISO } from 'date-fns';
import { useEffect, useState } from 'react';
import { ENQUIRY_PURPOSES, parseAccountEnquiries } from 'src/utils/account';
import { parseNumber } from 'src/utils/number';
import { fetchCreditScoreHistory } from 'src/api/customer';
import { UserContext, CreditEnquiries, AccountEnquiry } from 'src/utils/type';

/**
 * Retrieve customer's credit enquiries
 * @param {UserContext} userContext - Customer's session data
 * @returns {CreditEnquiries | undefined} - Customer's credit enquiries
 */
export default function useCreditEnquiries(userContext?: UserContext): CreditEnquiries | undefined {
  const [creditEnquiries, setCreditEnquiries] = useState<CreditEnquiries | undefined>(undefined);
  useEffect(() => {
    async function load() {
      if (userContext?.sessionToken) {
        const response = await fetchCreditScoreHistory(userContext);
        if (response?.ReportData) {
          const totalEnquiries = parseNumber(response?.ReportData?.EnquirySummary?.Past24Months);
          const enquiries = parseAccountEnquiries(response?.ReportData?.Enquiries) ?? [];
          setCreditEnquiries({ totalCreditEnquiries: totalEnquiries, enquiries });
        }
      }
    }
    load();
  }, [userContext]);

  return creditEnquiries;
}
