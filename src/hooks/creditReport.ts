import { homeLoanRecommendations } from '@/api/analytics';
import { ACCOUNT_OPEN, ACCOUNT_SECURED, parseAccountDetail, parseAccountEnquiries } from '@/utils/account';
import { log } from '@/utils/log';
import { parseNumber } from '@/utils/number';
import { useEffect, useState } from 'react';
import { fetchCreditScoreHistory } from 'src/api/customer';
import { CreditReport, UserContext } from 'src/utils/type';

/**
 * Retrieve the customer's credit report
 * @param {UserContext} userContext - Customer's session data
 * @returns {CreditReport | undefined} - Customer's credit report
 */
export default function useCreditReport(userContext?: UserContext): CreditReport | undefined {
  const [creditReport, setCreditReport] = useState<CreditReport | undefined>(undefined);
  useEffect(() => {
    async function load() {
      if (userContext?.sessionToken) {
        let response = await fetchCreditScoreHistory(userContext);

        let name = '';

        if (response?.ReportData?.IDAndContactInfo) {
          const contactInfo = response?.ReportData?.IDAndContactInfo;
          name = [contactInfo.PersonalInfo?.Name?.FirstName, contactInfo.PersonalInfo?.Name?.MiddleName]
            .filter((item) => item)
            .join(' ');
        }

        const addresses =
          response?.ReportData?.IDAndContactInfo?.AddressInfo?.map((info) => ({
            address: info.Address,
            state: info.State,
            postal: info.Postal,
          })) ?? [];

        const secureAccounts = response?.ReportData?.AccountDetails?.Account?.filter(ACCOUNT_SECURED).length ?? 0;
        const unsecureAccounts = response?.ReportData?.AccountDetails?.Account?.length ?? 0 - secureAccounts;

        const enquiries = parseAccountEnquiries(response?.ReportData?.Enquiries) ?? [];

        const accounts = response?.ReportData?.AccountDetails?.Account?.filter(ACCOUNT_OPEN).map((account) =>
          parseAccountDetail(account)
        );

        if (accounts) {
          for (const account of accounts) {
            if (account.type === 'Housing Loan' || account.type === 'Property Loan') {
              try {
                const recommendations = await homeLoanRecommendations(userContext);
                if (
                  recommendations &&
                  recommendations.accountType === account.type &&
                  recommendations.loanBalance === account.balance &&
                  recommendations.sanctionAmount === account.sanctionAmount
                ) {
                  let savingAmount = recommendations.savingAmount;
                  if (savingAmount < 20000) {
                    savingAmount = 20000;
                  }

                  account.savings = { amount: savingAmount };
                }
              } catch (error) {
                log.error(error);
              }
            }
          }
        }

        setCreditReport({
          personalInformation: { name, addresses },
          mixBorrowing: { secure: secureAccounts, unsecure: unsecureAccounts },
          enquiries,
          accounts,
        });
      }
    }
    load();
  }, [userContext]);

  return creditReport;
}
