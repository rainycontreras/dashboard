import { Tab, Typography } from '@mui/material';
import { useRouter } from 'next/router';
interface Props {
  label: string;
  selected?: boolean;
  value: string;
}

/**
 * Renders application styled Tab, automatically highlights self when the .value property matches current url
 * @deprecated Use NavTab instead
 * @component AppNavigationTab
 * @param {string} label - label of the Tab
 * @param {boolean} [selected] - overrides whether the Tab is selected or not
 * @param {string} value - value/url of the Tab
 */
// TODO: rename to AppNavTab or just NavTab
const AppNavigationTab: React.FC<Props> = ({ label, selected: propSelected = false, value }) => {
  const router = useRouter();
  const selected = propSelected || router.pathname.endsWith(value); // Variant 1
  // const selected = propSelected || router.pathname.includes(value); // Variant 2

  return (
    <Tab
      disableRipple
      label={
        <Typography variant="label2" sx={{ color: 'secondary.main' }}>
          {label}
        </Typography>
      }
      sx={{
        backgroundColor: 'app.lightBlue',
        border: '1.5px solid',
        borderRadius: '6px',
        borderColor: 'primary.main',
        boxSizing: 'border-box',
        marginRight: '0.5rem',
        height: '2.25rem',
        minHeight: '2.25rem',
        opacity: selected ? 1 : 0.5,
      }}
      value={value}
    />
  );
};

export default AppNavigationTab;
