import { Box, Stack, Typography } from '@mui/material';
import { AccountDetailFieldAligment } from 'src/utils/type';

interface AccountDetailFieldProps {
  label: string;
  value?: string;
  valueAligment?: AccountDetailFieldAligment;
}

/**
 * Account detail field
 * @component AccountDetailField
 * @param {string} label - Field's label
 * @param {string} value - Field's value
 * @param {AccountDetailFieldAligment} valueAligment - Filed value's alignment
 */
const AccountDetailField: React.FC<AccountDetailFieldProps> = ({
  label,
  value,
  valueAligment = AccountDetailFieldAligment.LEFT,
}) => {
  return (
    <Stack spacing={0.5}>
      <Typography variant="tiny" sx={{ opacity: 0.6 }}>
        {label}
      </Typography>
      <Box
        justifyContent={valueAligment === AccountDetailFieldAligment.LEFT ? 'start' : 'end'}
        sx={{ display: 'flex' }}
      >
        <Typography sx={{ fontWeight: '600', color: 'secondary.main', fontSize: '0.8125rem' }}>{value}</Typography>
      </Box>
    </Stack>
  );
};

export default AccountDetailField;
