import { useEffect, useState, useCallback } from 'react';
import { Drawer } from '@mui/material';

interface Props {
  open?: boolean;
}

/**
 * Drawer for Dashboard
 * @component AppDrawer
 * @param {boolean} open - controls open/close Drawer's status
 */
const AppDrawer: React.FC<Props> = ({ open = false, children }) => {
  const [container, setContainer] = useState<any>(null);
  const [openSideBar, setOpenSideBar] = useState(true); // TODO: Do we need this state?

  const onOpenSideBar = useCallback(() => {
    // TODO: Do we need this event?
    setOpenSideBar(true);
  }, []);

  useEffect(() => {
    setContainer(typeof window === 'undefined' ? () => window.document.body : undefined);
  }, []);

  return (
    <>
      <Drawer
        container={container}
        variant="temporary"
        open={open}
        ModalProps={{
          keepMounted: true,
        }}
        sx={{
          display: { xs: 'block', sm: 'none' },
          '& .MuiDrawer-paper': { boxSizing: 'border-box', width: '100%' },
        }}
      >
        {children}
      </Drawer>
      <Drawer
        variant="permanent"
        sx={{
          display: { xs: 'none', sm: 'block' },
          '& .MuiDrawer-paper': { boxSizing: 'border-box', width: '16rem' },
        }}
        open
      >
        {children}
      </Drawer>
    </>
  );
};

export default AppDrawer;
