import { useMemo } from 'react';
import { Box, Button, Stack, Typography } from '@mui/material';
import { ScoringFactorImpact } from 'src/utils/type';
import { PaletteOptions, useTheme } from '@mui/material/styles';
import DotIcon from 'src/components/icons/other/DotIcon';
import Link from 'next/link';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';

interface CreditScoreFactorProps {
  color: string;
  description: string;
  impact: ScoringFactorImpact;
  link: string;
  title: string;
  value?: string;
}

/**
 * Credit Factor component for Credit Score Factors Page
 * @param {string} color - Credit Score Factor Value's color
 * @param {string} description - Credit Score Factor's description
 * @param {ScoringFactorImpact} impact - Credit Score Factor's impact
 * @param {string} link - Credit Score Factor's learn more link
 * @param {string} title - Credit Score Factor's tile
 * @param {string} value - Credit Score Factor's value
 */

const CreditScoreFactor: React.FC<CreditScoreFactorProps> = ({ color, description, impact, link, title, value }) => {
  const theme = useTheme();

  const impactDefinition = useMemo(() => {
    const appColorSpace = (theme.palette as PaletteOptions)?.app;
    let color = undefined;
    let label = undefined;
    if (impact === ScoringFactorImpact.High) {
      color = appColorSpace?.colorBar.veryBad;
      label = 'High Impact';
    } else if (impact === ScoringFactorImpact.Medium) {
      color = appColorSpace?.colorBar.bad;
      label = 'Medium Impact';
    } else {
      color = appColorSpace?.colorBar.veryGood;
      label = 'Low Impact';
    }
    return { color, label };
  }, [impact, theme]);

  return (
    <Box
      sx={{
        border: '1px solid',
        borderRadius: '6px',
        borderColor: 'app.border',
        height: '100%',
      }}
    >
      <Stack justifyContent="end" direction="column" sx={{ height: '100%' }}>
        <Stack
          direction="row"
          justifyContent="space-between"
          sx={{
            borderBottom: '1px solid',
            borderBottomColor: 'app.border',
            paddingBottom: '1rem',
            paddingLeft: '1.5rem',
            paddingTop: '1rem',
            paddingRight: '1.5rem',
          }}
        >
          <Typography variant="h1" sx={{ fontSize: '0.9375rem' }}>
            {title}
          </Typography>
          <Box>
            <Box
              sx={{
                backgroundColor: `${color}Background`,
                borderRadius: '4px',
                padding: '0.125rem 0.25rem',
              }}
            >
              <Typography
                variant="h1"
                sx={{
                  color,
                  fontSize: '0.9375rem',
                }}
              >
                {value}
              </Typography>
            </Box>
          </Box>
        </Stack>
        <Stack
          spacing={1}
          sx={{
            flexGrow: 1,
            paddingBottom: '1rem',
            paddingLeft: '1.5rem',
            paddingTop: '1rem',
            paddingRight: '1.5rem',
          }}
        >
          <Box>
            {/* TODO: Make "Bullet" component for this */}
            <DotIcon color={impactDefinition?.color ?? ''} />
            <Typography sx={{ color: impactDefinition?.color ?? '', paddingLeft: '0.625rem' }} variant="label2">
              {impactDefinition?.label ?? ''}
            </Typography>
          </Box>
          <Typography variant="body">{description}</Typography>
        </Stack>
        <Stack
          sx={{
            paddingBottom: '1rem',
            paddingLeft: '1.5rem',
            paddingTop: '1rem',
            paddingRight: '1.5rem',
          }}
        >
          <Box>
            <Link href={link} passHref>
              <Button component="a" endIcon={<ChevronRightIcon />}>
                <Typography variant="link">Learn More</Typography>
              </Button>
            </Link>
          </Box>
        </Stack>
      </Stack>
    </Box>
  );
};

export default CreditScoreFactor;
