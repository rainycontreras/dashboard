import { useCallback, useState } from 'react';
import copyToClipboard from 'copy-to-clipboard';
import { Box, ClickAwayListener, Divider, Grid, Stack, Typography, useMediaQuery, Theme } from '@mui/material';
import Section from 'src/components/AppSection';
import { AppIcon, AppIconButton, AppLink } from '..';

interface Props {
  referralCode?: string;
}

/**
 * Refer and Earn section for Home page
 * @param {string} referralCode - User's referral code
 */
const ReferEarnSection: React.FC<Props> = ({ referralCode }) => {
  const smScreen = useMediaQuery((theme: Theme) => theme.breakpoints.up('sm'));
  const [openCopiedTooltip, setOpenCopiedTooltip] = useState(false);

  //#region handlers
  const closeCopiedTooltipHandle = useCallback(() => {
    setOpenCopiedTooltip(false);
  }, []);
  const copyReferralCodeHandle = useCallback(() => {
    if (referralCode) {
      copyToClipboard(referralCode);
      setOpenCopiedTooltip(true);
      setTimeout(() => {
        setOpenCopiedTooltip(false);
      }, 1000);
    }
  }, [referralCode]);
  //#endregion

  return (
    <Section title="Refer & Earn">
      <Grid container sx={{ padding: '1rem' }} spacing={2}>
        <Grid item xs={12} sm={12} md={3}>
          <Box alignItems="center" justifyContent="center" sx={{ display: 'flex', width: '100%', height: '100%' }}>
            <AppIcon name="Currencies" />
          </Box>
        </Grid>
        <Grid item xs={12} sm={12} md={9}>
          <Stack spacing={3}>
            <Stack spacing={1} sx={{ paddingTop: smScreen ? '0' : '1.6rem' }}>
              <Typography variant="h1" sx={{ fontSize: '1.125rem', lineHeight: '1.5rem' }}>
                Rs 80,000 cashback for helping your friends save on property loans
              </Typography>
              <Typography variant="body">
                Refer your friends for the best deals on property loans. Both of you earn when loan is disbursed.
              </Typography>
            </Stack>
            <Stack direction={smScreen ? 'row' : 'column'}>
              <Stack
                justifyContent="center"
                spacing={1}
                sx={{
                  paddingBottom: { xs: '1rem', sm: '0' },
                  paddingRight: { xs: '0', sm: '1rem' },
                }}
              >
                <Box justifyContent={smScreen ? 'start' : 'center'} sx={{ display: 'flex' }}>
                  <Typography
                    variant="text"
                    sx={{
                      fontSize: '0.875rem',
                      fontWeight: '500',
                    }}
                  >
                    Refer Now
                  </Typography>
                </Box>
                <Stack justifyContent={smScreen ? 'start' : 'center'} direction="row">
                  <AppIconButton
                    component={AppLink}
                    href={`https://www.facebook.com/sharer.php?u=https%3A%2F%2Fwww.mymoneykarma.com%2Ffast-home-loan.html%3FcampType%3DHOMELOAN%26utm_source%3D${referralCode}_facebook`}
                    icon="Facebook"
                    sx={{
                      margin: '0.5rem 0.5rem 0 0',
                      marginLeft: smScreen ? '0' : '0.5rem',
                      padding: '0',
                    }}
                  />
                  <AppIconButton
                    component={AppLink}
                    href={`https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fwww.mymoneykarma.com%2Ffast-home-loan.html%3FcampType%3DHOMELOAN%26utm_source%3D${referralCode}_linkedin`}
                    icon="LinkedIn"
                    sx={{
                      margin: '0.5rem 0.5rem 0 0',
                      marginLeft: smScreen ? '0' : '0.5rem',
                      padding: '0',
                    }}
                  />
                  <AppIconButton
                    component={AppLink}
                    href={`https://twitter.com/share?url=https%3A%2F%2Fwww.mymoneykarma.com%2Ffast-home-loan.html%3FcampType%3DHOMELOAN%26utm_source%3D${referralCode}_twitter&text=Mymoneykarma%20Home%20Loans`}
                    icon="Twitter"
                    sx={{
                      margin: '0.5rem 0.5rem 0 0',
                      marginLeft: smScreen ? '0' : '0.5rem',
                      padding: '0',
                    }}
                  />

                </Stack>
              </Stack>
              <Divider
                flexItem
                orientation={smScreen ? 'vertical' : 'horizontal'}
                sx={{
                  '&::after': { borderColor: 'app.antiflashWhite' },
                  '&::before': { borderColor: 'app.antiflashWhite' },
                  '& .MuiDivider-wrapper': { padding: 0 },
                }}
              >
                <Box
                  sx={{
                    border: '1px solid',
                    borderColor: 'app.antiflashWhite',
                    borderRadius: '50%',
                    padding: '6px',
                  }}
                >
                  <Typography
                    sx={{
                      color: 'app.grey',
                      fontSize: '0.625rem',
                      fontWeight: '600',
                      opacity: '50%',
                    }}
                  >
                    OR
                  </Typography>
                </Box>
              </Divider>
              <Stack
                justifyContent="center"
                spacing={1}
                sx={{
                  paddingLeft: { xs: '0', sm: '1rem' },
                  paddingTop: { xs: '1rem', sm: '0' },
                }}
              >
                <Box justifyContent={smScreen ? 'start' : 'center'} sx={{ display: 'flex' }}>
                  <Typography
                    variant="text"
                    sx={{
                      fontSize: '0.875rem',
                      fontWeight: '500',
                    }}
                  >
                    Copy Referral Code
                  </Typography>
                </Box>
                <Stack justifyContent={smScreen ? 'start' : 'center'}>
                  <Stack
                    direction="row"
                    sx={{
                      backgroundColor: 'app.lightGrey',
                      border: '1.5px dashed',
                      borderColor: 'app.lightPeriwinkle',
                      borderRadius: '6px',
                      marginTop: '0.5rem',
                    }}
                  >
                    <Stack alignItems="center" direction="row" sx={{ flexGrow: 1, padding: '0.5rem' }}>
                      <Typography
                        sx={{
                          color: 'secondary',
                          flexGrow: 1,
                          fontSize: '1rem',
                          fontWeight: '600',
                        }}
                        variant="h1"
                      >
                        {referralCode}
                      </Typography>
                    </Stack>
                    <ClickAwayListener onClickAway={closeCopiedTooltipHandle}>
                      <Box alignItems="center" justifyContent="center" sx={{ display: 'flex' }}>
                        <AppIconButton
                          icon="Copy"
                          sx={{
                            height: '2.5rem',
                            width: '2.5rem',
                          }}
                          title={openCopiedTooltip ? 'Copied' : undefined}
                          onClick={copyReferralCodeHandle}
                        />
                      </Box>
                    </ClickAwayListener>
                  </Stack>
                </Stack>
              </Stack>
            </Stack>
          </Stack>
        </Grid>
      </Grid>
    </Section>
  );
};

export default ReferEarnSection;
