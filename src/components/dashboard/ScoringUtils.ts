import * as d3 from 'd3-shape';
import { ColoredBarTier, PathColor, ScoreValuePointer } from 'src/utils/type';

const SCORING_STATUS_LOW = [300, 600];
const SCORING_STATUS_FAIR = [600, 700];
const SCORING_STATUS_GOOD = [700, 750];
const SCORING_STATUS_EXCELLENT = [750, 900];

/**
 * Earch tier for scoring levels
 */
export const SCORING_TIERS: ColoredBarTier[] = [
  {
    label: 'Low',
    min: SCORING_STATUS_LOW[0],
    max: SCORING_STATUS_LOW[1],
    color: '#E76262',
  },
  {
    label: 'Fair',
    min: SCORING_STATUS_FAIR[0],
    max: SCORING_STATUS_FAIR[1],
    color: '#F9CF4A',
  },
  {
    label: 'Good',
    min: SCORING_STATUS_GOOD[0],
    max: SCORING_STATUS_GOOD[1],
    color: '#ECA336',
  },
  {
    label: 'Excellent',
    min: SCORING_STATUS_EXCELLENT[0],
    max: SCORING_STATUS_EXCELLENT[1],
    color: '#3ECE80',
  },
];

/**
 * Translate the value of scoring to the arc's angle.
 * @param value {number} Scoring value
 * @param angleBelowHorizon {number} Angle below horizon where starts or ends the arc.
 * @returns {number}
 */
function calculateAngle(value: number, angleBelowHorizon: number) {
  return value * Math.PI + angleBelowHorizon * (2 * value - 1) - Math.PI / 2;
}

/**
 * Generate all svg parts for scoring's arc
 * @param {number} radius - Arc's radius
 * @param {CreditScoreTier[]} tiers - Division of the scoring's arc
 * @param {number} arcWidth - Width for arc's line
 * @param {number} minScore -  Minimum score value
 * @param {number} maxScore - Maximum score value
 * @param {number} paddleAngle - Angle of separation for arc's divisions
 * @param {number} angleBelowHorizon- Angle below horizon where starts or ends the arc.
 * @returns {PathColor[]}
 */
export function generateArc(
  radius: number,
  tiers: ColoredBarTier[],
  arcWidth: number,
  minScore: number,
  maxScore: number,
  paddleAngle: number,
  angleBelowHorizon: number
): PathColor[] {
  const range = maxScore - minScore;
  const innerRadius = radius - arcWidth;
  return tiers.map((tier: ColoredBarTier, index: number) => {
    let startPercentage = (tier.min - minScore) / range;
    let endPercentage = (tier.max - minScore) / range;
    let startAngle = calculateAngle(startPercentage, angleBelowHorizon) + (index === 0 ? 0 : paddleAngle);
    let endAngle = calculateAngle(endPercentage, angleBelowHorizon) - (index === tiers.length - 1 ? 0 : paddleAngle);

    const arc = d3
      .arc()
      .innerRadius(innerRadius)
      .outerRadius(radius)
      .startAngle(startAngle)
      .endAngle(endAngle)
      .padAngle(0)
      .cornerRadius(20) as Function;

    return {
      path: arc(),
      color: tier.color,
      key: `${tier.min}:${tier.max}:${tier.color}`,
    };
  });
}

/**
 * Generate all svg parts for arc's indicators.
 * @param {number} radius - Arc's radius
 * @param {number} arcWidth - Width for arc's line
 * @param {number} angleBelowHorizon - Angle below horizon where starts or ends the arc.
 * @returns {PathColor[]}
 */
export function generateAngleIndicators(
  radius: number,
  arcWidth: number,
  color: string,
  angleBelowHorizon: number
): PathColor[] {
  let result: PathColor[] = [];

  for (let i = (-1 * Math.PI) / 2 - angleBelowHorizon; i <= Math.PI / 2 + angleBelowHorizon; i += Math.PI / 20) {
    const innerRadius = radius - arcWidth - 20;
    const outerRadius = radius - arcWidth - 17;
    const levelsArc = d3
      .arc()
      .innerRadius(innerRadius)
      .outerRadius(outerRadius)
      .startAngle(i)
      .endAngle(i + Math.PI / 360)
      .padAngle(0)
      .cornerRadius(0) as Function;
    result.push({
      path: levelsArc(),
      color,
      key: `${i}`,
    });
  }
  return result;
}

/**
 * Generate data (svg to pointer,x position, y position) to paint score value's pointer.
 * @param {number} value - Score value
 * @param {number} pointerWidth - Pointer's width
 * @param {number} radius - Arc's radius
 * @param {number} minScore - Minimum score value
 * @param {number} maxScore - Maximum score value
 * @param {number} width - Scoring component's width
 * @param {number} height - Scoring component's height
 * @param {number} angleBelowHorizon - Angle below horizon where starts or ends the arc.
 * @returns {ScoreValuePointer}
 */
export function generateScoreValuePointer(
  value: number,
  pointerWidth: number,
  radius: number,
  minScore: number,
  maxScore: number,
  width: number,
  height: number,
  angleBelowHorizon: number
): ScoreValuePointer {
  // Scoring pointer
  const arc5 = d3
    .arc()
    .innerRadius(pointerWidth / 2 - 1)
    .outerRadius(pointerWidth)
    .startAngle(0)
    .endAngle(Math.PI * 2)
    .padAngle(0)
    .cornerRadius(0) as Function;
  const pointerPath = arc5();

  // Pointer white center
  const arc6 = d3
    .arc()
    .innerRadius(0)
    .outerRadius(pointerWidth - 1)
    .startAngle(0)
    .endAngle(Math.PI * 2)
    .padAngle(0)
    .cornerRadius(0) as Function;

  const fillPath = arc6();

  // Calculate pointer position
  const scoring = (value - minScore) / (maxScore - minScore);
  const angle = (1 - scoring) * Math.PI + angleBelowHorizon * (2 * (1 - scoring) - 1);
  const y = Math.sin(angle);
  const x = Math.cos(angle);

  const yPosition = height - (radius - pointerWidth / 2) * y;
  const xPosition = width / 2 + (radius - pointerWidth / 2) * x;

  return {
    pointerPath,
    fillPath,
    xPosition,
    yPosition,
  };
}

/**
 * Translate a score's value number to message
 * @param {number} value - Score's value
 * @returns {string} - Score value's message
 */
export function getScoreValueMessage(value: number): string {
  if (value >= 750) {
    return 'Excellent';
  } else if (value >= 700) {
    return 'Good';
  } else if (value >= 600) {
    return 'Average';
  } else {
    return 'Poor';
  }
}

/**
 * Get score's value color
 * @param {ColoredBarTier[] | undefined} tiers - Tiers
 * @param {number} value - Score's value
 * @param {string} defaultColor - Score default color
 * @returns {string} - Score value's color
 */
export function getScoreValueColor(
  tiers: ColoredBarTier[] | undefined,
  value: number | undefined,
  defaultColor?: string
): string | undefined {
  if (!tiers || value === null || value === undefined) {
    return defaultColor;
  }
  // Get color for current value
  let valuePart = tiers.filter(
    (p: ColoredBarTier, index) =>
      p.min <= value && (value < p.max || (value === p.max && (p.min === p.max || index === SCORING_TIERS.length - 1)))
  );
  if (valuePart && valuePart.length > 0) {
    return valuePart[0].color;
  }
  return defaultColor;
}
