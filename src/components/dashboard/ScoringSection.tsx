import { Button, Grid, Stack, Typography } from '@mui/material';
import Scoring from './Scoring';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import AppIcon from 'src/components/AppIcon';
import Section from 'src/components/AppSection';

interface ScoringSectionProps {
  score?: number;
  updatedDate?: Date;
}

/**
 * Score section for Home page
 * @param {number} score - User's credit score
 * @param {Date} updatedDate - Credit score's last updated date
 */
const ScoringSection: React.FC<ScoringSectionProps> = ({ score, updatedDate }) => {
  return (
    <Section title="My Credit Score">
      <Stack>
        <Stack
          alignItems="center"
          justifyContent="center"
          sx={{
            paddingTop: '1.25rem',
            paddingLeft: '1.5rem',
            paddingRight: '1.5rem',
          }}
        >
          <Stack
            alignItems="center"
            justifyContent="center"
            sx={{ paddingBottom: '2rem', paddingTop: '3rem', width: '100%' }}
            spacing={1}
          >
            <Scoring updatedDate={updatedDate} value={score} width={270} />
          </Stack>
        </Stack>
        <Stack />
      </Stack>
      <Grid
        container
        sx={{
          borderTop: '1px solid',
          borderColor: 'app.border',
        }}
      >
        <Grid item xs={12} md={6}>
          <Stack
            sx={{
              borderBottom: { xs: '1px solid', md: '1px' },
              borderColor: 'app.border',
              borderRight: { xs: '1px', md: '1px solid' },
              flexGrow: 1,
            }}
          >
            <Button
              startIcon={
                <Stack alignItems="center">
                  <AppIcon name="creditscore" color="#1DBD66" />
                </Stack>
              }
              endIcon={<ChevronRightIcon color="secondary" />}
            >
              <Typography variant="label2">See what affects your Credit Score</Typography>
            </Button>
          </Stack>
        </Grid>
        <Grid item xs={12} md={6}>
          <Stack sx={{ flexGrow: 1 }}>
            <Button
              startIcon={
                <Stack alignItems="center">
                  <AppIcon name="compare" color="#ECA336" />
                </Stack>
              }
              endIcon={<ChevronRightIcon color="secondary" />}
            >
              <Typography variant="label2">Compare your last 2 Credit Reports</Typography>
            </Button>
          </Stack>
        </Grid>
      </Grid>
    </Section>
  );
};

export default ScoringSection;
