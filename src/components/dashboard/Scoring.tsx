import { Box, Stack, Typography } from '@mui/material';
import { useState, useEffect, useMemo } from 'react';
import {
  generateAngleIndicators,
  generateArc,
  generateScoreValuePointer,
  getScoreValueMessage,
  SCORING_TIERS,
} from './ScoringUtils';
import { ColoredBarTier, PathColor } from 'src/utils/type';
import ColoredBar from 'src/components/ColoredBar';
import React from 'react';

interface ScoringProps {
  width: number;
  height?: number;
  value?: number;
  updatedDate?: Date;
  arcWidth?: number;
  pointerWidth?: number;
  showColoredBar?: boolean;
}

const PADDLE_ANGLE = Math.PI / 100;
const ARC_ANGLE_BELOW_HORIZON = Math.PI / 20;

// TODO: Check scoring min and max values
const SCORE_MIN = 300;
const SCORE_MAX = 900;

/**
 * Score chart component
 * @component
 * @param {number} width - Chart component's width
 * @param {number} height - Chart component's width
 * @param {number} value - Customer's score value
 * @param {number} updatedDate - Score value's updated date
 * @param {number} arcWidth - Arc's line width
 * @param {number} pointerWidth - Pointer's width
 * @param {boolean} showColoredBar - Controls if colored bar is shown
 */
const Scoring: React.FC<ScoringProps> = ({
  children,
  value,
  updatedDate,
  width,
  arcWidth = 14,
  pointerWidth = 15,
  height = width / 2,
  showColoredBar = true,
}) => {
  const [arcPaths, setArcPaths] = useState<PathColor[]>([]);
  const [pointer, setPointer] = useState<string | undefined>(undefined);
  const [levels, setLevels] = useState<PathColor[]>([]);
  const [fillPointer, setFillPointer] = useState<string | undefined>(undefined);
  const [x, setX] = useState(0);
  const [y, setY] = useState(0);
  const [color, setColor] = useState('white');
  const [heigthBelowHorizon, setHeightBelowHorizon] = useState(0);
  const [scoreMessage, setScoreMessage] = useState('');

  const { valueControl, labelControl } = useMemo(() => {
    const controls = React.Children.toArray(children);
    const valueControl = controls?.[0];
    const labelControl = controls?.[1];
    return { valueControl, labelControl };
  }, [children]);

  useEffect(() => {
    if (value != null) {
      setScoreMessage(getScoreValueMessage(value));
    }

    let radius = width / 2;

    setArcPaths(
      generateArc(radius, SCORING_TIERS, arcWidth, SCORE_MIN, SCORE_MAX, PADDLE_ANGLE, ARC_ANGLE_BELOW_HORIZON)
    );

    setLevels(generateAngleIndicators(radius, arcWidth, '#1D2145', ARC_ANGLE_BELOW_HORIZON));

    if (value != null) {
      // Get color for current value
      let valuePart = SCORING_TIERS.filter(
        (p: ColoredBarTier) => p.min <= value && (value < p.max || p.max >= SCORE_MAX)
      );
      if (valuePart && valuePart.length > 0) {
        setColor(valuePart[0].color);
      }

      let pointer = generateScoreValuePointer(
        value,
        pointerWidth,
        radius,
        SCORE_MIN,
        SCORE_MAX,
        width,
        height,
        ARC_ANGLE_BELOW_HORIZON
      );
      setPointer(pointer.pointerPath);
      setFillPointer(pointer.fillPath);
      setX(pointer.xPosition);
      setY(pointer.yPosition);
    }
    // Calculate additional height to show arc below horizon line
    setHeightBelowHorizon(radius * Math.abs(Math.sin(ARC_ANGLE_BELOW_HORIZON)));
  }, [pointerWidth, value, width, arcWidth, height]);

  return (
    <Stack alignItems="center" justifyContent="center" sx={{ width: '100%' }}>
      <div
        style={{
          position: 'relative',
          width: `${width}px`,
          height: `${height + heigthBelowHorizon}px`,
        }}
      >
        <svg
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            width: `${width}px`,
            height: `${height + heigthBelowHorizon}px`,
          }}
        >
          {arcPaths.map((part) => {
            return (
              <path
                key={part.key}
                fill={part.color}
                d={part.path}
                style={{ transform: `translate(50%, ${height}px)` }}
              />
            );
          })}

          {levels.map((part: PathColor) => {
            return (
              <path
                key={part.key}
                fill={part.color}
                d={part.path}
                style={{ transform: `translate(50%, ${height}px)` }}
              />
            );
          })}
        </svg>
        <svg
          style={{
            // The pointer must be painted over arc
            zIndex: 2,
            position: 'absolute',
            top: `${-pointerWidth}px`,
            left: `${-pointerWidth}`,
            width: `${width + pointerWidth * 2}px`,
            height: `${height + heigthBelowHorizon + pointerWidth * 2}px`,
          }}
        >
          <path
            fill="white"
            d={fillPointer}
            style={{
              transform: `translate(${x + pointerWidth}px, ${y + pointerWidth}px)`,
            }}
          />
          <path
            fill={color}
            d={pointer}
            style={{
              transform: `translate(${x + pointerWidth}px, ${y + pointerWidth}px)`,
            }}
          />
        </svg>
        <Box
          sx={{
            position: 'absolute',
            top: 0,
            left: 0,
            width: `${width}px`,
            height: `${height + heigthBelowHorizon}px`,
            fontSize: '3.5rem',
          }}
        >
          {valueControl}
          {!valueControl && (
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'end',
                height: '100%',
              }}
            >
              <Typography variant="h1" sx={{ fontSize: '3.5rem', fontWeight: 600 }}>
                {value}
              </Typography>
            </Box>
          )}
        </Box>
      </div>
      {labelControl}
      {!labelControl && (
        <Box sx={{ paddingTop: '0.75rem' }}>
          <Typography
            sx={{
              fontWeight: 500,
            }}
            variant="label2"
          >
            Your credit score is {scoreMessage}
          </Typography>
        </Box>
      )}
      {updatedDate && (
        <Typography sx={{ opacity: '0.5' }} variant="caption">
          Updated {updatedDate?.toLocaleDateString('hi')}
        </Typography>
      )}
      {showColoredBar && (
        <ColoredBar
          distribution="3.5fr 1.3fr 1fr 1.8fr" //TODO: refactor based on tiers values
          value={value}
          tiers={SCORING_TIERS}
        />
      )}
    </Stack>
  );
};

export default Scoring;
