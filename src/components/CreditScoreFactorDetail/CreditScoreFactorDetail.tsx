import { Box, Grid, Stack, Typography, useMediaQuery, Theme } from '@mui/material';
import { useTheme, PaletteOptions } from '@mui/material/styles';
import { ColoredBarTier, CreditScoreFactorDisplayType, ScoringFactorImpact } from 'src/utils/type';
import { useMemo } from 'react';
import DotIcon from 'src/components/icons/other/DotIcon';
import AppIcon from 'src/components/AppIcon';
import { formatMoney, formatPercentage } from 'src/utils/number';
import ColoredBar from 'src/components/ColoredBar';
import { getScoreValueColor } from 'src/components/dashboard/ScoringUtils';
import { ExplanationFooter } from '../forPages/CreditScoreFactors';
import React from 'react';

interface CalculationFieldProps {
  value?: string;
}

interface CreditScoreFactorDetailProps {
  calculationDescriptionTitle?: string;
  calculationDescription?: string;
  color?: string;
  coloredBarRtl?: boolean;
  description?: string;
  displayType?: CreditScoreFactorDisplayType;
  factorDescription?: string;
  factorDescriptionTitle?: string;
  formattedValue?: string;
  impact?: ScoringFactorImpact;
  link?: string;
  showColoredBar?: boolean;
  tiers?: ColoredBarTier[];
  title?: string;
  value?: number;
  iconName?: string;
  valueDescription?: string;
  valueDescriptionLabel1?: string;
  valueDescriptionLabel2?: string;
  valueDescriptionLabelResult?: string;
  valueDescriptionOperator1?: string;
  valueDescriptionOperator2?: string;
  valueDescriptionResult?: string;
}

//TODO: Refactor using variant definition
const CALCULATION_VARIANT = {
  fontWeight: 600,
  fontSize: '0.8125rem',
};

/**
 * Credit Score Factor Calculation field
 * @param {string} value - Calculation field's value
 */
const CalculationField: React.FC<CalculationFieldProps> = ({ value = '' }) => {
  return (
    <Box
      sx={{
        backgroundColor: 'app.lightGrey',
        border: '1px solid',
        borderColor: 'app.border',
        borderRadius: '0.375rem',
        padding: '0.25rem 0.75rem',
      }}
    >
      <Typography sx={CALCULATION_VARIANT}>{value}</Typography>
    </Box>
  );
};

/**
 * Credit Factor component for Credit Score Factors Page
 */
const CreditScoreFactorDetail: React.FC<CreditScoreFactorDetailProps> = ({
  calculationDescriptionTitle,
  calculationDescription,
  coloredBarRtl = false,
  children,
  displayType,
  factorDescription,
  factorDescriptionTitle,
  formattedValue,
  impact,
  showColoredBar = false,
  tiers,
  title,
  value,
  valueDescription,
  valueDescriptionLabel1,
  valueDescriptionLabel2,
  valueDescriptionLabelResult,
  valueDescriptionOperator1,
  valueDescriptionOperator2,
  valueDescriptionResult,
}) => {
  const theme = useTheme();
  const smScreen = useMediaQuery((theme: Theme) => theme.breakpoints.up('sm'));

  const impactDefinition = useMemo(() => {
    const appColorSpace = (theme.palette as PaletteOptions)?.app;
    let color = undefined;
    let label = undefined;
    if (impact === ScoringFactorImpact.High) {
      color = appColorSpace?.red;
      label = 'High Impact';
    } else if (impact === ScoringFactorImpact.Medium) {
      color = appColorSpace?.marigold;
      label = 'Medium Impact';
    } else {
      color = appColorSpace?.green;
      label = 'Low Impact';
    }
    return { color, label };
  }, [impact, theme]);

  const valueBackgroundColor = useMemo(() => {
    const appColorSpace = (theme.palette as PaletteOptions)?.app;
    //TODO: Refactor to remove displayType property
    if (showColoredBar || displayType === CreditScoreFactorDisplayType.PERCENTAGE) {
      return `${getScoreValueColor(tiers, value, appColorSpace?.lightGrey)}1A`;
    }
    return appColorSpace?.lightGrey;
  }, [showColoredBar, displayType, value, tiers, theme.palette]);

  const calculationLayout = useMemo(() => {
    if (smScreen) {
      return {
        gridTemplateRows: 'min-content 1fr',
        gridTemplateColumns: '1fr min-content 1fr min-content 1fr',
        gridTemplateAreas: `'title1 . title2 . title3'
                'value1 operator1 value2 operator2 value3'`,
      };
    } else {
      return {
        gridTemplateRows: 'min-content 1fr min-content min-content 1fr min-content min-content 1fr',
        gridTemplateColumns: '1fr',
        gridTemplateAreas: `'title1'
                            'value1'
                            'operator1'
                            'title2'
                            'value2'
                            'operator2'
                            'title3'
                            'value3'
              `,
      };
    }
  }, [smScreen]);

  const explanationFooter = useMemo(() => {
    if (children) {
      const explanationFooters = React.Children.toArray(children).filter(
        (child) => (child as React.ReactElement).type === ExplanationFooter
      );

      return explanationFooters && explanationFooters.length > 0 ? explanationFooters[0] : undefined;
    }
    return undefined;
  }, [children]);

  return (
    <Grid alignItems="stretch" container spacing={'1.25rem'}>
      <Grid item xs={12} md={showColoredBar || displayType === CreditScoreFactorDisplayType.PERCENTAGE ? 6 : 12}>
        <Box sx={{ border: '1px solid', borderColor: 'app.border', borderRadius: '0.375rem', height: '100%' }}>
          <Stack>
            {
              //#region header
            }
            <Stack
              direction="row"
              justifyContent="space-between"
              sx={{ padding: '1rem 1.5rem', borderBottom: '1px solid', borderBottomColor: 'app.border' }}
            >
              <Box>
                <Typography variant="label1">{title}</Typography>
              </Box>
              <Stack>
                <Box>
                  {/* TODO: Make "Bullet" component for this */}
                  <DotIcon color={impactDefinition?.color} />
                  <Typography sx={{ color: impactDefinition.color, paddingLeft: '0.625rem' }} variant="label2">
                    {impactDefinition?.label ?? ''}
                  </Typography>
                </Box>
              </Stack>
            </Stack>
            {
              //#endregion header
            }
            {
              //#region body
            }
            <Stack sx={{ padding: '1.5rem' }}>
              <Stack
                alignItems="center"
                direction="row"
                spacing={1.5}
                sx={{ borderRadius: '0.375rem', backgroundColor: valueBackgroundColor, padding: '1.25rem' }}
              >
                <Stack alignItems="center">
                  <AppIcon name={displayType === CreditScoreFactorDisplayType.MONEY ? 'currencies' : 'percentage'} />
                </Stack>
                <Stack sx={{ flexGrow: '1' }}>
                  <Typography variant="caption" sx={{ opacity: '0.6' }}>
                    {valueDescription}
                  </Typography>
                  <Typography variant="h2">
                    {formattedValue
                      ? formattedValue
                      : displayType === CreditScoreFactorDisplayType.MONEY
                      ? formatMoney(value) ?? ''
                      : `${formatPercentage(value, '')}`}
                  </Typography>
                </Stack>
              </Stack>
              <Stack spacing={1} sx={{ paddingTop: '1.25rem' }}>
                <Typography variant="label1">{factorDescriptionTitle}</Typography>
                <Typography variant="body" color="app.grey">
                  {factorDescription}
                </Typography>
              </Stack>
            </Stack>
            {
              //#endregion body
            }
          </Stack>
        </Box>
      </Grid>
      {(showColoredBar || displayType === CreditScoreFactorDisplayType.PERCENTAGE) && (
        <Grid item xs={12} md={6}>
          <Box sx={{ border: '1px solid', borderColor: 'app.border', borderRadius: '0.375rem', height: '100%' }}>
            <Stack>
              {
                //#region header
              }
              <Stack
                direction="row"
                justifyContent="space-between"
                sx={{
                  padding: '1rem 1.5rem',
                  borderBottom: '1px solid',
                  borderBottomColor: 'app.border',
                }}
              >
                <Box>
                  <Typography variant="label1">Where You Stand</Typography>
                </Box>
              </Stack>
              {
                //#endregion header
              }
              {
                //#region body
              }
              <Stack sx={{ padding: '0rem 1.5rem 1.5rem 1.5rem' }}>
                <Stack spacing={1}>
                  <ColoredBar
                    displayLabel
                    displayValueRange={false}
                    tiers={tiers}
                    height={6}
                    pointer={<AppIcon name="personindicator" />} // TODO: Change to correct icon name
                    ltr={!coloredBarRtl}
                    value={value}
                  />
                </Stack>
                <Stack spacing={1} sx={{ paddingTop: '1.25rem' }}>
                  <Typography variant="label1">{calculationDescriptionTitle}</Typography>
                  <Typography variant="body" color="app.grey">
                    {calculationDescription}
                  </Typography>
                </Stack>

                {valueDescriptionLabel1 && valueDescriptionLabel2 && valueDescriptionLabelResult && (
                  <Stack direction="column" spacing={1} sx={{ paddingTop: '1.5rem' }}>
                    <Box
                      sx={{
                        display: 'grid',
                        ...calculationLayout,
                      }}
                    >
                      <Typography variant="tiny" sx={{ gridArea: 'title1', paddingBottom: '0.5rem' }}>
                        {valueDescriptionLabel1}
                      </Typography>
                      <Typography variant="tiny" sx={{ gridArea: 'title2', paddingBottom: '0.5rem' }}>
                        {valueDescriptionLabel2}
                      </Typography>
                      <Typography variant="tiny" sx={{ gridArea: 'title3', paddingBottom: '0.5rem' }}>
                        {valueDescriptionLabelResult}
                      </Typography>

                      <Box sx={{ gridArea: 'value1' }}>
                        <CalculationField value={valueDescriptionOperator1 ?? ''} />
                      </Box>
                      <Stack
                        alignItems="center"
                        justifyContent="center"
                        sx={{ gridArea: 'operator1', padding: '0.375rem' }}
                      >
                        <Typography sx={{ ...CALCULATION_VARIANT, fontSize: '0.75rem' }}>÷</Typography>
                      </Stack>
                      <Box sx={{ gridArea: 'value2' }}>
                        <CalculationField value={valueDescriptionOperator2 ?? ''} />
                      </Box>
                      <Stack
                        alignItems="center"
                        justifyContent="center"
                        sx={{ gridArea: 'operator2', padding: '0.375rem' }}
                      >
                        <Typography sx={{ ...CALCULATION_VARIANT, fontSize: '0.75rem' }}>=</Typography>
                      </Stack>
                      <Box sx={{ gridArea: 'value3' }}>
                        <CalculationField value={valueDescriptionResult ?? ''} />
                      </Box>
                    </Box>
                  </Stack>
                )}
                {
                  //#endregion body
                }
                {explanationFooter}
              </Stack>
            </Stack>
          </Box>
        </Grid>
      )}
    </Grid>
  );
};

export default CreditScoreFactorDetail;
