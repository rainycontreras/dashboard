import { CustomerProfile } from "@/utils/type";
import {Box, ClickAwayListener, Divider, Stack, Theme, Typography, useMediaQuery } from "@mui/material";
import copyToClipboard from "copy-to-clipboard";
import React, { useCallback, useState } from "react";
import { AppIconButton, AppLink } from "..";
import ReferalCodeSocial from '@/components/ReferalCodeSharer/ReferalCodeSocial';
import ReferalCodeCopy from '@/components/ReferalCodeSharer/ReferalCodeCopy';

const ReferalCodeSharer: React.FC<{referralCode?: string}> = (props)=>{

    const smScreen = useMediaQuery((theme: Theme) => theme.breakpoints.up('sm'));

    return <Stack direction={smScreen ? 'row' : 'column'}>

        <ReferalCodeSocial referralCode={props.referralCode}/>

        <Divider
            flexItem
            orientation={smScreen ? 'vertical' : 'horizontal'}
            sx={{
                '&::after': {borderColor: 'app.antiflashWhite'},
                '&::before': {borderColor: 'app.antiflashWhite'},
                '& .MuiDivider-wrapper': {padding: 0},
            }}
        >
            <Box
                sx={{
                    border: '1px solid',
                    borderColor: 'app.antiflashWhite',
                    borderRadius: '50%',
                    padding: '0.375rem',
                }}
            >
                <Typography
                    sx={{
                        color: 'app.grey',
                        fontSize: '0.625rem',
                        fontWeight: '600',
                        opacity: '50%',
                    }}
                >
                    OR
                </Typography>
            </Box>
        </Divider>

        <ReferalCodeCopy referralCode={props.referralCode}/>


    </Stack>

}

export default ReferalCodeSharer;