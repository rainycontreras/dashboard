import {Box, ClickAwayListener, Stack, Theme, Typography, useMediaQuery } from "@mui/material";
import copyToClipboard from "copy-to-clipboard";
import React, {useCallback, useState } from "react";
import { AppIconButton } from "..";


interface referralCode {
    referralCode? : string
}

const ReferalCodeCopy: React.FC<referralCode> = (props)=>{

    const smScreen = useMediaQuery((theme: Theme) => theme.breakpoints.up('sm'));
    const [openCopiedTooltip, setOpenCopiedTooltip] = useState(false);

    //#region handlers
    const closeCopiedTooltipHandle = useCallback(() => {
        setOpenCopiedTooltip(false);
    }, []);
    const copyReferralCodeHandle = useCallback(() => {
        if (props.referralCode) {
            copyToClipboard(props.referralCode);
            setOpenCopiedTooltip(true);
            setTimeout(() => {
                setOpenCopiedTooltip(false);
            }, 1000);
        }
    }, [props.referralCode]);

    return   <Stack
        justifyContent="center"
        spacing={1}
        sx={{
            paddingLeft: {xs: '0', sm: '1rem'},
            paddingTop: {xs: '1rem', sm: '0'},
        }}
    >
        <Box justifyContent={smScreen ? 'start' : 'center'} sx={{display: 'flex'}}>
            <Typography
                variant="text"
                sx={{
                    fontSize: '0.875rem',
                    fontWeight: '500',
                }}
            >
                Copy Referral Code
            </Typography>
        </Box>
        <Stack justifyContent={smScreen ? 'start' : 'center'}>
            <Stack
                direction="row"
                sx={{
                    backgroundColor: 'app.lightGrey',
                    border: '1.5px dashed',
                    borderColor: 'app.lightPeriwinkle',
                    borderRadius: '6px',
                    marginTop: '0.5rem',
                }}
            >
                <Stack alignItems="center" direction="row" sx={{flexGrow: 1, padding: '0.5rem'}}>
                    <Typography
                        sx={{
                            color: 'secondary',
                            flexGrow: 1,
                            fontSize: '1rem',
                            fontWeight: '600',
                        }}
                        variant="h1"
                    >
                        {props.referralCode}
                    </Typography>
                </Stack>
                <ClickAwayListener onClickAway={closeCopiedTooltipHandle}>
                    <Box alignItems="center" justifyContent="center" sx={{display: 'flex'}}>
                        <AppIconButton
                            icon="Copy"
                            sx={{
                                height: '2.5rem',
                                width: '2.5rem',
                            }}
                            title={openCopiedTooltip ? 'Copied' : undefined}
                            onClick={copyReferralCodeHandle}
                        />
                    </Box>
                </ClickAwayListener>
            </Stack>
        </Stack>
    </Stack>

}

export default ReferalCodeCopy;