import {Box, Stack, Theme, Typography, useMediaQuery } from "@mui/material";
import React from "react";
import { AppIconButton, AppLink } from "..";

interface referralCode {
    referralCode? : string
}

const ReferalCodeSocial : React.FC<referralCode> = (props)=>{

    const smScreen = useMediaQuery((theme: Theme) => theme.breakpoints.up('sm'));

    return   <Stack
        justifyContent="center"
        spacing={1}
        sx={{
            paddingBottom: {xs: '1rem', sm: '0',},
            paddingRight: {xs: '0', sm: '1rem'},
        }}
    >
        <Box justifyContent={smScreen ? 'start' : 'center'} sx={{display: 'flex'}}>
            <Typography
                variant="text"
                sx={{
                    fontSize: '0.875rem',
                    fontWeight: '500',
                }}
            >
                Refer Now
            </Typography>
        </Box>
        <Stack justifyContent={smScreen ? 'start' : 'center'} direction="row">
            <AppIconButton
                component={AppLink}
                href={`https://www.facebook.com/sharer.php?u=https%3A%2F%2Fwww.mymoneykarma.com%2Ffast-home-loan.html%3FcampType%3DHOMELOAN%26utm_source%3D${props.referralCode}_facebook`}
                icon="Facebook"
                sx={{
                    margin: '0.5rem 0.5rem 0 0',
                    marginLeft: smScreen ? '0' : '0.5rem',
                    padding: '0',
                }}
            />
            <AppIconButton
                component={AppLink}
                href={`https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fwww.mymoneykarma.com%2Ffast-home-loan.html%3FcampType%3DHOMELOAN%26utm_source%3D${props.referralCode}_linkedin`}
                icon="LinkedIn"
                sx={{
                    margin: '0.5rem 0.5rem 0 0',
                    marginLeft: smScreen ? '0' : '0.5rem',
                    padding: '0',
                }}
            />
            <AppIconButton
                component={AppLink}
                href={`https://twitter.com/share?url=https%3A%2F%2Fwww.mymoneykarma.com%2Ffast-home-loan.html%3FcampType%3DHOMELOAN%26utm_source%3D${props.referralCode}_twitter&text=Mymoneykarma%20Home%20Loans`}
                icon="Twitter"
                sx={{
                    margin: '0.5rem 0.5rem 0 0',
                    marginLeft: smScreen ? '0' : '0.5rem',
                    padding: '0',
                }}
            />

        </Stack>
    </Stack>

}

export  default ReferalCodeSocial;