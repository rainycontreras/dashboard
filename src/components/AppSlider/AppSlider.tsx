import { Slider, SliderProps } from '@mui/material';
import { Box } from '@mui/system';
/**
 * Render's Slider using application visual design
 * @param {SliderProps} props - MUI Sliders props
 */
const AppSlider: React.FC<SliderProps> = (props) => {
  return (
    <Slider
      {...props}
      sx={{
        '& .MuiSlider-track': {
          background: `linear-gradient(90deg, rgba(29,161,242,1) 0%, rgba(41,239,132,1) 100%, rgba(0,212,255,1) 100%)`,
          borderRadius: '0.625rem',
          height: '0.375rem',
          border: '0',
        },
        '& .MuiSlider-thumb': {
          border: `0.25rem solid #29EF84`,
          backgroundColor: 'white',
          width: '1rem',
          height: '1rem',
          '&:focus, &:hover, &.Mui-active, &.Mui-focusVisible': {
            boxShadow: 'inherit',
          },
          '&:before': {
            display: 'none',
          },
        },
        '& .MuiSlider-rail': {
          borderRadius: '0.625rem',
          backgroundColor: 'app.border',
          height: '0.375rem',
          border: '0',
          opacity: 1,
        },
        '& .MuiSlider-valueLabel': {
          border: 0,
          backgroundColor: 'app.secondary',

          borderRadius: '0.375rem',
          padding: '0.4375 1.3125rem',
          boxShadow: '0px 0.375rem 0.375rem rgba(89, 100, 20, 0.13)',
          '&:focus': {
            outline: 'none',
          },
        },
      }}
    />
  );
};

export default AppSlider;
