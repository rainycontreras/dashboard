import AppIcon from 'src/components/AppIcon';
import ErrorBoundary from 'src/components/ErrorBoundary';
import CloseIcon from '@mui/icons-material/Close';
import MenuIcon from '@mui/icons-material/Menu';
import {
  Avatar,
  Box,
  Divider,
  IconButton,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  Stack,
  Typography,
} from '@mui/material';
import Logo from '../../icons/other/Logo';
import { useLogout } from '../../../hooks';
import { useCallback } from 'react';
import { useState } from 'react';
import { CustomerProfile } from '../../../utils/type';
import AppDrawer from '../../AppDrawer';
import NavBarMenuItem from './NavBarMenuItem';
import { COLOR_SIDEBAR_ICON } from 'src/components/icons';

interface NavBarProps {
  page?: string;
  profile?: CustomerProfile;
}

/**
 * Dashboard Navbar
 * Statically defined menu for customer
 * @param {CustomerProfile} profile - User's profile data
 */
const NavBar: React.FC<NavBarProps> = ({ page, profile }) => {
  const [openSideBar, setOpenSideBar] = useState(false);
  const [menuProfileAnchorEl, setMenuProfileAnchorEl] = useState(null);
  const [openProfileMenu, setOpenProfileMenu] = useState(false);
  const onLogout = useLogout();

  const onOpenSideBar = useCallback(() => {
    setOpenSideBar(true);
  }, [setOpenSideBar]);

  const onCloseSideBar = useCallback(() => {
    setOpenSideBar(false);
  }, [setOpenSideBar]);

  const onOpenProfileMenu = useCallback(
    (event) => {
      setOpenProfileMenu(true);
      setMenuProfileAnchorEl(event.currentTarget);
    },
    [setOpenProfileMenu]
  );

  const onCloseProfileMenu = useCallback(() => {
    setOpenProfileMenu(false);
    setMenuProfileAnchorEl(null);
  }, [setOpenProfileMenu]);

  return (
    <ErrorBoundary name="Navigation">
      {/* Mobile Header */}
      <Stack
        alignItems="center"
        direction="row"
        sx={{
          backgroundColor: 'secondary.main',
          display: { xs: 'flex', sm: 'none' },
          height: '3.5rem',
          paddingBottom: '0rem',
          paddingLeft: '1.5rem',
          paddingRight: '1.5rem',
          paddingTop: '0rem',
        }}
      >
        <Box alignItems="center" sx={{ display: 'flex', flexGrow: 1 }}>
          <Logo />
        </Box>
        <Box>
          <IconButton
            aria-label="upload picture"
            color="primary"
            component="span"
            disableRipple
            onClick={onOpenSideBar}
          >
            <MenuIcon sx={{ color: 'white' }} />
          </IconButton>
        </Box>
      </Stack>
      {/* End Mobile Header */}

      <AppDrawer open={openSideBar}>
        <Stack
          sx={{
            backgroundColor: 'secondary.main',
            height: '100%',
            width: '100%',
          }}
        >
          <Stack
            sx={{
              height: '100%',
              maxHeight: '100%',
              overflow: 'auto',
            }}
          >
            <Box sx={{ padding: '2rem' }}>
              <Stack alignItems="center" direction="row">
                <Box alignItems="center" sx={{ display: 'flex', flexGrow: 1 }}>
                  <Logo />
                </Box>
                <Box alignItems="center">
                  <IconButton
                    aria-label="upload picture"
                    color="primary"
                    component="span"
                    disableRipple
                    onClick={onCloseSideBar}
                    sx={{ display: { xs: 'flex', sm: 'none' }, flexGrow: 1 }}
                  >
                    <CloseIcon sx={{ color: 'white' }} />
                  </IconButton>
                </Box>
              </Stack>
            </Box>
            <nav style={{ flexGrow: 1 }}>
              <List sx={{ padding: 0 }}>
                <NavBarMenuItem href="/dashboard" icon="dashboard" selected={page === 'DASHBOARD'} title="Dashboard" />
                <NavBarMenuItem
                  href="/refer-and-earn"
                  icon="refer"
                  title="Refer & Earn"
                  selected={page === 'REFER_AND_EARN'}
                />
                <NavBarMenuItem icon="home" title="Home Loan" />
                <NavBarMenuItem
                  href="/credit-score-factors"
                  icon="creditscore"
                  selected={page === 'CREDIT_SCORE_FACTORS'}
                  title="CreditScore Factors"
                />
                <NavBarMenuItem icon="accounts" title="My Accounts" />
                <NavBarMenuItem count="3" icon="myapplications" title="My Applications" />
              </List>
              <List sx={{ padding: 0, paddingTop: '2rem' }}>
                <NavBarMenuItem href="/compare" title="Compare" />
                <NavBarMenuItem title="MoneyKarma" />
                <NavBarMenuItem title="Debt Analysis" />
                <NavBarMenuItem title="Learn" />
                <NavBarMenuItem title="Interest Saving" />
              </List>
              <Divider sx={{ backgroundColor: 'white', opacity: '0.1' }} />

              <List sx={{ padding: 0 }}>
                <NavBarMenuItem title="Home Loan" />
                <NavBarMenuItem title="Personal Loan" />
                <NavBarMenuItem title="Gold Loan" />
              </List>
            </nav>
            <List sx={{ padding: 0 }}>
              <ListItem disablePadding>
                <ListItemButton onClick={onOpenProfileMenu}>
                  <ListItemIcon>
                    <Avatar sx={{ padding: '0px' }}>{profile?.enteredFirstName?.substr(0, 1)}</Avatar>
                  </ListItemIcon>
                  <ListItemText
                    primary={profile ? `${profile?.enteredFirstName} ${profile?.enteredLastName}` : ''}
                    secondary={
                      <>
                        <Typography component="span" sx={{ color: 'app.ligthAliceBlue' }}>
                          View Profile
                        </Typography>
                      </>
                    }
                    sx={{ color: 'white' }}
                  />
                  <AppIcon name="ChevronDown" color={COLOR_SIDEBAR_ICON} />
                </ListItemButton>
              </ListItem>
            </List>

            <Menu
              anchorEl={menuProfileAnchorEl}
              open={openProfileMenu}
              PaperProps={{
                sx: {
                  width: '15.625rem',
                },
              }}
              onClose={onCloseProfileMenu}
            >
              <MenuItem>
                <Stack alignItems="center" direction="row" spacing={2} sx={{ width: '100%' }}>
                  <Avatar sx={{ padding: '0px' }}>{profile?.enteredFirstName?.substr(0, 1)}</Avatar>
                  <Stack sx={{ flexGrow: 1 }}>
                    <Typography component="span" variant="h1" sx={{ fontSize: '0.875rem' }}>
                      {profile ? `${profile?.enteredFirstName} ${profile?.enteredLastName}` : ''}
                    </Typography>
                  </Stack>
                  <AppIcon name="arrowright" />
                </Stack>
              </MenuItem>
              <Divider />
              <MenuItem>
                {/* TODO: We don't have "captionMenu" variant */}
                <Typography variant="captionMenu" sx={{ color: 'secondary' }}>
                  Consent Form
                </Typography>
              </MenuItem>
              <MenuItem>
                {/* TODO: We don't have "captionMenu" variant */}
                <Typography variant="captionMenu" sx={{ color: 'secondary' }}>
                  Pin Change
                </Typography>
              </MenuItem>
              <MenuItem>
                {/* TODO: We don't have "captionMenu" variant */}
                <Typography variant="captionMenu" sx={{ color: 'secondary' }}>
                  Product Suport
                </Typography>
              </MenuItem>
              <Divider />
              <MenuItem onClick={onLogout}>
                {/* TODO: We don;t have "logout" color */}
                {/* TODO: We don't have "captionMenu" variant */}
                <Typography variant="captionMenu" sx={{ color: 'primary.logout' }}>
                  Logout
                </Typography>
              </MenuItem>
            </Menu>
          </Stack>
        </Stack>
      </AppDrawer>

      {/* SideBar */}

      {/* End SideBar */}
    </ErrorBoundary>
  );
};

export default NavBar;
