import AppIcon from 'src/components/AppIcon';
import { Box, ListItem, ListItemButton, ListItemIcon, ListItemText, Typography } from '@mui/material';
import * as React from 'react';
import { COLOR_SIDEBAR_ICON } from 'src/components/icons';

interface NavBarMenuItemProps {
  count?: string;
  href?: string;
  icon?: string;
  selected?: boolean;
  title: string;
}

/**
 * Item for NavBar component
 */
const NavBarMenuItem: React.FC<NavBarMenuItemProps> = ({ count, href, icon, selected = false, title }) => {
  return (
    <ListItem
      disablePadding
      sx={{
        backgroundColor: selected ? 'primary.main' : 'secondary.main',
        paddingBottom: '0px',
        paddingTop: '0px',
      }}
    >
      <ListItemButton
        component="a"
        href={href}
        sx={{
          paddingLeft: icon ? '0.6rem' : '2rem',
          paddingRight: '2rem',
        }}
      >
        {icon && (
          <ListItemIcon sx={{ display: 'flex', justifyContent: 'center' }}>
            <Box
              alignItems="center"
              style={{
                display: 'flex',
                opacity: (selected ? 1.0 : 0.6) as number,
              }}
            >
              <AppIcon name={icon} color={COLOR_SIDEBAR_ICON} />
            </Box>
          </ListItemIcon>
        )}
        <ListItemText
          primary={
            // TODO: We don't have "menu" variant
            <Typography variant="menu" sx={{ display: 'flex', alignItems: 'center' }}>
              {title}
            </Typography>
          }
          sx={{ color: selected ? 'white' : 'app.ligthAliceBlue' }}
        />
        {count && (
          // Todo: We don't have "applications.color" color
          <Typography align="center" sx={{ color: 'applications.color', opacity: '0.6' }}>
            {count}
          </Typography>
        )}
      </ListItemButton>
    </ListItem>
  );
};

export default NavBarMenuItem;
