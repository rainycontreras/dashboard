/**
 * Dashboard header
 */
import { Stack } from '@mui/material';
import * as React from 'react';
import NavBar from './NavBar';

interface Props {
  children?: React.ReactNode;
}

/**
 * potentially @deprecated where we do use it?
 */
export const Header = (props: Props) => {
  return (
    <Stack direction="row" sx={{ height: '100vh' }}>
      <Stack direction={{ xs: 'column', md: 'row' }} sx={{ height: '100vh' }}>
        <NavBar />
      </Stack>
    </Stack>
  );
};
