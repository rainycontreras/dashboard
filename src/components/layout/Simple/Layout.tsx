import ErrorBoundary from 'src/components/ErrorBoundary';
import { Box, Stack } from '@mui/material';
import * as React from 'react';
import { CustomerProfile } from 'src/utils/type';
import NavBar from './NavBar';

interface LayoutProps {
  page?: string;
  profile?: CustomerProfile;
}

/**
 * General layout for Dashboard
 * @param {string} page - Current page
 * @param {CustomerProfile} profile - User's profile data
 */
const Layout: React.FC<LayoutProps> = ({ page, profile, children }) => {
  return (
    <Stack direction="row" sx={{ height: '100vh', width: '100%' }}>
      <Stack direction={{ xs: 'column', md: 'row' }} sx={{ height: '100vh', width: '100%' }}>
        <NavBar page={page} profile={profile} />
        <ErrorBoundary name="Content">
          <Box sx={{ paddingLeft: { xs: '0px', sm: '16rem' }, width: '100%' }}>
            <main>{children}</main>
          </Box>
        </ErrorBoundary>
      </Stack>
    </Stack>
  );
};

export default Layout;
