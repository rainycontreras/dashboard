import SmartLayout from './Smart/Layout';
import SimpleLayout from './Simple/Layout';

export { SmartLayout as default, SmartLayout, SimpleLayout };
