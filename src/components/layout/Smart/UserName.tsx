import React from 'react';
import { useCustomerProfile, useUserContext } from 'src/hooks';

interface Props {
  name?: string;
}

/**
 * Renders styled User's name. If no .name specified, currently logged user's first and last name is used.
 * @component UserName
 * @param {string} [name] - optional user name
 */
const UserName: React.FC<Props> = ({ name, ...restOfProps }) => {
  // TODO: Refactor to useAppStore() or other hook
  const userContext = useUserContext();
  const profile = useCustomerProfile(userContext);
  const textToRender = name || (profile && `${profile?.enteredFirstName} ${profile?.enteredLastName}`) || 'Unknown';
  return <span {...restOfProps}>{textToRender}</span>;
};

export default UserName;
