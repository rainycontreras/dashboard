import { COLOR_SIDEBAR_ICON } from 'src/components/icons';
import { List, ListItem, ListItemButton, ListItemIcon, ListItemText, Typography } from '@mui/material';
import React from 'react';
import { AppIcon } from '../..';
import UserAvatar from './UserAvatar';
import UserName from './UserName';

interface Props {
  onClick: React.MouseEventHandler<HTMLInputElement>;
}

/**
 * Renders "Profile" navigation item with user Avatar and Context Menu
 * @component ProfileNavItem
 */
const ProfileNavItem: React.FC<Props> = ({ onClick }) => {
  return (
    <List sx={{ padding: 0 }}>
      <ListItem disablePadding>
        <ListItemButton onClick={onClick}>
          <ListItemIcon>
            <UserAvatar />
          </ListItemIcon>
          <ListItemText
            primary={<UserName />}
            secondary={
              <Typography component="span" sx={{ color: 'app.ligthAliceBlue' }}>
                View Profile
              </Typography>
            }
            sx={{ color: 'white' }}
          />
          <AppIcon name="ChevronDown" color={COLOR_SIDEBAR_ICON} />
        </ListItemButton>
      </ListItem>
    </List>
  );
};

export default ProfileNavItem;
