import ErrorBoundary from 'src/components/ErrorBoundary';
import CloseIcon from '@mui/icons-material/Close';
import { Box, Divider, IconButton, List, Stack } from '@mui/material';
import Logo from '../../icons/other/Logo';
import { useCallback } from 'react';
import { useState } from 'react';
import AppDrawer from '../../AppDrawer';
import NavItem from './NavItem';
import MobileHeader from './MobileHeader';
import ProfileNavItem from './ProfileNavItem';
import ProfileContextMenu from './ProfileContextMenu';

/**
 * Smart navigation bar detects current user, url, etc. and renders the appropriate menu composition
 * @component SmartNavBar
 */
const SmartNavBar = () => {
  const [openSideBar, setOpenSideBar] = useState(false);
  const [profileMenuAnchorEl, setProfileMenuAnchorEl] = useState(null);
  const [profileMenuOpen, setProfileMenuOpen] = useState(false);

  const applicationCount = 3; // TODO: get from hook or API

  const onOpenSideBar = useCallback(() => {
    setOpenSideBar(true);
  }, [setOpenSideBar]);

  const onCloseSideBar = useCallback(() => {
    setOpenSideBar(false);
  }, [setOpenSideBar]);

  const onOpenProfileMenu = useCallback(
    (event) => {
      setProfileMenuOpen(true);
      setProfileMenuAnchorEl(event.currentTarget);
    },
    [setProfileMenuOpen]
  );

  const onCloseProfileMenu = useCallback(() => {
    setProfileMenuOpen(false);
    setProfileMenuAnchorEl(null);
  }, [setProfileMenuOpen]);

  return (
    <ErrorBoundary name="Navigation">
      <MobileHeader onMenuClick={onOpenSideBar} />

      <AppDrawer open={openSideBar}>
        <Stack
          sx={{
            backgroundColor: 'secondary.main',
            height: '100%',
            width: '100%',
          }}
        >
          <Stack
            sx={{
              height: '100%',
              maxHeight: '100%',
              overflow: 'auto',
            }}
          >
            <Box sx={{ padding: '2rem' }}>
              <Stack alignItems="center" direction="row">
                <Box alignItems="center" sx={{ display: 'flex', flexGrow: 1 }}>
                  <Logo />
                </Box>
                <Box alignItems="center">
                  {/* TODO: Replace with AppIconButton */}
                  <IconButton
                    aria-label="close sidebar"
                    color="primary"
                    component="span"
                    disableRipple
                    onClick={onCloseSideBar}
                    sx={{ display: { xs: 'flex', sm: 'none' }, flexGrow: 1 }}
                  >
                    <CloseIcon sx={{ color: 'white' }} />
                  </IconButton>
                </Box>
              </Stack>
            </Box>

            <nav style={{ flexGrow: 1 }}>
              <List sx={{ padding: 0 }}>
                <NavItem href="/dashboard" icon="dashboard" title="Dashboard" />
                <NavItem href="/dashboard/#/refer-friend" icon="refer" title="Refer & Earn" />
                <NavItem href="/hlonline/new/hl/" icon="home" title="Home Loan" />
                <NavItem href="/credit-score-factors" icon="scorefactors" title="CreditScore Factors" />
                <NavItem href="/dashboard/#/accounts" icon="accounts" title="My Accounts" />
                <NavItem href="/???" icon="myapplications" title="My Applications" count={applicationCount} />
              </List>
              <List sx={{ padding: 0, paddingTop: '2rem' }}>
                <NavItem href="/dashboard/#/compare" title="Compare" />
                <NavItem href="/dashboard/#/moneykarma" title="MoneyKarma" />
                <NavItem href="/dashboard/#/mydebt/total" title="Debt Analysis" />
                <NavItem href="/learning-center.html" title="Learn" />
                <NavItem href="/???" title="Interest Saving" />
              </List>
              <Divider sx={{ backgroundColor: 'white', opacity: '0.1' }} />

              <List sx={{ padding: 0 }}>
                <NavItem href="/hlonline/new/hl/" title="Home Loan" />
                <NavItem href="/plonline/" title="Personal Loan" />
                <NavItem href="/dashboard/#/gold-loan-online" title="Gold Loan" />
              </List>
            </nav>

            <ProfileNavItem onClick={onOpenProfileMenu} />
            <ProfileContextMenu anchorEl={profileMenuAnchorEl} open={profileMenuOpen} onClose={onCloseProfileMenu} />
          </Stack>
        </Stack>
      </AppDrawer>
    </ErrorBoundary>
  );
};

export default SmartNavBar;
