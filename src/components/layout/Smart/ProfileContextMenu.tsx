import React from 'react';
import { Divider, Menu, MenuItem, Stack, Typography } from '@mui/material';
import { useLogout } from 'src/hooks';
import { AppIcon } from '../..';
import UserAvatar from './UserAvatar';
import UserName from './UserName';

interface Props {
  anchorEl: null | Element | ((element: Element) => Element);
  open: boolean;
  onClose: React.MouseEventHandler<HTMLInputElement>;
}

/**
 * Renders "Context Menu" with User Profile, Logout, and other items
 * @component ProfileContextMenu
 */
const ProfileContextMenu: React.FC<Props> = ({ anchorEl, open, onClose }) => {
  const onLogout = useLogout();
  return (
    <Menu
      anchorEl={anchorEl}
      open={open}
      PaperProps={{
        sx: {
          width: '15.625rem',
        },
      }}
      onClose={onClose}
    >
      <MenuItem>
        <Stack alignItems="center" direction="row" spacing={2} sx={{ width: '100%' }}>
          <UserAvatar />
          <Stack sx={{ flexGrow: 1 }}>
            <Typography component="span" variant="h1" sx={{ fontSize: '0.875rem' }}>
              <UserName />
            </Typography>
          </Stack>
          <AppIcon name="ArrowRight" />
        </Stack>
      </MenuItem>
      <Divider />
      <MenuItem>
        {/* TODO: We don't have "captionMenu" variant */}
        <Typography variant="captionMenu" sx={{ color: 'secondary' }}>
          Consent Form
        </Typography>
      </MenuItem>
      <MenuItem>
        {/* TODO: We don't have "captionMenu" variant */}
        <Typography variant="captionMenu" sx={{ color: 'secondary' }}>
          Pin Change
        </Typography>
      </MenuItem>
      <MenuItem>
        {/* TODO: We don't have "captionMenu" variant */}
        <Typography variant="captionMenu" sx={{ color: 'secondary' }}>
          Product Support
        </Typography>
      </MenuItem>
      <Divider />
      <MenuItem onClick={onLogout}>
        {/* TODO: We don't have "captionMenu" variant */}
        <Typography variant="captionMenu" sx={{ color: 'app.lightCarminePink' }}>
          Logout
        </Typography>
      </MenuItem>
    </Menu>
  );
};

export default ProfileContextMenu;
