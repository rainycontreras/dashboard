import React from 'react';
import { Avatar } from '@mui/material';
import { useCustomerProfile, useUserContext } from 'src/hooks';

interface Props {
  name?: string;
}

/**
 * Renders Avatar using first char of given name. If no name specified, currently logged user's name is used.
 * @component UserAvatar
 * @param {string} [name] - optional user name
 */
const UserAvatar: React.FC<Props> = ({ name }) => {
  // TODO: Refactor to useAppStore() or other hook
  const userContext = useUserContext();
  const profile = useCustomerProfile(userContext);
  const charToRender = name?.charAt(0) || profile?.enteredFirstName?.charAt(0) || '?';
  return <Avatar sx={{ padding: '0px' }}>{charToRender}</Avatar>;
};

export default UserAvatar;
