import AppIcon from 'src/components/AppIcon';
import { Box, ListItem, ListItemButton, ListItemIcon, ListItemText, Typography } from '@mui/material';
import * as React from 'react';
import { useRouter } from 'next/router';

interface Props {
  count?: number;
  href?: string;
  icon?: string;
  selected?: boolean;
  title: string;
}

/**
 * Smart Navigation Item, detects current url and sets selected state
 * @component SmartNavItem
 */
const NavItem: React.FC<Props> = ({ count, href = '/', icon, selected: propSelected = false, title }) => {
  const router = useRouter();
  const selected = propSelected || router.pathname.includes(href);

  return (
    <ListItem
      disablePadding
      sx={{
        backgroundColor: selected ? 'primary.main' : 'secondary.main',
        paddingBottom: '0px',
        paddingTop: '0px',
      }}
    >
      <ListItemButton
        component="a"
        href={href}
        sx={{
          paddingLeft: icon ? '0.6rem' : '2rem',
          paddingRight: '2rem',
        }}
      >
        {icon && (
          <ListItemIcon sx={{ display: 'flex', justifyContent: 'center' }}>
            <Box
              alignItems="center"
              style={{
                display: 'flex',
                opacity: (selected ? 1.0 : 0.6) as number,
              }}
            >
              <AppIcon name={icon} />
            </Box>
          </ListItemIcon>
        )}
        <ListItemText
          primary={
            // TODO: We don't have "menu" variant
            <Typography variant="menu" sx={{ display: 'flex', alignItems: 'center' }}>
              {title}
            </Typography>
          }
          sx={{ color: selected ? 'white' : 'app.ligthAliceBlue' }}
        />
        {count && (
          <Typography align="center" sx={{ color: 'app.tertiary', opacity: '0.6' }}>
            {count}
          </Typography>
        )}
      </ListItemButton>
    </ListItem>
  );
};

export default NavItem;
