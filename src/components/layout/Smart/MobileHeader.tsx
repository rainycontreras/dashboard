import React from 'react';
import { Box, IconButton, Stack } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu'; // TODO: Replace with AppIcon or AppIconButton by name
import Logo from 'src/components/icons/other/Logo'; // TODO: it is not icon, so move to somewhere else

interface Props {
  onMenuClick: React.MouseEventHandler<HTMLInputElement>;
}

/**
 * Renders Header with logo and "Hamburger menu" for small screens
 * @component MobileHeader
 * @param {function} onMenuClick - callback to be called when menu button is clicked
 */
const MobileHeader: React.FC<Props> = ({ onMenuClick }) => {
  return (
    <Stack
      alignItems="center"
      direction="row"
      sx={{
        backgroundColor: 'secondary.main',
        display: { xs: 'flex', sm: 'none' },
        height: '3.5rem',
        paddingBottom: '0rem',
        paddingLeft: '1.5rem',
        paddingRight: '1.5rem',
        paddingTop: '0rem',
      }}
    >
      <Box alignItems="center" sx={{ display: 'flex', flexGrow: 1 }}>
        <Logo />
      </Box>
      <Box>
        {/* TODO: Replace with AppIconButton */}
        <IconButton aria-label="main menu" color="primary" component="span" disableRipple onClick={onMenuClick}>
          <MenuIcon sx={{ color: 'white' }} />
        </IconButton>
      </Box>
    </Stack>
  );
};

export default MobileHeader;
