import React from 'react';
import ErrorBoundary from 'src/components/ErrorBoundary';
import { Box, Stack } from '@mui/material';
import SmartNavBar from './NavBar';

interface Props {
  children: React.ReactNode;
}

/**
 * Application layout with Smart navigation bar
 * @component SmartLayout
 */
const SmartLayout: React.FC<Props> = ({ children }) => {
  return (
    <Stack direction="row" sx={{ height: '100vh', width: '100%' }}>
      <Stack direction={{ xs: 'column', md: 'row' }} sx={{ height: '100vh', width: '100%' }}>
        <SmartNavBar />
        <ErrorBoundary name="Content">
          <Box sx={{ paddingLeft: { xs: '0px', sm: '16rem' }, width: '100%' }}>
            <main>{children}</main>
          </Box>
        </ErrorBoundary>
      </Stack>
    </Stack>
  );
};

export default SmartLayout;
