import { Box, Stack, Typography } from '@mui/material';

interface ColloredBarItemProps {
  color: string;
  displayLabel?: boolean;
  displayPointer: boolean;
  displayMax?: boolean;
  displayValueRange?: boolean;
  height?: number;
  pointer?: React.ReactElement;
  label?: string;
  ltr?: boolean;
  max: number;
  min: number;
  value?: number;
}

/**
 * Each item for collored bar component
 */
const ColloredBarItem: React.FC<ColloredBarItemProps> = ({
  color,
  displayLabel = false,
  displayPointer = false,
  displayMax = false,
  displayValueRange = true,
  height = 3,
  pointer,
  label,
  ltr,
  max = 1,
  min = max, //min value have to be lower than or equal to max value
  value = 0,
}) => {
  if (min > max) {
    throw new Error('The min property value has to be lower than or equal to the max property value.');
  }

  const percentageValue = min === max ? 100 : ((value - min) / (max - min)) * 100;
  const pointerWidth = (10 * height) / 3;

  return (
    <Stack justifyContent="end" direction="column">
      {displayValueRange && (
        <Stack direction="row" justifyContent="space-between">
          <Typography variant="tiny">{min}</Typography>
          {displayMax && <Typography variant="tiny">{max}</Typography>}
        </Stack>
      )}
      <Box alignItems="center" sx={{ height: '10px', display: 'flex', position: 'relative' }}>
        <Box
          sx={{
            backgroundColor: color,
            borderRadius: `${height}px`,
            height: `${height}px`,
            width: '100%',
          }}
        />
        {displayPointer && !pointer && (
          <Box
            sx={{
              backgroundColor: 'white',
              border: `${height}px solid ${color}`,
              borderRadius: `27px`,
              boxSizing: 'border-box',
              height: `${pointerWidth}px`,
              position: 'absolute',
              width: `${pointerWidth}px`,
              left: `calc(${ltr ? 100 - percentageValue : percentageValue}% - ${pointerWidth / 2}px )`,
            }}
          />
        )}
        {displayPointer && pointer && (
          <Box
            alignItems="center"
            justifyContent="center"
            sx={{
              backgroundColor: 'white',
              boxSizing: 'border-box',
              borderRadius: `27px`,
              display: 'flex',
              height: `${pointerWidth}px`,
              position: 'absolute',
              width: `${pointerWidth}px`,
              left: `calc(${ltr ? 100 - percentageValue : percentageValue}% - ${pointerWidth / 2}px )`,
              boxShadow: '0px 2px 2px rgba(29, 33, 69, 0.03), 0px 6px 12px rgba(29, 33, 69, 0.08)',
            }}
          >
            {pointer}
          </Box>
        )}
      </Box>
      {displayLabel && (
        <Stack direction="row" justifyContent="space-between" sx={{ paddingTop: '0.25rem' }}>
          <Typography variant="tiny" sx={{ fontWeight: 600, fontSize: '0.825rem' }}>
            {label}
          </Typography>
        </Stack>
      )}
    </Stack>
  );
};

export default ColloredBarItem;
