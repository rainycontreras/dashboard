import { Box } from '@mui/material';
import { useMemo } from 'react';
import { ColoredBarTier } from 'src/utils/type';
import ColloredBarItem from './ColoredBarItem';
interface ColloredBarProps {
  displayLabel?: boolean;
  displayMax?: boolean;
  displayValueRange?: boolean;
  distribution?: string;
  height?: number;
  pointer?: React.ReactElement;
  ltr?: boolean;
  tiers?: ColoredBarTier[];
  value?: number;
}

/**
 * Collored Bar component
 * @component ColloredBar
 */
const ColloredBar: React.FC<ColloredBarProps> = ({
  value = 0,
  height,
  displayLabel,
  displayValueRange,
  distribution,
  pointer,
  ltr = false,
  tiers,
}) => {
  const templateColumns = useMemo(() => {
    if (distribution) {
      return distribution;
    }
    if (!tiers) {
      return '';
    }
    return `repeat(${tiers.length}, 1fr)`;
  }, [distribution, tiers]);

  const tiersToShow = useMemo(() => {
    if (!tiers) {
      return [];
    }
    let tiersToShow = [...tiers];
    if (ltr) {
      tiersToShow.reverse();
    }
    return tiersToShow;
  }, [ltr, tiers]);

  return (
    <Box
      sx={{
        width: '100%',
        paddingTop: '2rem',
        display: 'grid',
        gridTemplateColumns: templateColumns,
        gridColumnGap: '0.5rem',
      }}
    >
      {tiersToShow?.map((part, index) => {
        return (
          <ColloredBarItem
            key={part.color}
            color={part.color}
            displayLabel={displayLabel}
            displayPointer={
              value != null &&
              value >= part.min &&
              (value < part.max ||
                (value === part.max && part.min === part.max) ||
                (value === part.max && ((!ltr && index === tiersToShow.length - 1) || (ltr && index === 0))))
            }
            displayMax={index >= (tiersToShow?.length || 0) - 1}
            displayValueRange={displayValueRange}
            height={height}
            label={part.label}
            max={part.max}
            min={part.min}
            ltr={ltr}
            value={value}
            pointer={pointer}
          />
        );
      })}
    </Box>
  );
};

export default ColloredBar;
