import { formatDate, formatDuration } from '@/utils/date';
import { formatMoney } from '@/utils/number';
import { AccountDetails } from '@/utils/type';
import { Grid } from '@mui/material';
import AccountDetailField from '../AccountDetailField';
interface Props {
  account?: AccountDetails;
}

const AccountDetailReport: React.FC<Props> = ({ account }) => {
  return (
    <>
      <Grid item xs={6} md={3}>
        <AccountDetailField label="Last Reported" value={formatDate(account?.lastReportedDate, '')} />
      </Grid>
      <Grid item xs={6} md={3}>
        <AccountDetailField label="Creditor Name" value={account?.institution ?? ''} />
      </Grid>
      <Grid item xs={6} md={3}>
        <AccountDetailField label="Account Type" value={account?.type ?? ''} />
      </Grid>
      <Grid item xs={6} md={3}>
        <AccountDetailField label="Account Status" value={account?.status ?? ''} />
      </Grid>
      <Grid item xs={6} md={3}>
        <AccountDetailField label="Opened Date" value={formatDate(account?.openedDate, '')} />
      </Grid>
      <Grid item xs={6} md={3}>
        <AccountDetailField label="Closed Date" value={formatDate(account?.closedDate, '---')} />
      </Grid>
      <Grid item xs={6} md={3}>
        <AccountDetailField label="Limit" value={formatMoney(account?.creditLimit) ?? '---'} />
      </Grid>
      <Grid item xs={6} md={3}>
        <AccountDetailField label="Term" value={formatDuration(account?.term, '---')} />
      </Grid>
      <Grid item xs={6} md={3}>
        <AccountDetailField label="Monthly Payment" value={'---'} />
      </Grid>
      <Grid item xs={6} md={3}>
        <AccountDetailField label="Responsibility" value={account?.responsibility ?? '---'} />
      </Grid>
      <Grid item xs={6} md={3}>
        <AccountDetailField label="Balance" value={formatMoney(account?.balance) ?? '---'} />
      </Grid>
      <Grid item xs={6} md={3}>
        <AccountDetailField label="Highest Balance" value={formatMoney(account?.highCredit) ?? '---'} />
      </Grid>

      <Grid item xs={6} md={3}>
        <AccountDetailField
          label="Times Late (60/60/90)"
          value={`${account?.missedPayments30Days ?? ''}/ ${account?.missedPayments60Days ?? ''} /${
            account?.missedPayments90Days ?? ''
          }`}
        />
      </Grid>
      <Grid item xs={6} md={3}>
        <AccountDetailField label="Remarks" value={account?.remarks ?? '---'} />
      </Grid>
    </>
  );
};

export default AccountDetailReport;
