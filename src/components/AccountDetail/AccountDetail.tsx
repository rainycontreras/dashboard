import { Box, Grid, Stack, Typography, useMediaQuery, Theme } from '@mui/material';
import { PaletteOptions, useTheme } from '@mui/material/styles';
import { useCallback, useState, useMemo } from 'react';
import AccountDetailField from 'src/components/AccountDetailField';
import AccountPaymentCalendar from 'src/components/AccountPaymentCalendar';
import { formatDate } from 'src/utils/date';
import { formatMoney } from 'src/utils/number';
import {
  AccountDetailFieldAligment,
  AccountDetails,
  AccountHeaderProps,
  CreditScoreFactorDisplayType,
} from 'src/utils/type';
import DotIcon from 'src/components/icons/other/DotIcon';
import React from 'react';
import { AppButton } from 'src/components/AppButton';
import AccountDetailFactor from './AccountDetailFactor';
import AccountDetailReport from './AccountDetailReport';

interface AccountDetailProps {
  header: React.ElementType<AccountHeaderProps>;
  account: AccountDetails;
  variant?: 'creditfactor' | 'creditreport';
}

/**
 * Account detail
 * @component AccountDetail
 */

const AccountDetail: React.FC<AccountDetailProps> = ({ header, account, variant = 'creditfactor' }) => {
  const theme = useTheme();
  const lgScreen = useMediaQuery((theme: Theme) => theme.breakpoints.up('lg'));
  const [detailOpened, setDetailOpened] = useState(false);
  const value = useMemo(() => {
    if (!account) return 0;
    if (!account.creditLimit) return 0;
    return (account.balance ?? 0) / account.creditLimit;
  }, [account]);

  const acountPaymentStatus = useMemo(() => {
    const appColorSpace = (theme.palette as PaletteOptions)?.app;
    if (account?.totalMissedPayments === 0) {
      return { label: 'Good', color: appColorSpace?.green };
    } else if (account?.totalMissedPayments <= 10) {
      return { label: 'Poor', color: appColorSpace?.crayola };
    } else {
      return { label: 'Bad', color: appColorSpace?.colorBar.veryBad };
    }
  }, [account?.totalMissedPayments, theme]);

  const accountStatusColor = (theme.palette as PaletteOptions)?.app?.green;

  const openCloseDetailHandle = useCallback(() => {
    setDetailOpened((status) => !status);
  }, []);

  return (
    <Stack
      sx={{
        border: '1px solid',
        borderColor: 'app.border',
        borderRadius: '0.375rem',
      }}
    >
      {
        //#region header
      }
      {React.createElement(header, { account, detailsOpen: detailOpened, onOpenCloseDetails: openCloseDetailHandle })}

      {
        //#endregion header
      }
      {detailOpened && (
        <Stack>
          {account.displayType === CreditScoreFactorDisplayType.PERCENTAGE && (
            <Stack
              spacing={2}
              sx={{
                backgroundColor: 'app.lightGrey',
                padding: '1.5rem',
                borderBottom: '1px solid',
                borderBottomColor: 'app.border',
                borderTop: '1px solid',
                borderTopColor: 'app.border',
              }}
            >
              <Typography variant="label1">Overview</Typography>
              {
                //TODO: Refactor slider with AppSlider PR: https://bitbucket.org/mymoneykarma/dashboard/pull-requests/34
              }
              <Box sx={{ position: 'relative' }}>
                <Box sx={{ borderRadius: '10px', backgroundColor: 'app.border', height: '0.375rem', width: '100%' }} />
                <Box
                  sx={{
                    borderRadius: '10px',
                    background: `linear-gradient(90deg, rgba(29,161,242,1) 0%, rgba(41,239,132,1) 100%, rgba(0,212,255,1) 100%)`,
                    height: '0.375rem',
                    position: 'absolute',
                    top: '0',
                    left: '0',
                    width: `${value * 100}%`,
                    zIndex: '1',
                  }}
                />
                <Box
                  sx={{
                    boxSizing: 'border-box',
                    top: '-0.3rem',
                    left: `calc(${value * 100}% - 0.5rem)`,
                    position: 'absolute',
                    borderRadius: '28px',
                    border: `4px solid #29EF84`, //TODO: move colors out this file
                    backgroundColor: 'white',
                    width: '1rem',
                    height: '1rem',
                    zIndex: 2,
                  }}
                />
              </Box>
              <Stack justifyContent="space-between" direction="row">
                <AccountDetailField label="Credit Limit" value={formatMoney(account?.creditLimit, '')} />
                <AccountDetailField
                  valueAligment={AccountDetailFieldAligment.RIGHT}
                  label="Outstanding Balance"
                  value={formatMoney(account?.balance, '')}
                />
              </Stack>
            </Stack>
          )}
          <Stack padding={{ padding: '1.5rem' }}>
            <Stack spacing={2}>
              <Stack alignItems="center" direction="row" justifyContent="space-between">
                <Typography variant="label1">Account Details</Typography>
                {variant === 'creditfactor' && (
                  <Box
                    sx={{
                      backgroundColor: `${accountStatusColor}1A`,
                      display: 'inline-block',
                      padding: '0.125rem 0.75rem',
                      borderRadius: '1.25rem',
                    }}
                  >
                    {/* TODO: Make "Bullet" component for this */}
                    <DotIcon color={accountStatusColor} size={3} />
                    <Typography sx={{ color: accountStatusColor }} variant="label2">
                      Current
                    </Typography>
                  </Box>
                )}
              </Stack>
              <Box>
                <Grid container spacing={2}>
                  {variant === 'creditfactor' && <AccountDetailFactor account={account} />}
                  {variant === 'creditreport' && <AccountDetailReport account={account} />}
                  <Grid item xs={12} md={12}></Grid>
                  <Grid item xs={12} md={6}>
                    <AccountPaymentCalendar paymentCalendar={account?.paymentCalendar} />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <Grid container spacing={2}>
                      <Grid item xs={6} md={6}>
                        <AccountDetailField label="Last Payment" value={formatDate(account?.lastPaymentDate, '---')} />
                      </Grid>
                      <Grid item xs={6} md={6}>
                        <AccountDetailField label="Payment Status" value={account?.paymentStatus ?? '---'} />
                      </Grid>
                      <Grid item xs={6} md={6}>
                        <AccountDetailField
                          label="Amount Past Due"
                          value={formatMoney(account?.amountPastDue, '---')}
                        />
                      </Grid>
                      <Grid item xs={6} md={6}>
                        <AccountDetailField label="Worst Payment Status" value={account?.worstPaymentStatus ?? '---'} />
                      </Grid>
                    </Grid>
                  </Grid>
                  <Grid item xs={12} md={12}>
                    <Stack
                      alignItems={lgScreen ? 'center' : 'start'}
                      justifyContent="space-between"
                      direction={lgScreen ? 'row' : 'column'}
                      sx={{
                        backgroundColor: 'app.lightGrey',
                        borderRadius: '.375rem',
                        padding: '1.125rem 1.5rem',
                      }}
                      spacing={lgScreen ? 0 : 2}
                    >
                      <Stack spacing={0.5}>
                        <Typography variant="text" sx={{ fontWeight: 600 }}>
                          Report an error
                        </Typography>
                        <Typography variant="label2" sx={{ opacity: 0.6, fontWeight: 400, fontSize: '.875rem' }}>
                          Notice something wrong? You can report an error to Equifax
                        </Typography>
                      </Stack>
                      <AppButton
                        href="http://www.equifax.co.in/consumer/forms/dispute_resolution/en_in"
                        text="Report Error"
                      />
                    </Stack>
                  </Grid>
                </Grid>
              </Box>
            </Stack>
          </Stack>
        </Stack>
      )}
    </Stack>
  );
};

export default AccountDetail;
