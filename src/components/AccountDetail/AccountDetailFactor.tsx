import { formatDate, formatDuration } from '@/utils/date';
import { formatMoney } from '@/utils/number';
import { AccountDetails } from '@/utils/type';
import { Grid } from '@mui/material';
import AccountDetailField from '../AccountDetailField';
interface Props {
  account?: AccountDetails;
}

const AccountDetailFactor: React.FC<Props> = ({ account }) => {
  return (
    <>
      <Grid item xs={6} md={3}>
        <AccountDetailField label="Last Reported" value={formatDate(account?.lastReportedDate, '')} />
      </Grid>
      <Grid item xs={6} md={3}>
        <AccountDetailField label="Opened Date" value={formatDate(account?.openedDate, '')} />
      </Grid>
      <Grid item xs={6} md={3}>
        <AccountDetailField label="Closed Date" value={formatDate(account?.closedDate, '---')} />
      </Grid>
      <Grid item xs={6} md={3}>
        <AccountDetailField label="Remarks" value={account?.remarks ?? '---'} />
      </Grid>
      <Grid item xs={6} md={3}>
        <AccountDetailField label="Responsibility" value={account?.responsibility ?? '---'} />
      </Grid>
      <Grid item xs={6} md={3}>
        <AccountDetailField label="Term" value={formatDuration(account?.term, '---')} />
      </Grid>
      <Grid item xs={6} md={3}>
        <AccountDetailField label="Highest Balance" value={formatMoney(account?.highCredit) ?? '---'} />
      </Grid>
      <Grid item xs={6} md={3}>
        <AccountDetailField
          label="Times Late (60/60/90)"
          value={`${account?.missedPayments30Days ?? ''}/ ${account?.missedPayments60Days ?? ''} /${
            account?.missedPayments90Days ?? ''
          }`}
        />
      </Grid>
      <Grid item xs={6} md={3}>
        <AccountDetailField label="Monthly Payment" value={formatMoney(account?.monthlyPayment) ?? '---'} />
      </Grid>
      <Grid item xs={6} md={3}>
        <AccountDetailField label="Due from Last Payment" value={formatMoney(account?.dueFromLastPayment) ?? '---'} />
      </Grid>
    </>
  );
};

export default AccountDetailFactor;
