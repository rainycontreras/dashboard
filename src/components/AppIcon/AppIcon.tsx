// MUI Icons
import DefaultIcon from '@mui/icons-material/MoreHoriz';
// Application Icons
import CreditScoreIcon from 'src/components/icons/CreditScore';
import AccountsIcon from 'src/components/icons/Accounts';
import DashboardIcon from 'src/components/icons/Dashboard';
import MyApplicationsIcon from 'src/components/icons/MyApplications';
import CheckIcon from 'src/components/icons/Check';
import ReferIcon from 'src/components/icons/Refer';
import CompareIcon from 'src/components/icons/Compare';
import HomeIcon from '../icons/Home';
import CloseIcon from '../icons/Close';
import ChevronRightIcon from '../icons/ChevronRight';
import ChevronUpIcon from '../icons/ChevronUp';
import ChevronDownIcon from '../icons/ChevronDown';
import UserCircleIcon from '../icons/UserCircle';
import HigherLoansIcon from 'src/components/icons/HigherLoans';
import PercentageIcon from 'src/components/icons/Percentage';
import MyAccountIcon from '../icons/MyAccount';
import AddIcon from '../icons/Add';
import TimeIcon from '../icons/Time';
import PhoneIcon from '../icons/Phone';
import CheckCircleIcon from '../icons/CheckCircle';
import ArrowsIcon from '../icons/Arrows';
import PiggyBankIcon from '../icons/PiggyBank';
import CalendarIcon from '../icons/Calendar';
import PaperIcon from '../icons/Paper';
import CardIcon from '../icons/Card';
import CurrenciesIcon from '../icons/Currencies';
import BusinessIcon from '../icons/Business';
import PaymentErrorIcon from '../icons/PaymentError';
import IncreaseCreditScoreIcon from '../icons/IncreaseCreditScore';
import EnquiryIcon from '../icons/Enquiry';
import LoansIcon from '../icons/Loans';
import BillIcon from '../icons/Bill';
import BankIcon from '../icons/Bank';
import ErrorIcon from '../icons/Error';
import ArrowLeftIcon from '../icons/ArrowLeft';
import ArrowRightIcon from '../icons/ArrowRight';
import ChatIcon from '../icons/Chat';
import ChevronLeftIcon from '../icons/ChevronLeft';
import CopyIcon from '../icons/Copy';
import MenuIcon from '../icons/Menu';
import PinIcon from '../icons/Pin';
import RupeeIcon from '../icons/Rupee';
import TickBadgeIcon from '../icons/TickBage';
import FacebookIcon from '../icons/social/Facebook';
import LinkedInIcon from '../icons/social/LinkedIn';
import TwitterIcon from '../icons/social/Twitter';
import WhatsAppIcon from '../icons/social/WhatsApp';
import Forward from 'src/components/icons/Forward';
import Backward from 'src/components/icons/Backward';
import CalendarPaymentDone from 'src/components/icons/calendarPayment/Done';
import CalendarPaymentNew from 'src/components/icons/calendarPayment/New';
import CalendarPaymentNoInformation from 'src/components/icons/calendarPayment/NoInformation';
import CalendarPaymentNoPayment from 'src/components/icons/calendarPayment/NoPayment';
import ShieldProtected from 'src/components/icons/other/ShieldProtected';
import ShieldUnprotected from 'src/components/icons/other/ShieldUnprotected';
import Chart from '../icons/Chart';

/**
 * How to use:
 * 1. Import all required MUI or other SVG icons into this file.
 * 2. Add icons with "unique lowercase names" into ICONS object.
 * 3. Use icons everywhere in the App by their names in <AppIcon name="xxx" /> component
 * Important: properties of ICONS object MUST be lowercase!
 * Note: You can use camelCase or UPPERCASE in the <AppIcon name="someIconByName" /> component
 */
export const ICONS: Record<string, React.ComponentType> = {
  // MUI Icons
  default: DefaultIcon,
  // Application Icons
  accounts: AccountsIcon,
  add: AddIcon,
  arrowleft: ArrowLeftIcon,
  arrowright: ArrowRightIcon,
  arrows: ArrowsIcon,
  bank: BankIcon,
  bill: BillIcon,
  business: BusinessIcon,
  calendar: CalendarIcon,
  calendarpaymentdone: CalendarPaymentDone,
  calendarpaymentnew: CalendarPaymentNew,
  calendarpaymentnoinformation: CalendarPaymentNoInformation,
  calendarpaymentnopayment: CalendarPaymentNoPayment,
  card: CardIcon,
  chat: ChatIcon,
  check: CheckIcon,
  checkcircle: CheckCircleIcon,
  chevrondown: ChevronDownIcon,
  chevronleft: ChevronLeftIcon,
  chevronright: ChevronRightIcon,
  chevronup: ChevronUpIcon,
  close: CloseIcon,
  compare: CompareIcon,
  copy: CopyIcon,
  creditscore: CreditScoreIcon,
  currencies: CurrenciesIcon,
  dashboard: DashboardIcon,
  enquiry: EnquiryIcon,
  error: ErrorIcon,
  higherloans: HigherLoansIcon,
  home: HomeIcon,
  increasecreditscore: IncreaseCreditScoreIcon,
  loans: LoansIcon,
  menu: MenuIcon,
  myaccount: MyAccountIcon,
  myapplications: MyApplicationsIcon,
  paper: PaperIcon,
  paymenterror: PaymentErrorIcon,
  percentage: PercentageIcon,
  phone: PhoneIcon,
  piggybank: PiggyBankIcon,
  pin: PinIcon,
  refer: ReferIcon,
  rupee: RupeeIcon,
  shieldprotected: ShieldProtected,
  shieldunprotected: ShieldUnprotected,
  tickbadge: TickBadgeIcon,
  time: TimeIcon,
  usercircle: UserCircleIcon,
  // Social Icons
  facebook: FacebookIcon,
  linkedin: LinkedInIcon,
  twitter: TwitterIcon,
  whatsapp: WhatsAppIcon,
  forward: Forward,
  backward: Backward,
  chart: Chart
};

interface Props {
  color?: string; // Icon's color
  name?: string; // Icon's name
  icon?: string; // Icon's name alternate prop
}

/**
 * Renders SVG icon by given Icon name
 * @param {string} [color] - optional color to pass to the SVG icon wrapper
 * @param {string} [name] - name of the Icon to render
 * @param {string} [icon] - alternate property to use as name of the Icon
 */
const AppIcon: React.FC<Props> = ({ name, icon, ...restOfProps }) => {
  const iconName = (name || icon || 'default').trim().toLowerCase();
  const ComponentToRender = ICONS[iconName] || DefaultIcon;
  return <ComponentToRender {...restOfProps} />;
};

export default AppIcon;
