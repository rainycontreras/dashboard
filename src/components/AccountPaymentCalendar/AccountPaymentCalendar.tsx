import { Box, Typography, useMediaQuery, Theme, Stack } from '@mui/material';
import { AppIcon } from 'src/components';
import AccountPaymentCalendarCell from './AccountPaymentCalendarCell';
import { PaymentCalendar, PaymentCalendarStatus } from 'src/utils/type';
import { MONTH_NAMES } from 'src/utils/date';

interface AccountPaymentCalendarProps {
  paymentCalendar: PaymentCalendar;
}

/**
 * Account payment calendar
 * @component AccountPaymentCalendar
 */
const AccountPaymentCalendar: React.FC<AccountPaymentCalendarProps> = ({ paymentCalendar }) => {
  const mdScreen = useMediaQuery((theme: Theme) => theme.breakpoints.up('lg'));

  return (
    <Stack sx={{ border: '1px solid', borderColor: 'app.border', borderRadius: '0.375rem' }}>
      {/* Header start */}
      <Stack
        direction="row"
        sx={{ backgroundColor: 'app.lightGrey', borderBottom: '1px solid', borderBottomColor: 'app.border' }}
      >
        <Box sx={{ backgroundColor: 'app.lightGrey', width: '40px' }} />
        <Box
          alignItems="center"
          sx={{
            backgroundColor: 'white',
            display: 'grid',
            gridTemplateColumns: 'repeat(12, 1fr)',
            height: '2.8125rem',
            width: '100%',
          }}
        >
          {MONTH_NAMES.map((monthName) => (
            <AccountPaymentCalendarCell key={monthName}>
              <Typography variant="caption" sx={{ lineHeight: '1.25rem' }}>
                {mdScreen ? monthName : monthName.substring(0, 1)}
              </Typography>
            </AccountPaymentCalendarCell>
          ))}
        </Box>
      </Stack>
      {/* Header end */}
      <Stack direction="row">
        <Stack
          direction="column"
          sx={{ backgroundColor: '#F5F7FC', paddingBottom: '1rem', paddingTop: '1rem', width: '2.5rem' }}
        >
          {paymentCalendar?.years?.map((year) => (
            <AccountPaymentCalendarCell key={year}>
              <Typography variant="caption">{year}</Typography>
            </AccountPaymentCalendarCell>
          ))}
        </Stack>
        <Box
          sx={{
            backgroundColor: 'white',
            display: 'grid',
            gridTemplateColumns: 'repeat(12, 1fr)',
            paddingBottom: '1rem',
            paddingTop: '1rem',
            width: '100%',
          }}
        >
          {paymentCalendar?.years
            ?.map((year) => {
              const yearPaymentCalendar = paymentCalendar.paymentCalendar[year.toString()];
              return MONTH_NAMES.map((month) => {
                const status = yearPaymentCalendar && yearPaymentCalendar[month];

                // Must compare to null, because status can be 0.
                if (status != null) {
                  let iconName = '';

                  if (status === PaymentCalendarStatus.OK) {
                    iconName = 'calendarpaymentdone';
                  } else if (status === PaymentCalendarStatus.NEW) {
                    iconName = 'calendarpaymentnew';
                  } else if (status === PaymentCalendarStatus.NO_PAYMENT) {
                    iconName = 'calendarpaymentnopayment';
                  } else {
                    iconName = 'calendarpaymentnoinformation';
                  }

                  return (
                    <AccountPaymentCalendarCell key={`${year}:${month}`}>
                      <AppIcon name={iconName} />
                    </AccountPaymentCalendarCell>
                  );
                } else {
                  return <AccountPaymentCalendarCell key={`${year}:${month}`} />;
                }
              });
            })
            .flat()}
        </Box>
      </Stack>
    </Stack>
  );
};

export default AccountPaymentCalendar;
