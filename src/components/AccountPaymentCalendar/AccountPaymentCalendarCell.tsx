import { Box } from '@mui/material';

interface AccountPaymentCalendarCellProps {}

/**
 * Account payment calendar cell
 * @component AccountPaymentCalendarCell
 */

const AccountPaymentCalendarCell: React.FC<AccountPaymentCalendarCellProps> = ({ children }) => {
  return (
    <Box
      alignItems="center"
      justifyContent="center"
      sx={{
        display: 'flex',
        height: '1.25rem',
        marginBottom: '3px',
        marginTop: '3px',
      }}
    >
      {children}
    </Box>
  );
};

export default AccountPaymentCalendarCell;
