// import AppLink from './ReactRouterLink';
import AppLink from './NextLink';
import type { AppLinkProps } from './NextLink';

export type { AppLinkProps };
export { AppLink as default, AppLink };
