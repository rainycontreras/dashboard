import React, { Component, ErrorInfo } from 'react';
import { log } from 'src/utils/log';

interface Props {
  name: string;
}

interface State {
  hasError: boolean;
  error?: any;
  errorInfo?: ErrorInfo;
}

/**
 * Error boundary wrapper to save Application parts from falling
 * @param {string} [props.name] - name of the wrapped segment, "Error Boundary" by default
 */
class ErrorBoundary extends Component<Props, State> {
  state: State = {
    hasError: false,
  };

  static getDerivedStateFromError() {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error: any, errorInfo: ErrorInfo) {
    // Catch errors in any components below and re-render with error message
    this.setState({
      error: error,
      errorInfo: errorInfo,
    });

    log.error(error, errorInfo);
    // We can also log error messages to an error reporting service here
  }

  render() {
    if (this.state.hasError) {
      // Error flow, show fallback UI
      const { error, errorInfo } = this.state;
      const { name = 'Error Boundary' } = this.props;
      return (
        <div>
          <h2>{name} - Something went wrong</h2>
          <details style={{ whiteSpace: 'pre-wrap' }}>
            {error ? error?.toString() : null}
            <br />
            {errorInfo?.componentStack}
          </details>
        </div>
      );
    }

    // Normal flow, just render children
    return this.props.children;
  }
}

export default ErrorBoundary;
