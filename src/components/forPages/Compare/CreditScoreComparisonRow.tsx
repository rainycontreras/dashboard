import { Box, Typography, useMediaQuery, Theme } from '@mui/material';
import React from 'react';
import { PaletteOptions, useTheme } from '@mui/material/styles';
import { ScoreStatus, CreditScoreFactor, TableColumn } from '@/utils/type';

interface CellProps {
  variant?: 'body' | 'header' | 'value';
  display?: boolean;
  fontOpacity?: number;
  fontWeight?: number;
}

/**
 * Render a Credit Score Factor comparison table cell
 */
const Cell: React.FC<CellProps> = ({
  variant = 'body',
  display = true,
  fontOpacity = 1,
  fontWeight = 400,
  children,
}) => {
  const theme = useTheme();
  const headerBaseColor = (theme.palette as PaletteOptions)?.app?.border ?? '#FFFFFF';
  return (
    <>
      {display && (
        <Box
          sx={{
            paddingTop: '0.6875rem',
            paddingLeft: '1.25rem',
            paddingBottom: '0.6875rem',
            display: 'flex',
            alignItems: 'center',
            height: '3.75rem',
            backgroundColor: variant === 'header' ? `${headerBaseColor}` : '',
          }}
        >
          <Typography
            variant="label2"
            sx={{ fontWeight: variant === 'value' ? 500 : fontWeight, fontSize: '0.875rem', opacity: fontOpacity }}
          >
            {children}
          </Typography>
        </Box>
      )}
    </>
  );
};

/**
 * Render a divider for Credit Score Factor comparison table
 */
const RowDivider = () => {
  const lgScreen = useMediaQuery((theme: Theme) => theme.breakpoints.up('lg'));
  const vertical = !lgScreen;
  return (
    <>
      <Box
        sx={{ borderBottom: '1px solid', borderColor: 'app.border', gridColumn: `1 /  ${vertical ? '3' : '6'}` }}
      ></Box>
    </>
  );
};

interface CreditScoreComparisonRowProps {
  columns: TableColumn[];
  title: string;
  lastReport?: CreditScoreFactor;
  thisReport?: CreditScoreFactor;
  formatter?: (score?: CreditScoreFactor) => string | undefined;
  status?: ScoreStatus;
  headerBaseColor?: string;
  showBorders?: boolean;
}

/**
 * Render comparison row for a credit score
 */
const CreditScoreComparisonRow: React.FC<CreditScoreComparisonRowProps> = ({
  columns,
  title,
  lastReport,
  thisReport,
  formatter,
  status,
  showBorders,
}) => {
  const lgScreen = useMediaQuery((theme: Theme) => theme.breakpoints.up('lg'));
  const vertical = !lgScreen;
  const variation = (thisReport?.value ?? 0) - (lastReport?.value ?? 0);
  const fontOpacity = vertical ? 1 : 0.7;

  return (
    <React.Fragment key={title}>
      <Cell display={vertical} variant="header" fontOpacity={0.7}>
        {columns[0].label}
      </Cell>
      <Cell variant={vertical ? 'header' : 'body'} fontOpacity={fontOpacity} fontWeight={500}>
        {title}
      </Cell>

      <Cell display={vertical} fontOpacity={0.7}>
        {columns[1].label}
      </Cell>

      <Cell fontOpacity={fontOpacity}>{(formatter && formatter(lastReport)) ?? ''}</Cell>

      {vertical && <RowDivider />}

      <Cell display={vertical} fontOpacity={0.7}>
        {columns[2].label}
      </Cell>
      <Cell variant={vertical ? 'body' : 'value'}>
        {variation === 0
          ? 'No change'
          : (formatter && formatter({ displayType: thisReport?.displayType, value: variation })) ?? ''}
      </Cell>

      {vertical && <RowDivider />}

      <Cell display={vertical} fontOpacity={0.7}>
        {columns[3].label}
      </Cell>
      <Cell fontOpacity={fontOpacity}>{(formatter && formatter(thisReport)) ?? ''}</Cell>

      {vertical && <RowDivider />}

      <Cell display={vertical} fontOpacity={0.7}>
        {columns[4].label}
      </Cell>

      <Box
        key={`${title}_status`}
        sx={{
          paddingTop: '0.6875rem',
          paddingLeft: '1.25rem',
          paddingBottom: '0.6875rem',
          display: 'flex',
          alignItems: 'center',
          height: '3.75rem',
        }}
      >
        {status && (
          <Box
            sx={{
              backgroundColor: `${status.color}1A`,
              width: 'fit-content',
              padding: '0.25rem 0.75rem',
              borderRadius: '1rem',
            }}
          >
            <Typography color={status.color} variant="label2" sx={{ fontWeight: '600' }}>
              {status.label}
            </Typography>
          </Box>
        )}
      </Box>
      {showBorders && <RowDivider />}
    </React.Fragment>
  );
};

export default CreditScoreComparisonRow;
