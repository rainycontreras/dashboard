import CompareScoringItem from './CompareScoringItem';
import CreditScoreFactorComparisonTable from './CreditScoreFactorComparisonTable';
export { CompareScoringItem, CreditScoreFactorComparisonTable };
