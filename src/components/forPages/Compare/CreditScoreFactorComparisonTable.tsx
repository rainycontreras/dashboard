import { Box, Typography, useMediaQuery, Theme } from '@mui/material';
import React from 'react';
import { PaletteOptions, useTheme } from '@mui/material/styles';
import { CreditReportCompare, CreditScoreFactorItem, TableColumn } from '@/utils/type';
import { formatInt, formatMoney } from '@/utils/number';
import { formatDuration } from '@/utils/date';
import CreditScoreComparisonRow from './CreditScoreComparisonRow';

interface CreditScoreFactorComparisonTableProps {
  reports?: CreditReportCompare;
}

/**
 * Table columns
 */
const columns: TableColumn[] = [
  { label: 'Factor', id: 'factor' },
  { label: 'Last Report', id: 'lastreport' },
  { label: 'Change', id: 'change' },
  { label: 'This Report', id: 'thisreport' },
  { label: 'Status', id: 'status' },
];

/**
 * Credit score factors
 */
const items: CreditScoreFactorItem[] = [
  {
    label: 'Credit Card Utilization',
    accessor: 'creditCardUtilization',
    formatter: (score) => formatMoney(score?.value),
  },
  { label: 'Age of Credit', accessor: 'ageCreditHistory', formatter: (score) => formatDuration(score?.value) },
  { label: 'Credit Enquiries', accessor: 'creditInquires', formatter: (score) => formatInt(score?.value, '0') },
  { label: 'Payment History', accessor: 'paymentHistory', formatter: (score) => formatInt(score?.value, '0') },
  { label: 'Total Accounts', accessor: 'numberOfAccounts', formatter: (score) => formatInt(score?.value, '0') },
];

/**
 * Render comparison table for Last and current credit score reports
 * https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=17%3A16
 * @param {CreditReportCompare} reports - Last and current credit score reports
 */
const CreditScoreFactorComparisonTable: React.FC<CreditScoreFactorComparisonTableProps> = ({ reports }) => {
  const lgScreen = useMediaQuery((theme: Theme) => theme.breakpoints.up('lg'));
  const theme = useTheme();
  const headerBaseColor = (theme.palette as PaletteOptions)?.app?.border ?? '#FFFFFF';

  return (
    <Box sx={{ display: 'grid', gridTemplateColumns: lgScreen ? `repeat(${columns.length}, 1fr)` : `repeat(2, 1fr)` }}>
      {lgScreen &&
        columns &&
        columns.map((column) => (
          <Box
            key={`${column.id}`}
            sx={{
              backgroundColor: `${headerBaseColor}66`,
              height: '2.25rem',
              borderBottom: '1px solid',
              borderTop: '1px solid',
              borderBottomColor: 'app.brightGray',
              borderTopColor: 'app.brightGray',
              paddingTop: '0.6875rem',
              paddingLeft: '1.25rem',
              paddingBottom: '0.6875rem',
              display: 'flex',
              alignItems: 'center',
            }}
          >
            <Typography variant="label2" sx={{ opacity: 0.5, fontSize: '0.6875rem' }}>
              {column.label.toUpperCase()}
            </Typography>
          </Box>
        ))}
      {items &&
        items.map((row, index) => (
          <CreditScoreComparisonRow
            columns={columns}
            key={row.label}
            title={row.label}
            lastReport={reports?.oldReport ? reports?.oldReport[row.accessor] : undefined}
            thisReport={reports?.newReport ? reports?.newReport[row.accessor] : undefined}
            formatter={row.formatter}
            status={reports?.status ? reports?.status[row.accessor] : undefined}
            showBorders={index < items.length - 1}
          />
        ))}
    </Box>
  );
};

export default CreditScoreFactorComparisonTable;
