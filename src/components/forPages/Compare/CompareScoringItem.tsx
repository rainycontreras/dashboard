import { Box, Stack, Typography, useMediaQuery, Theme } from '@mui/material';
import React, { useMemo } from 'react';
import Scoring from '@/components/dashboard/Scoring';
import { formatDate } from '@/utils/date';
import { calculateScoreStatus } from '@/utils/account';
import { formatInt } from '@/utils/number';

const VARIATION_INDICATOR_SIDE_1 = 'calc(50% - 2.4rem + 0.5px)';
const VARIATION_INDICATOR_SIDE_2 = 'calc(0px - 2.4rem + 0.5px)';
const VARIATION_INDICATOR_BORDER = '1px solid';

interface CompareScoringItemProps {
  color: 'primary' | 'secondary';
  label: string;
  position: 'left' | 'right';
  score?: number;
  scoreDate?: Date;
}

/**
 * Render scoring chart for compare page
 * https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=17%3A16
 */
const CompareScoringItem: React.FC<CompareScoringItemProps> = ({ color, label, position, score, scoreDate }) => {
  const lgScreen = useMediaQuery((theme: Theme) => theme.breakpoints.up('lg'));
  const variationIndicatorLeftBorder = useMemo(() => {
    if (lgScreen) {
      return {
        top: VARIATION_INDICATOR_SIDE_1,
        right: VARIATION_INDICATOR_SIDE_2,
        borderTop: VARIATION_INDICATOR_BORDER,
        borderLeft: VARIATION_INDICATOR_BORDER,
        transform: 'rotate(-45deg)',
      };
    }
    return {
      right: VARIATION_INDICATOR_SIDE_1,
      bottom: VARIATION_INDICATOR_SIDE_2,
      borderLeft: VARIATION_INDICATOR_BORDER,
      borderTop: VARIATION_INDICATOR_BORDER,
      transform: 'rotate(45deg)',
    };
  }, [lgScreen]);

  const variationIndicatorRightBorder = useMemo(() => {
    if (lgScreen) {
      return {
        top: VARIATION_INDICATOR_SIDE_1,
        left: VARIATION_INDICATOR_SIDE_2,
        borderTop: VARIATION_INDICATOR_BORDER,
        borderLeft: VARIATION_INDICATOR_BORDER,
        transform: 'rotate(135deg)',
      };
    }
    return {
      right: VARIATION_INDICATOR_SIDE_1,
      top: VARIATION_INDICATOR_SIDE_2,
      borderTop: VARIATION_INDICATOR_BORDER,
      borderLeft: VARIATION_INDICATOR_BORDER,
      transform: 'rotate(225deg)',
    };
  }, [lgScreen]);

  const scoreStatus = useMemo(() => {
    if (!score) {
      return undefined;
    }
    return calculateScoreStatus(score);
  }, [score]);

  return (
    <Box sx={{ position: 'relative', width: '100%', height: lgScreen ? '15rem' : '17.5rem' }}>
      <Stack
        sx={{
          width: '100%',
          background: 'white',
          border: VARIATION_INDICATOR_BORDER,
          borderColor: 'app.border',
          borderRadius: '0.375rem',
          padding: '1.25rem',
          height: lgScreen ? '15rem' : '17.5rem',
        }}
      >
        <Typography variant="label2" color={color}>
          {label}
        </Typography>

        <Stack
          alignItems="center"
          justifyContent="center"
          direction="row"
          sx={{
            flexGrow: 1,
          }}
        >
          <Scoring value={score} width={184} arcWidth={10} pointerWidth={10} showColoredBar={false}>
            <Stack alignItems="center" justifyContent="end" sx={{ height: '100%' }}>
              <Typography variant="label1" sx={{ fontSize: '2rem', lineHeight: '2.6rem' }}>
                {formatInt(score)}
              </Typography>
              <Typography variant="tiny" sx={{ opacity: 0.6 }}>
                {formatDate(scoreDate, '', 'dd-MM-yyyy')}
              </Typography>
            </Stack>
            <Box sx={{ paddingTop: '0.75rem' }}>
              <Typography
                sx={{
                  fontWeight: 500,
                }}
                variant="label2"
              >
                Your credit score is
                <span
                  style={{
                    backgroundColor: `${scoreStatus?.color ?? '#FFFFFF'}1A`,
                    width: 'fit-content',
                    padding: '1px 0.25rem',
                    borderRadius: '0.25rem',
                    margin: '0px 2px',
                  }}
                >
                  <Typography color={scoreStatus?.color ?? '#FFFFFF'} variant="label2" sx={{ fontWeight: '600' }}>
                    {scoreStatus?.label ?? ''}
                  </Typography>
                </span>
              </Typography>
            </Box>
          </Scoring>
        </Stack>
      </Stack>
      {position === 'left' && (
        <Box
          sx={{
            ...variationIndicatorLeftBorder,
            position: 'absolute',
            width: '4.8rem',
            height: '4.8rem',
            background: 'white',
            borderColor: 'app.border',
          }}
        />
      )}
      {position === 'right' && (
        <Box
          sx={{
            ...variationIndicatorRightBorder,
            position: 'absolute',
            width: '4.8rem',
            height: '4.8rem',
            background: 'white',
            borderColor: 'app.border',
          }}
        />
      )}
    </Box>
  );
};

export default CompareScoringItem;
