import { TextTypography } from '@/components/typography';
import { Stack, Typography, useTheme } from '@mui/material';
import React from 'react';

/**
 * Renders "PublicRecords" section for "CreditScore Details" page
 */
const PublicRecords = () => {
  const theme = useTheme();
  return (
    <Stack
      spacing={1.5}
      sx={{ padding: '1.5rem', border: '1px solid', borderColor: 'app.border', borderRadius: '0.375rem' }}
    >
      <TextTypography variant="semibold">Public Records</TextTypography>
      <Typography variant="body">
        A Public record is a mark on your credit resulting from a government proceeding, including bankruptcies, civil judgements and more. These marks can negatively affect your credit health.
      </Typography>
    </Stack>
  );
};

export default PublicRecords;
