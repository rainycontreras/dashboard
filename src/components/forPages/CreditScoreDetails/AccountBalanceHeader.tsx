import { Box, Grid, IconButton, Stack, Typography } from '@mui/material';
import { AccountHeaderProps } from 'src/utils/type';
import { formatMoney } from 'src/utils/number';
import BankLogo from '@/components/BankLogo';
import DotIcon from '@/components/icons/other/DotIcon';
import { useMemo } from 'react';
import { PaletteOptions, useTheme } from '@mui/material/styles';
import { Label2Typography } from '@/components/typography';
import { AppButton, AppIcon } from '@/components';

/**
 * Render account header for accounts list
 * Figma design https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=222%3A31
 */
const AccountBalanceHeader: React.FC<AccountHeaderProps> = ({ account, detailsOpen, onOpenCloseDetails }) => {
  const theme = useTheme();
  const acountPaymentStatus = useMemo(() => {
    const appColorSpace = (theme.palette as PaletteOptions)?.app;
    if (account?.totalMissedPayments === 0) {
      return { label: 'No missed payment', color: appColorSpace?.grey };
    } else {
      return { label: `${account.totalMissedPayments} missed payment`, color: appColorSpace?.red };
    }
  }, [account?.totalMissedPayments, theme]);

  return (
    <Stack>
      <Stack alignItems="center" direction="row" spacing={1} sx={{ padding: '1rem' }}>
        <BankLogo name={account.institution} />
        <Grid container>
          <Grid item xs={12} md={4} lg={4}>
            <Stack sx={{ flexGrow: 1 }}>
              <Typography variant="label2">{account.institution}</Typography>
              <Typography variant="caption" sx={{ color: 'app.grey' }}>
                {account.status ?? ''}
              </Typography>
            </Stack>
          </Grid>

          <Grid alignItems="center" justifyContent="start" item xs={12} md={3} lg={3} sx={{ display: 'flex' }}>
            <Label2Typography color="app.grey" variant="regular">
              {account.type ?? ''}
            </Label2Typography>
          </Grid>

          <Grid alignItems="center" justifyContent="center" item xs={12} md={3} lg={3} sx={{ display: 'flex' }}>
            <Stack alignItems="center" justifyContent="center">
              <Box>
                <Stack
                  alignItems="center"
                  justifyContent="center"
                  direction="row"
                  spacing={0.5}
                  sx={{
                    backgroundColor: `${acountPaymentStatus?.color}1A`,
                    padding: '0.125rem 0.75rem',
                    borderRadius: '1.25rem',
                  }}
                >
                  <DotIcon color={acountPaymentStatus.color} size={3} />
                  <Typography sx={{ color: acountPaymentStatus.color }} variant="label2">
                    {acountPaymentStatus?.label ?? ''}
                  </Typography>
                </Stack>
              </Box>
            </Stack>
          </Grid>
          <Grid alignItems="center" justifyContent="center" item xs={12} md={2} lg={2} sx={{ display: 'flex' }}>
            <Typography variant="label2">{formatMoney(account.balance, '')}</Typography>
          </Grid>
        </Grid>

        <IconButton
          disableFocusRipple
          disableRipple
          onClick={onOpenCloseDetails}
          size="small"
          sx={{ padding: '0', borderRadius: '50%', height: '2rem', width: '2rem' }}
        >
          <Stack
            alignItems="center"
            justifyContent="center"
            sx={{
              backgroundColor: 'app.lightGrey',
              borderRadius: '50%',
              height: '2rem',
              width: '2rem',
            }}
          >
            <AppIcon name={detailsOpen ? 'chevronup' : 'chevrondown'} />
          </Stack>
        </IconButton>
      </Stack>
      {account.savings?.amount && (
        <Stack
          alignItems="center"
          justifyContent="center"
          sx={{ backgroundColor: 'app.lightGrey', borderTop: '.0625rem solid', borderTopColor: 'app.border' }}
        >
          <Box sx={{ padding: '.75rem' }}>
            <AppButton variant="solid" endIcon="arrowright">
              <Typography color="white" variant="label2" sx={{ fontWeight: 600 }}>
                Click here to save
                <Box
                  sx={{
                    display: 'inline',
                    backgroundColor: '#FFFFFF33',
                    borderRadius: '.25rem',
                    padding: '.125rem .3125rem',
                  }}
                >
                  <Typography color="white" variant="label2" sx={{ fontWeight: 600 }}>
                    {formatMoney(account.savings?.amount, '')}
                  </Typography>
                </Box>
                on your loan
              </Typography>
            </AppButton>
          </Box>
        </Stack>
      )}
    </Stack>
  );
};

export default AccountBalanceHeader;
