import AppIcon from '@/components/AppIcon';
import { TextTypography } from '@/components/typography';
import { MixBorrowing } from '@/utils/type';
import { Box, Grid, Stack, Typography, useTheme } from '@mui/material';
import React from 'react';

interface Props {
  mixBorrowing?: MixBorrowing;
}

/**
 * Renders "Mix of Borrowing" section for "CreditScore Details" page
 */
const MixBorrowing: React.FC<Props> = ({ mixBorrowing }) => {
  const theme = useTheme();
  return (
    <Stack
      spacing={2.5}
      sx={{ padding: '1.5rem', border: '1px solid', borderColor: 'app.border', borderRadius: '0.375rem' }}
    >
      <TextTypography variant="semibold">Mix of Borrowing</TextTypography>
      <Box>
        <Grid alignContent="stretch" container spacing={2}>
          <Grid item xs={12} md={6} lg={3}>
            <Box sx={{ backgroundColor: 'app.lightGrey', borderRadius: '0.375rem', padding: '1.5rem', height: '100%' }}>
              <Stack direction="row" spacing={1.25}>
                <AppIcon name="shieldprotected" />
                <Stack>
                  <Stack spacing={0.5}>
                    <Typography variant="body" sx={{ opacity: 0.6 }}>
                      Secured Loans
                    </Typography>
                    <Typography variant="h2">{mixBorrowing?.secure}</Typography>
                  </Stack>
                </Stack>
              </Stack>
            </Box>
          </Grid>
          <Grid item xs={12} md={6} lg={3}>
            <Box sx={{ backgroundColor: 'app.lightGrey', borderRadius: '0.375rem', padding: '1.5rem', height: '100%' }}>
              <Stack direction="row" spacing={1.25}>
                <AppIcon name="shieldunprotected" />
                <Stack spacing={0.5}>
                  <Typography variant="body" sx={{ opacity: 0.6 }}>
                    Not Secured Loans
                  </Typography>
                  <Typography variant="h2">{mixBorrowing?.unsecure}</Typography>
                </Stack>
              </Stack>
            </Box>
          </Grid>
        </Grid>
      </Box>
    </Stack>
  );
};

export default MixBorrowing;
