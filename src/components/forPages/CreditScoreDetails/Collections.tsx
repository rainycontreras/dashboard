import { TextTypography } from '@/components/typography';
import { Stack, Typography, useTheme } from '@mui/material';
import React from 'react';

/**
 * Renders "Collections" section for "CreditScore Details" page
 * Figma design: https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=222%3A31
 */
const Collections = () => {
  const theme = useTheme();
  return (
    <Stack
      spacing={1.5}
      sx={{ padding: '1.5rem', border: '1px solid', borderColor: 'app.border', borderRadius: '0.375rem' }}
    >
      <TextTypography variant="semibold">Collections</TextTypography>
      <Typography variant="body">
        If you&apos;ve fallen behind on payments, your account could be sent to a collections agency. This can have a
        big impact on your credit score.
      </Typography>
    </Stack>
  );
};

export default Collections;
