import AppIcon from '@/components/AppIcon';
import { TextTypography } from '@/components/typography';
import { Address, PersonalInformation } from '@/utils/type';
import { Box, Divider, Grid, PaletteOptions, Stack, Typography, useTheme } from '@mui/material';
import React, { useMemo } from 'react';

interface Props {
  personalInformation?: PersonalInformation;
}

const FIELD_TITLE_VARIANT = {
  color: 'app.grey',
  fontSize: '0.75rem',
  fontWeight: '700',
};

/**
 * Renders "Personal Information" section for "CreditScore Details" page
 */
const PersonalInformation: React.FC<Props> = ({ personalInformation }) => {
  const theme = useTheme();
  const primaryColor = (theme.palette as PaletteOptions)?.app?.primary;
  const uniqueAddresses: string[] | undefined = useMemo(() => {
    return personalInformation?.addresses?.reduce((addresses: string[], personalAdress: Address) => {
      const address = `${personalAdress.address ?? ''}, ${personalAdress.state ?? ''} ${personalAdress.postal ?? ''}`;
      if (addresses.indexOf(address) === -1) {
        addresses.push(address);
      }
      return addresses;
    }, []);
  }, [personalInformation?.addresses]);
  return (
    <Stack
      spacing={1.5}
      sx={{ padding: '1.5rem', border: '1px solid', borderColor: 'app.border', borderRadius: '0.375rem' }}
    >
      <TextTypography variant="semibold">Personal Information</TextTypography>
      <Typography variant="body">
        Personal information, like names and addresses, are typically reported to the credit bureaus by your lenders. If
        you&apos;ve filled out applications using different name variations or addresses, then those variations will
        likely appear on your credit reports as well. Find out more about personal information on your credit report.
      </Typography>
      <Box sx={{ paddingTop: '0.25rem' }}>
        <Grid container spacing={2.5}>
          <Grid item xs={12} md={6}>
            <Stack spacing={0.5}>
              <Typography sx={FIELD_TITLE_VARIANT}>NAMES REPORTED</Typography>
              <Typography variant="label2">{personalInformation?.name ?? ''}</Typography>
            </Stack>
          </Grid>
          <Grid item xs={12} md={6}>
            <Stack>
              <Typography sx={FIELD_TITLE_VARIANT}>EMPLOYMENT INFO</Typography>
              <Typography variant="body">
                {!personalInformation?.employmentInfo && 'You have no employment information on your credit report.'}
                {personalInformation?.employmentInfo && (
                  <>
                    {personalInformation?.employmentInfo?.name ?? ''}
                    <br />
                    {personalInformation?.employmentInfo?.position ?? ''}
                    <br />
                    {personalInformation?.employmentInfo?.address ?? ''}
                    <br />
                  </>
                )}
              </Typography>
            </Stack>
          </Grid>
        </Grid>
      </Box>
      <Divider></Divider>
      <Box>
        <Typography sx={FIELD_TITLE_VARIANT}>ADDRESSES REPORTED</Typography>
      </Box>
      {uniqueAddresses &&
        uniqueAddresses.map((address) => (
          <Stack alignItems="center" key={address} direction="row" spacing={1.5}>
            <AppIcon color={primaryColor} name="pin" />
            <Typography variant="body">{address}</Typography>
          </Stack>
        ))}
    </Stack>
  );
};

export default PersonalInformation;
