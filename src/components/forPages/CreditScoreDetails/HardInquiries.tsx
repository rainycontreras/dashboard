import { AppIcon, AppLink } from '@/components';
import AppButton from '@/components/AppButton';
import { Label2Typography, TextTypography } from '@/components/typography';
import { AccountEnquiry } from '@/utils/type';
import { Box, Grid, Stack, Typography, useTheme } from '@mui/material';
import React, { useState } from 'react';
import { useCallback } from 'react';
import { useMemo } from 'react';
import Enquiry from '../CreditEnquiries';

interface Props {
  enquiries?: AccountEnquiry[];
}

/**
 * Render a list of enquiries
 * @param {AccountEnquiry} enquiries - Enquiries to render
 */
function renderEnquiries(enquiries?: AccountEnquiry[]) {
  {
    return (
      <>
        {enquiries &&
          enquiries
            .filter((enquiry) => enquiry)
            .map((enquiry) => (
              <Enquiry key={enquiry.id} enquiry={enquiry}>
                <Box
                  sx={{ padding: '1.25rem 1.5rem 1.5rem 1.5rem', borderTop: '1px solid', borderTopColor: 'app.border' }}
                >
                  <Grid container>
                    <Grid item xs={12} md={6}>
                      <Stack spacing={0.5}>
                        <TextTypography variant="semibold">See an error?</TextTypography>
                        <Label2Typography variant="regular">
                          Find out how to&nbsp;
                          <AppLink href="http://www.equifax.co.in/consumer/forms/dispute_resolution/en_in">
                            <Label2Typography color="primary" variant="regular">
                              dispute a hard inquiry
                            </Label2Typography>
                          </AppLink>
                        </Label2Typography>
                      </Stack>
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <Stack spacing={0.5}>
                        <TextTypography variant="semibold">Institution Information</TextTypography>
                        <Label2Typography variant="regular">
                          No contact information has been reported by this creditor.
                        </Label2Typography>
                      </Stack>
                    </Grid>
                  </Grid>
                </Box>
              </Enquiry>
            ))}
      </>
    );
  }
}

/**
 * Renders "HardInquiries" section for "CreditScore Details" page
 * Figma design https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=222%3A31
 */
const HardInquiries: React.FC<Props> = ({ enquiries }) => {
  const theme = useTheme();
  const [showAll, setShowAll] = useState<boolean>(false);
  const { firstPage, restEnquiries } = useMemo(() => {
    if (!enquiries) return {};
    return { firstPage: enquiries.slice(0, 2), restEnquiries: enquiries.slice(2) };
  }, [enquiries]);

  const seeAllHandle = useCallback(() => {
    setShowAll(true);
  }, []);

  return (
    <Stack
      spacing={1.5}
      sx={{ padding: '1.5rem', border: '1px solid', borderColor: 'app.border', borderRadius: '0.375rem' }}
    >
      <TextTypography variant="semibold">Hard Inquiries</TextTypography>
      <Typography variant="body">
        When you apply for a new credit account, a hard inquiry will usally get added to your report, which can make a
        small dent in your score. Here are the inquiries on your Equifax report.
      </Typography>
      <Stack spacing={1} sx={{ paddingTop: '0.25rem' }}>
        {renderEnquiries(firstPage)}
        {restEnquiries?.length && !showAll && (
          <Stack direction="row" justifyContent="end">
            <Box>
              <AppButton
                onClick={seeAllHandle}
                variant="outline"
                size="small"
                endIcon={<AppIcon name={'chevrondown'} />}
                sx={{
                  borderColor: 'app.border',
                  '&:hover': {
                    borderColor: 'app.border',
                  },
                }}
              >
                <Label2Typography variant="medium" color="app.grey">
                  See All
                </Label2Typography>
              </AppButton>
            </Box>
          </Stack>
        )}
        {(restEnquiries?.length ?? 0 > 0) && showAll && renderEnquiries(restEnquiries)}
      </Stack>
    </Stack>
  );
};

export default HardInquiries;
