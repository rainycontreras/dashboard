import AccountDetail from '@/components/AccountDetail';
import { TextTypography } from '@/components/typography';
import { AccountDetails } from '@/utils/type';
import { Stack } from '@mui/material';
import React from 'react';
import AccountBalanceHeader from './AccountBalanceHeader';
interface Props {
  accounts?: AccountDetails[];
}

/**
 * Renders "Account Balances" section for "CreditScore Details" page
 * Figma design: https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=222%3A31
 */
const AccountBalances: React.FC<Props> = ({ accounts }) => {
  return (
    <Stack
      spacing={2.5}
      sx={{ padding: '1.5rem', border: '1px solid', borderColor: 'app.border', borderRadius: '0.375rem' }}
    >
      <TextTypography variant="semibold">Account Balances</TextTypography>
      <Stack spacing={1}>
        {accounts &&
          accounts.map((account) => (
            <AccountDetail
              variant="creditreport"
              header={AccountBalanceHeader}
              key={account.number}
              account={account}
            />
          ))}
      </Stack>
    </Stack>
  );
};

export default AccountBalances;
