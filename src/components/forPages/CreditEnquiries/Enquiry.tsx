import { Grid, Stack, Typography, useMediaQuery, Theme, IconButton } from '@mui/material';
import { formatDate } from 'src/utils/date';
import { AccountEnquiry } from 'src/utils/type';
import BankLogo from 'src/components/BankLogo';
import { useCallback, useState } from 'react';
import AppIcon from '@/components/AppIcon';

interface Props {
  enquiry: AccountEnquiry;
}

/**
 * Render enquiry component
 * Figma design https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=222%3A31
 */
const Enquiry: React.FC<Props> = ({ children, enquiry }) => {
  const mdScreen = useMediaQuery((theme: Theme) => theme.breakpoints.up('md'));
  const [showChildren, setShowChildren] = useState<boolean>(false);
  const showChildrenHandle = useCallback(() => {
    setShowChildren((currentValue) => !currentValue);
  }, []);
  return (
    <Stack
      sx={{
        border: '1px solid',
        borderColor: 'app.border',
        borderRadius: '0.375rem',
      }}
    >
      {
        //#region header
      }
      <Stack alignItems="center" direction="row" spacing={1} sx={{ padding: '1rem' }}>
        <BankLogo name={enquiry.institution} />
        <Grid container>
          <Grid item xs={12} md={10} lg={10}>
            <Stack sx={{ flexGrow: 1 }}>
              <Typography variant="label2">{enquiry.institution}</Typography>
              <Typography variant="caption" sx={{ color: 'app.grey' }}>
                Inquiry from {formatDate(enquiry.date, 'Date Not Available')}
              </Typography>
            </Stack>
          </Grid>
          <Grid
            alignItems="center"
            justifyContent={mdScreen ? 'end' : 'start'}
            item
            xs={12}
            md={2}
            lg={2}
            sx={{ display: 'flex' }}
          >
            <Typography variant="label2">{enquiry.purpose ?? 'Reason Not Available'}</Typography>
          </Grid>
        </Grid>
        {children && (
          <IconButton
            disableFocusRipple
            disableRipple
            onClick={showChildrenHandle}
            size="small"
            sx={{ padding: '0', borderRadius: '50%', height: '2rem', width: '2rem' }}
          >
            <Stack
              alignItems="center"
              justifyContent="center"
              sx={{
                backgroundColor: 'app.lightGrey',
                borderRadius: '50%',
                height: '2rem',
                width: '2rem',
              }}
            >
              <AppIcon name={showChildren ? 'chevronup' : 'chevrondown'} />
            </Stack>
          </IconButton>
        )}
      </Stack>
      {
        //#endregion header
      }
      {showChildren && children}
    </Stack>
  );
};

export default Enquiry;
