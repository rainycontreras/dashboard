import { Box, Grid, Stack, Typography, useMediaQuery, Theme, Select, MenuItem } from '@mui/material';
import { AppIcon } from '@/components';
import { useTheme, PaletteOptions } from '@mui/material/styles';
import React, { useCallback } from 'react';
import { formatMoney, formatPercentage } from '@/utils/number';
import { AccountDetails } from '@/utils/type';
import LoansIcon from '@/components/icons/Loans';

interface Props {
  loans?: AccountDetails[];
  loan?: AccountDetails;
  salary?: number;
  onChange?: (loan: AccountDetails) => void;
}

/**
 * Render Loan Saving section
 * Figma https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=13%3A9
 * @param {AccountDetails} loan - loan to render
 * @param {number} salary - Customer's salary
 * @param {AccountDetails[]} loans - Elegible loans to save interes
 * @param {(loan: AccountDetails) => void} onChange - Callback handler for loan change event.
 * @returns
 */
const LoanSaving: React.FC<Props> = ({ loan, salary, loans, onChange }) => {
  const lgScreen = useMediaQuery((theme: Theme) => theme.breakpoints.up('lg'));
  const theme = useTheme();
  const primaryColor = (theme.palette as PaletteOptions)?.app?.primary;
  const secondaryColor = (theme.palette as PaletteOptions)?.app?.secondary;

  const value = loan?.paidPercentage ?? 0;

  const loanChangeHandle = useCallback(
    (event) => {
      const selectedLoan = loans?.filter((loanAccount) => loanAccount.number === event.target.value)[0];
      if (selectedLoan && onChange) {
        onChange(selectedLoan);
      }
    },
    [loans]
  );

  return (
    <Stack
      spacing={5.25}
      sx={{
        width: '100%',
        background: 'white',
        border: '1px solid',
        borderColor: 'app.border',
        borderRadius: '0.375rem',
        padding: '1.25rem',
      }}
    >
      <Grid container>
        <Grid item xs={12} lg={6}>
          <Stack spacing={0.75}>
            <Box
              sx={{
                backgroundColor: 'app.oldLace',
                borderRadius: '4px',
                padding: '2px 4px',
                width: 'fit-content',
              }}
            >
              <Typography color="app.gamboge" variant="label2" sx={{ fontSize: '0.75rem' }}>
                LOAN WITH HIGHEST SAVING POTENTIAL
              </Typography>
            </Box>
            <Typography variant="h1">{loan?.institution}</Typography>
          </Stack>
        </Grid>
        <Grid item xs={12} lg={6}>
          <Stack justifyContent={lgScreen ? 'end' : 'start'} direction="row">
            <Box
              sx={{
                border: '1px solid',
                borderColor: 'app.border',
                padding: '4px 0px 4px  12px',
                borderRadius: '6px',
              }}
            >
              <Select
                value={loan?.number ?? ''}
                variant="outlined"
                margin="dense"
                onChange={loanChangeHandle}
                IconComponent={(props) => (
                  <Box {...props} style={{ height: '1.25rem', top: 'calc(50% - 0.6rem)' }}>
                    <AppIcon name="dropdown" color={secondaryColor} />
                  </Box>
                )}
                inputProps={{
                  sx: {
                    '&.MuiOutlinedInput-input': {
                      padding: '0px',
                    },
                  },
                }}
                sx={{
                  '& > fieldset': {
                    display: 'none',
                  },
                }}
              >
                {loans &&
                  loans.map((loanAccount) => (
                    <MenuItem key={loanAccount.number} value={loanAccount.number}>
                      <Box sx={{ height: '1.4375rem' }}>
                        <Stack alignItems="center" spacing={0.5} direction="row" sx={{ height: '100%' }}>
                          <Typography variant="tiny">{loanAccount?.accountType ?? ''}</Typography>
                          <Typography color="primary" variant="tiny">
                            ({formatMoney(loanAccount.sanctionAmount)})
                          </Typography>
                        </Stack>
                      </Box>
                    </MenuItem>
                  ))}
              </Select>
            </Box>
          </Stack>
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={12} lg={6}>
          <Stack spacing={1.5}>
            <Stack justifyContent="space-between" direction="row">
              <Stack spacing={0.5}>
                <Typography variant="tiny" sx={{ opacity: 0.6 }}>
                  Paid
                </Typography>
                <Typography variant="label2" sx={{ fontWeight: 600 }}>
                  {formatMoney(loan?.paidAmount, '')}
                </Typography>
              </Stack>
              <Stack spacing={0.5}>
                <Typography variant="tiny" sx={{ opacity: 0.6 }}>
                  Remaining
                </Typography>
                <Typography variant="label2" sx={{ fontWeight: 600 }}>
                  {formatMoney(loan?.balance, '')}
                </Typography>
              </Stack>
            </Stack>
            <Box sx={{ position: 'relative' }}>
              <Box
                sx={{
                  borderRadius: '10px',
                  backgroundColor: 'app.border',
                  height: '0.375rem',
                  width: '100%',
                }}
              />
              <Box
                sx={{
                  borderRadius: '10px',
                  backgroundColor: 'app.tertiary',
                  height: '0.375rem',
                  position: 'absolute',
                  top: '0',
                  left: '0',
                  width: `${value * 100}%`,
                  zIndex: '1',
                }}
              />
            </Box>
          </Stack>
        </Grid>
        <Grid item xs={12} lg={6}>
          <Grid container>
            <Grid item xs={6} lg={6}>
              <Stack justifyContent={lgScreen ? 'end' : 'start'} direction="row" spacing={1}>
                <AppIcon color={primaryColor} name="percentage" />
                <Stack spacing={0.5}>
                  <Typography variant="tiny" sx={{ opacity: 0.6, paddingTop: '0.25rem' }}>
                    Interest Rate
                  </Typography>
                  <Typography variant="label2" sx={{ fontWeight: '600' }}>
                    {formatPercentage(loan?.interestRate)}
                  </Typography>
                </Stack>
              </Stack>
            </Grid>
            <Grid item xs={6} lg={6}>
              <Stack justifyContent={lgScreen ? 'end' : 'start'} direction="row" spacing={1}>
                <AppIcon color={primaryColor} name="calendar" />
                <Stack spacing={0.5}>
                  <Typography variant="tiny" sx={{ opacity: 0.6, paddingTop: '0.25rem' }}>
                    Monthly Income
                  </Typography>
                  <Typography variant="label2" sx={{ fontWeight: '600' }}>
                    {formatMoney(salary ?? 0)}
                  </Typography>
                </Stack>
              </Stack>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Stack>
  );
};

export default LoanSaving;
