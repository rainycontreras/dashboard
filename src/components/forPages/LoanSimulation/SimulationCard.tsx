import { Stack, Typography } from '@mui/material';
import { AppIcon } from '@/components';
import { useTheme, PaletteOptions } from '@mui/material/styles';
import React, { useMemo } from 'react';
import { LoanSimulation, Simulation } from '@/utils/type';
import { formatMoney } from '@/utils/number';

interface Props {
  simulation: Simulation;
}

const SIMULATION_AT_10 = 10;
const SIMULATION_AT_15 = 15;

/**
 * Renders simulation card
 * Figma: https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=13%3A9
 * @param {Simulation} simulation - Simulation to render
 * @returns
 */
const SimulationCard: React.FC<Props> = ({ simulation }) => {
  const theme = useTheme();
  const color = useMemo(() => {
    if (simulation?.extraPaid === SIMULATION_AT_15) {
      return (theme.palette as PaletteOptions)?.app?.primary;
    } else if (simulation?.extraPaid === SIMULATION_AT_10) {
      return (theme.palette as PaletteOptions)?.app?.greenCrayola;
    } else {
      return (theme.palette as PaletteOptions)?.app?.buttonBlue;
    }
  }, [simulation, theme]);

  return (
    <Stack
      sx={{
        width: '100%',
        background: 'white',
        border: '1px solid',
        borderColor: 'app.border',
        borderRadius: '0.375rem',
      }}
    >
      <Stack
        alignItems="center"
        justifyContent="center"
        spacing={1}
        sx={{ paddingTop: '1.25rem', paddingBottom: '1.25rem' }}
      >
        <Stack
          alignItems="center"
          justifyContent="center"
          sx={{ width: '2.5rem', height: '2.5rem', borderRadius: '50%', backgroundColor: '#F5F7FC' }}
        >
          <AppIcon color={color} name="piggybank" />
        </Stack>
        <Stack alignItems="center" justifyContent="center">
          <Typography color={color} variant="h2">
            {formatMoney(simulation?.interestSaved, '')}
          </Typography>
          <Typography variant="tiny" sx={{ opacity: 0.6 }}>
            Interest Savings
          </Typography>
        </Stack>
      </Stack>
      <Stack direction="row" sx={{ borderTop: '1px solid', borderTopColor: 'app.border' }}>
        <Stack sx={{ padding: '0.875rem 1rem', flexGrow: 1 }}>
          <Typography variant="body" sx={{ fontSize: '0.6875rem', opacity: 0.6 }}>
            Additional Payment
          </Typography>
          <Typography variant="label1" sx={{ fontSize: '0.75rem' }}>
            {formatMoney(simulation?.additionalPayment, '')}
          </Typography>
        </Stack>
        <Stack sx={{ borderLeft: '1px solid', borderLeftColor: 'app.border', padding: '0.875rem 1rem', flexGrow: 1 }}>
          <Typography variant="body" sx={{ fontSize: '0.6875rem', opacity: 0.6 }}>
            Total Monthly Payment
          </Typography>
          <Typography variant="label1" sx={{ fontSize: '0.75rem' }}>
            {formatMoney(simulation?.totalPayment, '')}
          </Typography>
        </Stack>
      </Stack>
    </Stack>
  );
};

export default SimulationCard;
