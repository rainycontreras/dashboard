import { Box, Grid, Stack, Typography, InputBase, InputAdornment } from '@mui/material';
import { AppIcon } from '@/components';
import { ToggleButtonGroup, ToggleButton } from '@mui/material';
import React, { useCallback, useState } from 'react';
import { formatMoney } from '@/utils/number';
import { AppSlider } from '@/components/AppSlider';
import { AccountDetails } from '@/utils/type';

interface Props {
  loan?: AccountDetails;
}

/**
 * Render Loan Extra Contribution Section
 * Figma https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=13%3A9
 * @param {AccountDetails} loan - loan to render extra contribution
 * @returns
 */
const LoanExtraContribution: React.FC<Props> = ({ loan }) => {
  const [monthlyPayment, setMonthlyPayment] = useState<number>(0);

  const extraPaymantMinusHandle = useCallback(() => {
    setMonthlyPayment((currentValue) => {
      const newValue = currentValue - 1;
      return newValue > 0 ? newValue : 0;
    });
  }, [loan]);

  const extraPaymantPlusHandle = useCallback(() => {
    setMonthlyPayment((currentValue) => {
      const emi = loan?.emi ?? 0;
      const newValue = currentValue + 1;
      return newValue < emi ? newValue : emi;
    });
  }, [loan]);

  const extraPaymentChangeHandle = useCallback(
    (event) => {
      const extraPaymentValue = parseFloat(event.target.value);
      setMonthlyPayment(() => {
        if (!isNaN(extraPaymentValue) && extraPaymentValue > 0) {
          const emi = loan?.emi ?? 0;
          return extraPaymentValue < emi ? extraPaymentValue : emi;
        } else {
          return 0;
        }
      });
    },
    [loan]
  );

  return (
    <Stack
      sx={{
        width: '100%',
        background: 'white',
        border: '1px solid',
        borderColor: 'app.border',
        borderRadius: '0.375rem',
        height: '100%',
      }}
    >
      <Stack>
        <Stack sx={{ padding: '1rem 1.25rem' }}>
          <Typography variant="label2" sx={{ fontWeight: '600' }}>
            Additional Monthly Payment
          </Typography>
          <Stack spacing={1.5} sx={{ paddingTop: '2rem' }}>
            <Stack alignItems="center" justifyContent="space-between" direction="row">
              <Typography variant="label2" sx={{ fontSize: '0.75rem', fontWeight: '600' }}>
                {formatMoney(monthlyPayment)}
              </Typography>
              <Stack>
                <Box display="block">
                  <Box
                    sx={{
                      border: '2px solid',
                      borderColor: '#F0F2F7',
                      borderRadius: '.375rem',
                      height: '2rem',
                    }}
                  >
                    <ToggleButtonGroup sx={{ height: '100%' }}>
                      <ToggleButton
                        value="minus"
                        sx={{ border: '0px', padding: '0' }}
                        onClick={extraPaymantMinusHandle}
                      >
                        <AppIcon name="minus" />
                      </ToggleButton>
                      <Box
                        sx={{
                          borderLeft: '1px solid',
                          borderLeftColor: '#E8E9ED',
                          borderRight: '1px solid',
                          borderRightColor: '#E8E9ED',
                          height: '100%',
                        }}
                      >
                        <InputBase
                          onChange={extraPaymentChangeHandle}
                          size="small"
                          inputProps={{
                            size: 10,
                          }}
                          value={monthlyPayment ?? 0}
                          startAdornment={
                            <InputAdornment position="start">
                              <Typography sx={{ fontWeight: '600', fontSize: '0.75rem', color: '#5562EB' }}>
                                ₹
                              </Typography>
                            </InputAdornment>
                          }
                          sx={{
                            border: 0,
                            height: '100%',
                            fontWeight: '600',
                            fontSize: '0.75rem',
                            color: '#5562EB',
                            paddingLeft: '0.5rem',
                            '& input': {
                              padding: '0',
                            },
                          }}
                        />
                      </Box>
                      <ToggleButton value="plus" sx={{ border: '0px', padding: '0' }} onClick={extraPaymantPlusHandle}>
                        <AppIcon name="plus" />
                      </ToggleButton>
                    </ToggleButtonGroup>
                  </Box>
                </Box>
              </Stack>
            </Stack>
            <Stack sx={{ paddingBottom: '1.25rem' }}>
              <AppSlider
                max={loan?.emi ?? 0}
                value={monthlyPayment}
                onChange={(e: any) => {
                  setMonthlyPayment(e.target.value);
                }}
                valueLabelDisplay="auto"
                valueLabelFormat={(value) => {
                  return (
                    <Typography color="white" variant="label1" sx={{ fontSize: '0.75rem' }}>
                      {formatMoney(value)}
                    </Typography>
                  );
                }}
              />
            </Stack>
          </Stack>
        </Stack>
      </Stack>
      <Box sx={{ flexGrow: 1 }} />
      <Stack
        sx={{
          backgroundColor: '#F5F7FC',
          borderRadius: '0px 0px 0.375rem 0.375rem',
          padding: '1.25rem 1.5rem',
        }}
      >
        <Grid alignItems="stretch" container spacing={1.25}>
          <Grid item xs={12} lg={6}>
            <Stack spacing={0.5} sx={{ height: '100%' }}>
              <Typography variant="tiny" sx={{ fontSize: '.75rem' }}>
                Interest Saved
              </Typography>
              <Typography variant="h2" color="primary">
                ₹
              </Typography>
            </Stack>
          </Grid>
          <Grid item xs={12} lg={3}>
            <Stack
              justifyContent="space-between"
              spacing={0.5}
              sx={{
                backgroundColor: 'white',
                boxShadow: '0rem .0625rem .1875rem rgba(29, 33, 69, 0.03), 0rem .375rem .5rem rgba(29, 33, 69, 0.03)',
                borderRadius: '.375rem',
                padding: '.625rem .4375rem',
                height: '100%',
              }}
            >
              <Typography variant="tiny" sx={{ fontSize: '.75rem', opacity: 0.6 }}>
                Additional monthly payment
              </Typography>
              <Typography variant="label1" color="secondary">
                {formatMoney(monthlyPayment)}
              </Typography>
            </Stack>
          </Grid>
          <Grid item xs={12} lg={3}>
            <Stack
              justifyContent="space-between"
              spacing={0.5}
              sx={{
                backgroundColor: 'white',
                boxShadow: '0rem .0625rem .1875rem rgba(29, 33, 69, 0.03), 0rem .375rem .5rem rgba(29, 33, 69, 0.03)',
                borderRadius: '.375rem',
                padding: '.625rem .4375rem',
                height: '100%',
              }}
            >
              <Typography variant="tiny" sx={{ fontSize: '.75rem', opacity: 0.6 }}>
                Total monthly payment
              </Typography>
              <Typography variant="label2" color="secondary">
                {formatMoney(loan?.emi)}
              </Typography>
            </Stack>
          </Grid>
        </Grid>
      </Stack>
    </Stack>
  );
};

export default LoanExtraContribution;
