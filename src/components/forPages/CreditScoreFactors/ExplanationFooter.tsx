import React from 'react';

interface Props {}

/**
 * Wrapper for explanation section footer.
 */
const ExplanationFooter: React.FC<Props> = ({ children }) => {
  return <>{children}</>;
};

export default ExplanationFooter;
