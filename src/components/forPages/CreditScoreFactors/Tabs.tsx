import React from 'react';
import { SecondLevelTab, SecondLevelTabs } from 'src/components/AppTabs';

/**
 * Renders composition of Navigation Tabs for "Credit Score Factors" internal pages
 * @component CreditScoreFactorTabs
 */
const CreditScoreFactorsTabs = () => {
  return (
    <SecondLevelTabs areaLabel="different credit score factors by tabs">
      <SecondLevelTab label="CreditCard Utilization" to="credit-card-utilization" />
      <SecondLevelTab label="Payment History" to="payment-history" />
      <SecondLevelTab label="Negative Status Accounts" to="negative-status-accounts" />
      <SecondLevelTab label="Age of Credit History" to="age-of-credit-history" />
      <SecondLevelTab label="Credit Enquiries" to="credit-enquiries" />
      <SecondLevelTab label="Total Accounts" to="total-accounts" />
    </SecondLevelTabs>
  );
};

export default CreditScoreFactorsTabs;
