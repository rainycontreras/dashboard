import { Box, Grid, IconButton, Stack, Typography } from '@mui/material';
import { AccountHeaderProps } from 'src/utils/type';
import { formatMoney } from 'src/utils/number';
import BankLogo from '@/components/BankLogo';
import { formatDate } from '@/utils/date';
import DotIcon from '@/components/icons/other/DotIcon';
import { useMemo } from 'react';
import { PaletteOptions, useTheme } from '@mui/material/styles';
import { AppIcon } from '@/components';

/**
 * Render account header for accounts list
 * Figma design https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=1338%3A26741
 */
const CreditCardAccountHeader: React.FC<AccountHeaderProps> = ({ account, detailsOpen, onOpenCloseDetails }) => {
  const theme = useTheme();
  const acountPaymentStatus = useMemo(() => {
    const appColorSpace = (theme.palette as PaletteOptions)?.app;
    if (account?.totalMissedPayments === 0) {
      return { label: 'Good', color: appColorSpace?.green };
    } else if (account?.totalMissedPayments <= 10) {
      return { label: 'Poor', color: appColorSpace?.crayola };
    } else {
      return { label: 'Bad', color: appColorSpace?.red };
    }
  }, [account?.totalMissedPayments, theme]);

  return (
    <Stack alignItems="center" direction="row" spacing={1} sx={{ padding: '1rem' }}>
      <BankLogo name={account.institution} />
      <Grid container>
        <Grid item xs={12} md={8} lg={8}>
          <Stack sx={{ flexGrow: 1 }}>
            <Typography variant="label2">{account.institution}</Typography>
            <Typography variant="caption" sx={{ color: 'app.grey' }}>
              Reported {formatDate(account.lastReportedDate, '')}
            </Typography>
          </Stack>
        </Grid>

        <Grid alignItems="center" justifyContent="end" item xs={12} md={2} lg={2} sx={{ display: 'flex' }}>
          <Stack alignItems="center" justifyContent="center">
            <Box>
              <Box
                sx={{
                  backgroundColor: `${acountPaymentStatus?.color}1A`,
                  display: 'inline-block',
                  padding: '0.125rem 0.75rem',
                  borderRadius: '1.25rem',
                }}
              >
                <DotIcon color={acountPaymentStatus.color} size={3} />
                <Typography sx={{ color: acountPaymentStatus.color }} variant="label2">
                  {acountPaymentStatus?.label ?? ''}
                </Typography>
              </Box>
            </Box>
          </Stack>
        </Grid>
        <Grid alignItems="center" justifyContent="center" item xs={12} md={2} lg={2} sx={{ display: 'flex' }}>
          <Typography variant="label2">{formatMoney(account.balance, '')}</Typography>
        </Grid>
      </Grid>
      <IconButton
        disableFocusRipple
        disableRipple
        onClick={onOpenCloseDetails}
        size="small"
        sx={{ padding: '0', borderRadius: '50%', height: '2rem', width: '2rem' }}
      >
        <Stack
          alignItems="center"
          justifyContent="center"
          sx={{
            backgroundColor: 'app.lightGrey',
            borderRadius: '50%',
            height: '2rem',
            width: '2rem',
          }}
        >
          <AppIcon name={detailsOpen ? 'chevronup' : 'chevrondown'} />
        </Stack>
      </IconButton>
    </Stack>
  );
};

export default CreditCardAccountHeader;
