import { AppIcon } from 'src/components';
import { Box, Button, Stack, Typography, useTheme } from '@mui/material';
import Link from 'next/link';
import React from 'react';

const URL_BACK = '/credit-score-factors';
const URL_ACCOUNTS = '/dashboard/#/accounts'; // TODO: change when new "My Accounts" page will be implemented

/**
 * Renders "What Can Affect My Score?" header for "Credit Score Factors" internal pages
 */
const CreditScoreFactorsHeader = () => {
  const theme = useTheme();
  return (
    <Stack
      direction="row"
      justifyContent="space-between"
      sx={{ padding: '1rem 2rem', borderBottom: '1px solid', borderBottomColor: 'app.border' }}
    >
      {/* TODO: Refactor with AppButton "ghost" variant */}
      <Link href={URL_BACK} passHref>
        <Box component="a" alignItems="center" sx={{ display: 'flex', height: '1.875rem' }}>
          <AppIcon name="ArrowLeft" />
          <Typography variant="h2" sx={{ marginLeft: '.25rem' }}>
            What Can Affect My Score?
          </Typography>
        </Box>
      </Link>

      <Box sx={{ display: { xs: 'none', md: 'block' } }}>
        {/* TODO: Refactor with AppButton "outlined" variant */}
        <Link href={URL_ACCOUNTS} passHref>
          <Button
            component="a"
            sx={{
              border: '1px solid',
              borderColor: `${theme.palette.primary.main}`,
              borderRadius: '2.75rem',
              height: '1.875rem',
              padding: '0px',
            }}
          >
            <Typography variant="label2" sx={{ color: 'primary.main', padding: '0rem 0.875rem' }}>
              Go To My Accounts
            </Typography>
          </Button>
        </Link>
      </Box>
    </Stack>
  );
};

export default CreditScoreFactorsHeader;
