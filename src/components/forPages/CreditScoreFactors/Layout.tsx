import React from 'react';
import { Stack } from '@mui/material';
import Layout from 'src/components/layout/Smart/Layout';
import CreditScoreFactorsHeader from './Header';
import CreditScoreFactorsTabs from './Tabs';

interface Props {}

const CreditScoreFactorsLayout: React.FC<Props> = ({ children }) => {
  return (
    <Layout>
      <CreditScoreFactorsHeader />
      <Stack sx={{ paddingTop: '1.875rem', paddingBottom: '1.25rem' }}>
        <CreditScoreFactorsTabs />
      </Stack>
      <Stack sx={{ paddingX: '2.5rem', paddingBottom: '1.25rem' }}>{children}</Stack>
    </Layout>
  );
};

export default CreditScoreFactorsLayout;
