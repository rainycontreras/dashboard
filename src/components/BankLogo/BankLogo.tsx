import DefaultLogo from 'public/images/default_bank_logo.svg';
import Badora from 'public/images/bank_badora_logo.png';
import Hdfc from 'public/images/hdfc_logo.png';
import Icici from 'public/images/icici_logo.png';

/**
 * How to use:
 * 1. Import all required Logos into this file.
 * 2. Add logos with "unique lowercase names" into LOGOS object.
 * 3. Use logos everywhere in the App by their names in <BankLogo name="xxx" /> component
 * Important: properties of LOGOS object MUST be lowercase!
 * Note: You can use camelCase or UPPERCASE in the <BankLogo name="someIconByName" /> component
 */
const LOGOS: Record<string, StaticImageData> = {
  default: DefaultLogo,
  badora: Badora,
  'hdfc bank limited': Hdfc,
  'icici bank limited': Icici,
};

/**
 * Renders SVG icon by given Icon name
 * @param {string} [props.name] - name of the Icon to render
 * @param {string} [props.icon] - name of the Icon to render
 */
interface Props {
  name?: string; // Logo's name
}
const BankLogo: React.FC<Props> = ({ name, ...restOfProps }) => {
  const iconName = (name || 'default').trim().toLowerCase();
  const imageToRender = LOGOS[iconName] ?? LOGOS['default'];
  // TODO: config loaders for Image from 'next/image' and replace <img... with <Image...
  return <img alt={name} height={32} width={32} src={imageToRender.src} {...restOfProps} />;
};

export default BankLogo;
