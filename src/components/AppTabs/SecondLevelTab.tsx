import AppLink, { AppLinkProps } from 'src/components/AppLink';
import { Tab } from '@mui/material';
import { useRouter } from 'next/router';

interface Props extends AppLinkProps {
  label: string | React.ReactNode;
  selected?: boolean;
  value?: string;
}

/**
 * Renders application styled "Second Level" tab, automatically highlights self when the .to or .href property matches current url
 * @component SecondLevelTab
 * @param {string} [href] - external link, to open (in new Browser Tab) when user clicks on the Tab
 * @param {string|ReactNode} label - label of the Tab
 * @param {boolean} [selected] - overrides whether the Tab is selected or not
 * @param {string} [to] - internal link, to open when user clicks on the Tab
 * @param {string} [value] - value of the Tab, if not specified the .to or .href value is used
 */
const SecondLevelTab: React.FC<Props> = ({ href, label, selected: propSelected = false, to, value = href || to }) => {
  const router = useRouter();
  const selected =
    propSelected ||
    (to && router.pathname.endsWith(to.toString())) ||
    (href && router.pathname.endsWith(href.toString())); // Variant 1
  // const selected = propSelected || (to && router.pathname.includes(to.toString())) || (href && router.pathname.includes(href.toString())); // Variant 2

  return (
    <AppLink
      href={href}
      noLinkStyle // reset <a> styles TODO: Fix styles when selected by Tab key, I think it is :focus-visible style
      to={to}
    >
      <Tab
        label={label}
        sx={{
          backgroundColor: 'app.lightBlue',
          borderColor: selected ? 'primary.main' : 'app.blueBorder',
          borderRadius: '6px',
          borderStyle: 'solid',
          borderWidth: '1.5px',
          fontSize: '0.875rem', // 14px
          fontWeight: 600, // Bold text
          lineHeight: '1.125rem', // 18px
          marginRight: '0.5rem', // Spacing between tabs
          minHeight: 0, // Removes default Tab sizing
          minWidth: 0, // Removes default Tab sizing TODO: could be 2rem or something
          opacity: selected ? 1 : 0.5, // Grayed text for unselected tabs
          paddingX: '0.75rem', // 12px
          paddingY: '7.5px', // 9px on Figma minus 1.5px of .borderWidth
          textTransform: 'none', // No uppercase
          whiteSpace: 'nowrap', // Single line Tab's label inside <AppLink> component
        }}
        value={value}
      />
    </AppLink>
  );
};

export default SecondLevelTab;
