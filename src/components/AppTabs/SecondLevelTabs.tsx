import React from 'react';
import { Tabs } from '@mui/material';

interface Props {
  areaLabel?: string;
  hideScrollButtonsOnMobile?: boolean;
}

/**
 * Renders "Second Level" tabs, used on pages with multiple tabs
 * @component SecondLevelTabs
 * @param {string} [areaLabel] - label of the area, used for accessibility
 * @param {React.ReactNode} children - array of <NavTab> or MUI Tabs components
 * @param {boolean} [hideScrollButtonsOnMobile] - hides scroll buttons on mobile
 */
const SecondLevelTabs: React.FC<Props> = ({
  areaLabel = 'scrollable tabs',
  children,
  hideScrollButtonsOnMobile = false,
}) => {
  return (
    <Tabs
      aria-label={areaLabel}
      allowScrollButtonsMobile={!hideScrollButtonsOnMobile}
      scrollButtons={true} // "auto" would be better but it doesn't add margin on the left side, before tabs :(
      // TabIndicatorProps={{
      //   style: {
      //     display: 'none', // Don't show the selected tab's "underline" indicator
      //   },
      // }}
      textColor="secondary"
      value={false} // We don't use auto selected tabs, so we set value to false
      variant="scrollable"
      sx={{
        minHeight: 0, // Fixes vertical alignment of Scroll Buttons when there are visible
      }}
    >
      {children}
    </Tabs>
  );
};

export default SecondLevelTabs;
