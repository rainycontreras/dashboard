import React from 'react';
import { Tabs } from '@mui/material';

interface Props {
  areaLabel?: string;
  hideScrollButtonsOnMobile?: boolean;
  sx?: object;
}

/**
 * Renders "Top Level" tabs, used on pages with multiple tabs
 * @component TopLevelTabs
 * @param {string} [areaLabel] - label of the area, used for accessibility
 * @param {React.ReactNode} children - array of <NavTab> or MUI Tabs components
 * @param {boolean} [hideScrollButtonsOnMobile] - hides scroll buttons on mobile
 * @param {object} sx - styling object, passed to MUI Tabs components
 */
const TopLevelTabs: React.FC<Props> = ({
  areaLabel = 'navigation tabs',
  children,
  hideScrollButtonsOnMobile = false,
  sx: propSx = {},
  ...restOfProps
}) => {
  return (
    <Tabs
      aria-label={areaLabel}
      allowScrollButtonsMobile={!hideScrollButtonsOnMobile}
      scrollButtons={true} // "auto" would be better but it doesn't add margin on the left side, before tabs :(
      // TabIndicatorProps={{
      //   style: {
      //     display: 'none', // Don't show the selected tab's "underline" indicator
      //   },
      // }}
      textColor="secondary"
      value={false} // We don't use auto selected tabs, so we set value to false
      variant="scrollable"
      sx={{
        display: 'inline-flex',
        ...propSx,
      }}
      {...restOfProps}
    >
      {children}
    </Tabs>
  );
};

export default TopLevelTabs;
