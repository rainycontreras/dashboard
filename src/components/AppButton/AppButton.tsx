import { useMemo } from 'react';
import { Button, ButtonProps, ButtonTypeMap, Typography } from '@mui/material';
import { useTheme, PaletteColor } from '@mui/material/styles';
import { AppIcon } from '../';

type AppButtonSize = 'large' | 'medium' | 'small';
type AppButtonVariant = 'solid' | 'outline' | 'ghost';

interface Props extends Omit<ButtonProps, 'color' | 'size' | 'variant'> {
  color?: string;
  endIcon?: string | React.ReactNode;
  label?: string; // Alternate to text
  size?: AppButtonSize;
  text?: string; // Alternate to label
  variant?: AppButtonVariant;
  startIcon?: string | React.ReactNode;
  // Missing props
  component?: React.ElementType; // Could be NextLink, AppLink, etc.
  to?: string; // MuiLink prop
  href?: string; // AppLink prop
  openInNewTab?: boolean; // AppLink prop
  underline?: 'none' | 'hover' | 'always'; // MuiLink prop
}

const FONT_SIZES = {
  large: '1rem', // 16px
  medium: '0.875rem', // 14px
  small: '0.875rem', // 14px
};

const FONT_WEIGHTS = {
  large: '700',
  medium: '600',
  small: '500',
};

const LINE_HEIGHTS = {
  large: '1.3rem',
  medium: '1.1375rem',
  small: '1.1375rem',
};

const PADDINGS = {
  large: '0.75rem 1.25rem',
  medium: '0.5rem 1.25rem',
  small: '0.3125rem 0.875rem',
};

const VARIANT_TO_MUI: Record<string, ButtonTypeMap['props']['variant']> = {
  solid: 'contained',
  outline: 'outlined',
  ghost: 'text',
};

const VARIANT_TO_COLORSPACE: Record<string, 'contrastText' | 'main'> = {
  solid: 'contrastText',
  outline: 'main',
  ghost: 'main',
};

/**
 * Application styled Material UI Button
 * Figma Design: https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=230%3A438
 * @component AppButton
 * @param {string} [color] - color of the button body, "primary" by default
 * @param {string} [children] - content to render, overrides .label and .text
 * @param {string} [endIcon] - name of Icon to show after the button label
 * @param {string} [label] - text to render, alternate to .text
 * @param {AppButtonSize} size - button size type according to Figma Design
 * @param {string} [startIco] - name of Icon to show before the button label
 * @param {Array<func| object| bool> | func | object} [sx] - additional CSS styles to apply to the button
 * @param {string} [text] - text to render, alternate to .label
 * @param {AppButtonVariant} variant - button visual type according to Figma Design
 */
const AppButton: React.FC<Props> = ({
  color = 'primary',
  endIcon: endIcon,
  children,
  label,
  variant = 'solid', // TODO: Maybe 'outline' is more frequent?
  startIcon: startIcon,
  sx = { margin: 0.5 }, // TODO: Do we need default margin for buttons?
  text,
  underline = 'none',
  size = variant === 'ghost' ? 'small' : 'medium', // Ghost buttons are small by default
  ...restOfProps
}) => {
  const theme = useTheme();

  const iconColor: string | undefined = useMemo(
    () => theme.palette?.primary?.[VARIANT_TO_COLORSPACE[variant]],
    [theme, variant]
  );

  const iconStart: React.ReactNode = useMemo(
    () =>
      !startIcon ? undefined : typeof startIcon === 'string' ? (
        <AppIcon color={iconColor} name={String(startIcon)} />
      ) : (
        startIcon
      ),
    [startIcon, iconColor]
  );

  const iconEnd: React.ReactNode = useMemo(
    () =>
      !endIcon ? undefined : typeof endIcon === 'string' ? (
        <AppIcon color={iconColor} name={String(endIcon)} />
      ) : (
        endIcon
      ),
    [endIcon, iconColor]
  );

  const buttonSx: any = {
    borderRadius: '2.75rem', // 44px
    padding: PADDINGS[size],
    ...sx,
  };
  buttonSx.textTransform = 'none'; // This is a workaround for TypeScript error

  const labelSx = {
    fontWeight: FONT_WEIGHTS[size],
    fontSize: FONT_SIZES[size],
    lineHeight: LINE_HEIGHTS[size],
  };

  return (
    <Button
      color={color as ButtonTypeMap['props']['color']}
      endIcon={iconEnd}
      variant={VARIANT_TO_MUI[variant]}
      startIcon={iconStart}
      sx={buttonSx}
      {...restOfProps}
    >
      <Typography sx={labelSx}>{children || label || text}</Typography>
    </Button>
  );
};

export default AppButton;
