import { render, screen } from '@testing-library/react';
import AppButton from './';
import { AppThemeProvider } from '../../theme';

describe('AppButton component', () => {
  //   beforeEach(() => {});

  it('renders itself', async () => {
    let text = 'sample button';
    await render(
      <AppThemeProvider>
        <AppButton>{text}</AppButton>
      </AppThemeProvider>
    );
    let span = screen.getByText(text);
    expect(span).toBeDefined();
    expect(span).toHaveTextContent(text);
    let button = span.closest('button'); // parent <button> element
    expect(button).toBeDefined();
    expect(button).toHaveAttribute('type', 'button'); // not "submit" or "input" by default
  });

  it('supports .className property', async () => {
    let text = 'button with specific class';
    let className = 'someClassName';
    await render(
      <AppThemeProvider>
        <AppButton className={className}>{text}</AppButton>
      </AppThemeProvider>
    );
    let span = screen.getByText(text);
    expect(span).toBeDefined();
    let button = span.closest('button'); // parent <button> element
    expect(button).toBeDefined();
    expect(button).toHaveClass(className);
  });

  it('supports .label property', async () => {
    let text = 'button with label';
    await render(
      <AppThemeProvider>
        <AppButton label={text} />
      </AppThemeProvider>
    );
    let span = screen.getByText(text);
    expect(span).toBeDefined();
    let button = span.closest('button'); // parent <button> element
    expect(button).toBeDefined();
  });

  it('supports .text property', async () => {
    let text = 'button with text';
    await render(
      <AppThemeProvider>
        <AppButton text={text} />
      </AppThemeProvider>
    );
    let span = screen.getByText(text);
    expect(span).toBeDefined();
    let button = span.closest('button'); // parent <button> element
    expect(button).toBeDefined();
  });

  it('supports .startIcon property', async () => {
    let text = 'button with start icon';
    await render(
      <AppThemeProvider>
        <AppButton text={text} startIcon="ArrowLeft" />
      </AppThemeProvider>
    );
    let span = screen.getByText(text);
    let previousSibling = span?.previousSibling; // icon before text as <span> element
    expect(previousSibling).toBeDefined();
    expect(previousSibling).toHaveClass('MuiButton-startIcon');
  });

  it('supports .endIcon property', async () => {
    let text = 'button with end icon';
    await render(
      <AppThemeProvider>
        <AppButton text={text} endIcon="ArrowRight" />
      </AppThemeProvider>
    );
    let span = screen.getByText(text);
    let nextSibling = span?.nextSibling; // icon after text as <span> element
    expect(nextSibling).toBeDefined();
    expect(nextSibling).toHaveClass('MuiButton-endIcon');
  });
});
