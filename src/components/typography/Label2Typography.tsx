import { Typography, TypographyProps } from '@mui/material';

type Label2TypographyVariant = 'regular' | 'medium' | 'semibold';

interface Props extends Omit<TypographyProps, 'variant'> {
  variant?: Label2TypographyVariant;
}

const FONT_WEIGHTS = {
  regular: '400',
  medium: '500',
  semibold: '600',
};

/**
 * Application Label2 variant typography
 * Figma Design: https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design
 * @component Label2Typography
 * @param {Label2TypographyVariant} variant - typography visual type according to Figma Design
 */
const Label2Typography: React.FC<Props> = ({ variant = 'regular', children, ...restOfProps }) => {
  return (
    <Typography variant="label2" sx={{ fontWeight: FONT_WEIGHTS[variant] }} {...restOfProps}>
      {children}
    </Typography>
  );
};

export default Label2Typography;
