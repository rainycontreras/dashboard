import { Typography, TypographyProps } from '@mui/material';

type TextTypographyVariant = 'regular' | 'semibold';

interface Props extends Omit<TypographyProps, 'variant'> {
  variant?: TextTypographyVariant;
}

const FONT_WEIGHTS = {
  regular: 'normal',
  semibold: '600',
};

/**
 * Application Text variant typography
 * Figma Design: https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design
 * @component TextTypography
 * @param {TextTypographyVariant} variant - typography visual type according to Figma Design
 */
const TextTypography: React.FC<Props> = ({ variant = 'regular', children, ...restOfProps }) => {
  return (
    <Typography variant="text" sx={{ fontWeight: FONT_WEIGHTS[variant] }} {...restOfProps}>
      {children}
    </Typography>
  );
};

export default TextTypography;
