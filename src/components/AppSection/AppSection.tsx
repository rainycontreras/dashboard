import { Box, Stack, Typography } from '@mui/material';

interface Props {
  title?: string;
}

/**
 * General section component for Dashboard
 * @param {string} title - text in Section's header
 */
const AppSection: React.FC<Props> = ({ children, title }) => {
  return (
    <Box
      sx={{
        border: '1px solid',
        borderRadius: '6px',
        borderColor: 'app.border',
      }}
    >
      <Stack>
        <Stack
          alignItems="center"
          justifyContent="center"
          sx={{
            paddingTop: '1.25rem',
            paddingLeft: '1.5rem',
            paddingRight: '1.5rem',
          }}
        >
          <Stack justifyContent="start" sx={{ width: '100%' }}>
            <Typography variant="h1" sx={{ fontSize: '1rem' }}>
              {title}
            </Typography>
          </Stack>
        </Stack>
      </Stack>
      {children}
    </Box>
  );
};

export default AppSection;
