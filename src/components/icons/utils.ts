export const DEFAULT_SVG_ICON_COLOR = '#1D2145';
export const COLOR_SIDEBAR_ICON = 'white'; // '#FFFFFF'

export interface IconProps {
  color?: string;
}
