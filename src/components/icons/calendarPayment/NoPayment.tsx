import { APP_COLOR_STYLES } from '@/theme/AppColorStyles';
import { IconProps } from '@mui/material';

/**
 * Calendar Payment's No Payment  icon
 */
const CalendarPaymentNoPayment: React.FC<IconProps> = ({ color = APP_COLOR_STYLES.red }) => {
  return (
    <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="7" cy="7" r="7" fill={color} />
      <path d="M9 5L5 9" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
      <path d="M5 5L9 9" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );
};

export default CalendarPaymentNoPayment;
