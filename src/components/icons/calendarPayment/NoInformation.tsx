import { APP_COLOR_STYLES } from '@/theme/AppColorStyles';
import { IconProps } from '@mui/material';

/**
 * Calendar Payment's No Information icon
 */
const CalendarPaymentNoInformation: React.FC<IconProps> = ({ color = APP_COLOR_STYLES.blueBorder }) => {
  return (
    <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="7" cy="7" r="7" fill={color} />
    </svg>
  );
};

export default CalendarPaymentNoInformation;
