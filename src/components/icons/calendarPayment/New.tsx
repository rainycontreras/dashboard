import { APP_COLOR_STYLES } from '@/theme/AppColorStyles';
import { IconProps } from '@mui/material';

/**
 * Calendar Payment's New  icon
 */
const CalendarPaymentNew: React.FC<IconProps> = ({ color = APP_COLOR_STYLES.primary }) => {
  return (
    <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="7" cy="7" r="7" fill={color} />
      <path
        d="M6.16216 6.92264L5.38378 5.67622H5.35135V10H4V4H5.56757L7.83784 7.07736L8.61622 8.32378H8.64865V4H10V10H8.43243L6.16216 6.92264Z"
        fill="white"
      />
    </svg>
  );
};

export default CalendarPaymentNew;
