import { APP_COLOR_STYLES } from '@/theme/AppColorStyles';
import { IconProps } from '@mui/material';

/**
 * Calendar Payment's Done icon
 */
const CalendarPaymentDone: React.FC<IconProps> = ({ color = APP_COLOR_STYLES.green }) => {
  return (
    <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="7" cy="7" r="7" fill={color} />
      <path
        d="M10.3334 4.70825L5.75008 9.29159L3.66675 7.20825"
        stroke="white"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default CalendarPaymentDone;
