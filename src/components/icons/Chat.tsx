import React from 'react';
import { DEFAULT_SVG_ICON_COLOR, IconProps } from './utils';

/**
 * Renders "Chat" icon
 * Figma design https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=413%3A2428
 */
const ChatIcon: React.FC<IconProps> = ({ color = DEFAULT_SVG_ICON_COLOR }) => {
  return (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M12.0002 19.9779H5.33124C4.66524 19.9779 4.18524 19.3399 4.37024 18.7009L5.12924 16.0789L5.12524 16.0809C2.59924 11.8369 4.45924 6.19992 9.37124 4.43892C12.3612 3.36692 15.8052 4.24092 17.9202 6.60892C21.3932 10.4979 20.3372 16.3369 16.0892 18.8619C14.8292 19.6109 13.4142 19.9859 11.9992 19.9859"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default ChatIcon;
