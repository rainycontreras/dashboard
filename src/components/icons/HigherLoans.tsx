import React from 'react';
import { DEFAULT_SVG_ICON_COLOR, IconProps } from './utils';

/**
 * Renders "Higher Loans" icon
 * Figma design https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=1422%3A29281
 */
const HigherLoansIcon: React.FC<IconProps> = ({ color = DEFAULT_SVG_ICON_COLOR }) => {
  return (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M13.6366 8.38869C15.2734 9.51829 17.4006 12.2383 17.4006 14.6887C17.4006 17.1743 14.7118 19.1887 12.0006 19.1887C9.28939 19.1887 6.60059 17.1743 6.60059 14.6887C6.60059 12.2383 8.72779 9.51829 10.3646 8.38869L9.12059 5.42309C9.0705 5.33565 9.03933 5.23866 9.02909 5.13841C9.01884 5.03816 9.02976 4.93688 9.06113 4.84111C9.0925 4.74535 9.14362 4.65723 9.21119 4.58247C9.27876 4.50771 9.36127 4.44796 9.45339 4.40709C11.1068 3.8643 12.8904 3.8643 14.5438 4.40709C14.6362 4.44757 14.7192 4.50704 14.7871 4.58164C14.8551 4.65623 14.9067 4.74429 14.9384 4.84009C14.9702 4.9359 14.9815 5.03731 14.9715 5.13774C14.9615 5.23818 14.9306 5.3354 14.8806 5.42309L13.6366 8.38869Z"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M10.3643 8.38867H13.6363"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M6 18.5887C3.6 18.5887 3 16.1887 3 14.4975C3.03533 13.5065 3.31534 12.5397 3.81506 11.6832C4.31478 10.8267 5.01868 10.1071 5.864 9.58872L4.8528 8.13032C4.79048 8.04033 4.75396 7.93501 4.74721 7.82576C4.74045 7.7165 4.76371 7.60748 4.81447 7.5105C4.86522 7.41352 4.94155 7.33227 5.03517 7.27555C5.12879 7.21883 5.23614 7.18881 5.3456 7.18872H7.2"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M17.9998 18.5887C20.3998 18.5887 20.9998 16.1887 20.9998 14.4975C20.9645 13.5065 20.6845 12.5397 20.1847 11.6832C19.685 10.8267 18.9811 10.1071 18.1358 9.58872L19.147 8.13032C19.2093 8.04033 19.2458 7.93501 19.2526 7.82576C19.2594 7.7165 19.2361 7.60748 19.1853 7.5105C19.1346 7.41352 19.0583 7.33227 18.9646 7.27555C18.871 7.21883 18.7637 7.18881 18.6542 7.18872H16.7998"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M10.5 11.5H11.3908C12.1319 11.5 12.7327 12.1013 12.7327 12.8396C12.7327 13.5794 12.1325 14.1792 11.3908 14.1792H10.7233L12.7327 16.4119"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path d="M11.1699 11.5H13.6259" stroke={color} strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );
};

export default HigherLoansIcon;
