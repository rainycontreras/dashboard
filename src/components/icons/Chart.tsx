import React from 'react';
import { DEFAULT_SVG_ICON_COLOR, IconProps } from './utils';

/**
 * Renders "Bill" icon
 * Figma design https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=1594%3A29534
 */
const Chart: React.FC<IconProps> = ({ color = DEFAULT_SVG_ICON_COLOR }) => {
    return (
        <svg width="44" height="44" viewBox="0 0 44 44" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="22" cy="22" r="22" fill="white"/>
            <path opacity="0.3" fillRule="evenodd" clipRule="evenodd" d="M12.6696 22.2333L23.1667 24.3327V12.7383C27.7707 13.3124 31.3333 17.2399 31.3333 21.9994C31.3333 27.1541 27.1547 31.3327 22 31.3327C16.9236 31.3327 12.7937 27.2799 12.6696 22.2333Z" fill="#1BCA8C"/>
            <path fillRule="evenodd" clipRule="evenodd" d="M11.5705 19.6808C12.1383 15.0699 16.069 11.5 20.8334 11.5V21.5333L11.5705 19.6808Z" fill="#1BCA8C"/>
        </svg>
    );
};

export default Chart;
