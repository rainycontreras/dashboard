import React from 'react';
import { DEFAULT_SVG_ICON_COLOR, IconProps } from './utils';

/**
 * Renders "Bank" icon
 * Figma design https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=1566%3A30892
 */
const BankIcon: React.FC<IconProps> = ({ color = DEFAULT_SVG_ICON_COLOR }) => {
  return (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M18.6708 16.4363H5.33104C4.91606 16.4363 4.5616 16.7129 4.46651 17.102L4.03424 18.9261C3.90456 19.4708 4.32818 19.9895 4.89877 19.9895H19.1117C19.6823 19.9895 20.106 19.4708 19.9763 18.9261L19.544 17.102C19.4403 16.7129 19.0858 16.4363 18.6708 16.4363Z"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M10.0083 10.1255H6.45508V16.4366H10.0083V10.1255Z"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M17.106 10.1255H13.5527V16.4366H17.106V10.1255Z"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M19.9939 8.49127C19.9939 9.39038 19.2677 10.1252 18.3772 10.1252H5.61668C4.72621 10.1252 4 9.39903 4 8.49127C4 7.84287 4.37175 7.26363 4.95963 7.00427L11.3399 4.14265C11.7549 3.95245 12.2304 3.95245 12.654 4.14265L19.0343 7.00427C19.6221 7.26363 19.9939 7.84287 19.9939 8.49127Z"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M11.5566 7.42798H12.4385"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default BankIcon;
