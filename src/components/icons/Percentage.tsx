import React from 'react';
import { DEFAULT_SVG_ICON_COLOR, IconProps } from './utils';

/**
 * Renders "Percentage" icon
 * Figma design https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=701%3A8544
 */
const PercentageIcon: React.FC<IconProps> = ({ color = DEFAULT_SVG_ICON_COLOR }) => {
  return (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M5.25669 13.9691L3.71269 15.5131C2.32569 12.2351 2.96169 8.30513 5.63369 5.63413C8.87969 2.38813 13.9847 2.14513 17.5157 4.89313"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M6.4834 19.108C10.0164 21.855 15.1204 21.614 18.3654 18.367C21.0364 15.696 21.6734 11.766 20.2864 8.48804L18.7424 10.032"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M8.39941 15.6001L15.5994 8.40015"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M15.423 14.5765C15.6582 14.8117 15.6582 15.1909 15.423 15.4237C15.1878 15.6589 14.8086 15.6589 14.5758 15.4237C14.3406 15.1885 14.3406 14.8093 14.5758 14.5765C14.8062 14.3413 15.1878 14.3413 15.423 14.5765Z"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M9.4249 8.57466C9.65999 8.80974 9.65999 9.19115 9.4249 9.42384C9.18982 9.65892 8.80841 9.65892 8.57573 9.42384C8.34064 9.18875 8.34064 8.80734 8.57573 8.57466C8.81081 8.34198 9.18982 8.34198 9.4249 8.57466Z"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default PercentageIcon;
