import React from 'react';
import { DEFAULT_SVG_ICON_COLOR, IconProps } from './utils';

/**
 * Renders "Phone" icon
 * Figma design https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=421%3A2281
 */
const PhoneIcon: React.FC<IconProps> = ({ color = DEFAULT_SVG_ICON_COLOR }) => {
  return (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M18.1624 16.1056L17.1218 15.0651C16.602 14.5452 15.7592 14.5452 15.2403 15.0651L14.4223 15.883C14.2396 16.0657 13.9628 16.1269 13.7268 16.0231C12.5416 15.5042 11.3707 14.7173 10.3265 13.6732C9.28681 12.6335 8.5026 11.4678 7.98275 10.287C7.87452 10.0431 7.93751 9.75654 8.12646 9.56759L8.85922 8.83483C9.45448 8.23957 9.45448 7.3977 8.93463 6.87785L7.89404 5.83726C7.2012 5.14442 6.07811 5.14442 5.38527 5.83726L4.80687 6.41477C4.14951 7.07212 3.87539 8.02045 4.05282 8.9608C4.49105 11.2788 5.8377 13.8169 8.01025 15.9894C10.1828 18.162 12.7208 19.5086 15.0389 19.9469C15.9792 20.1243 16.9276 19.8502 17.5849 19.1928L18.1624 18.6153C18.8553 17.9225 18.8553 16.7994 18.1624 16.1056Z"
        stroke={color}
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12.8711 7.57235C13.7857 7.55993 14.7048 7.89881 15.4029 8.59697"
        stroke={color}
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M17.9117 6.08828C16.5198 4.69639 14.695 4 12.8711 4"
        stroke={color}
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M16.427 11.1288C16.4394 10.2141 16.1005 9.29509 15.4023 8.59692"
        stroke={color}
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M17.9111 6.08838C19.303 7.48027 19.9994 9.30507 19.9994 11.129"
        stroke={color}
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default PhoneIcon;
