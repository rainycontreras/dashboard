import React from 'react';
import { DEFAULT_SVG_ICON_COLOR, IconProps } from './utils';

/**
 * Renders "Calendar" icon
 * Figma design https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=413%3A2410
 * TODO: We also have another "Calendar" icon at https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=701%3A8543
 */
const CalendarIcon: React.FC<IconProps> = ({ color = DEFAULT_SVG_ICON_COLOR }) => {
  return (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M13.5703 17.5H16.2503" stroke={color} strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
      <path
        d="M14.9102 17.5V12.5L13.4502 13.82"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path d="M21 9H3" stroke={color} strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
      <path d="M8 2.87988V5.11988" stroke={color} strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
      <path d="M16 2.87988V5.11988" stroke={color} strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
      <path
        d="M18 21H6C4.343 21 3 19.657 3 18V7C3 5.343 4.343 4 6 4H18C19.657 4 21 5.343 21 7V18C21 19.657 19.657 21 18 21Z"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M8.25 13.46C8.409 12.91 8.895 12.5 9.496 12.5C10.224 12.5 10.815 13.09 10.815 13.819C10.815 14.471 10.286 15 9.634 15C10.286 15 10.815 15.529 10.815 16.181C10.815 16.909 10.225 17.5 9.496 17.5C8.895 17.5 8.409 17.09 8.25 16.54"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default CalendarIcon;
