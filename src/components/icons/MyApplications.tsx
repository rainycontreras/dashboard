import React from 'react';
import { DEFAULT_SVG_ICON_COLOR, IconProps } from './utils';

/**
 * Renders "My Applications" icon
 * Figma design https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=230%3A354
 */
const MyApplicationsIcon: React.FC<IconProps> = ({ color = DEFAULT_SVG_ICON_COLOR }) => {
  return (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M19 7.95977V15.0575C19 16.1034 18.0504 17 16.9424 17H10.0576C8.94964 17 8 16.1034 8 15.0575V5.94253C8 4.89655 8.94964 4 10.0576 4H14.8058C15.2014 4 15.518 4.14943 15.7554 4.37356L18.6043 7.06322C18.8417 7.28736 19 7.58621 19 7.95977Z"
        stroke={color}
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M11.3585 9.88672H15.9623"
        stroke={color}
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M11.3585 12.8301H15.9623"
        stroke={color}
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M7.05755 7C5.94964 7 5 7.89655 5 8.94253L5.00001 18C5.00001 19.1046 5.89544 20 7.00001 20H13.9424C15.0504 20 16 19.1034 16 18.0575"
        stroke={color}
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default MyApplicationsIcon;
