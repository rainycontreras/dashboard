import React from 'react';
import { DEFAULT_SVG_ICON_COLOR, IconProps } from './utils';

/**
 * Renders "My Account" icon
 * Figma Design https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=230%3A353
 */
const MyAccountIcon: React.FC<IconProps> = ({ color = DEFAULT_SVG_ICON_COLOR }) => {
  return (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M14.5996 10.3547H16.5996"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M14.5996 12.4883H15.5996"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M7 5H17C18.6569 5 20 6.34315 20 8V9.08333V12.9167V15C20 16.6569 18.6569 18 17 18H16.8H14.6667H9.33333H7.2H7.06667H7C5.34315 18 4 16.6569 4 15V9.08333V8C4 6.34314 5.34315 5 7 5Z"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M9.5 11.0881C10.3284 11.0881 11 10.4166 11 9.58813C11 8.75971 10.3284 8.08813 9.5 8.08813C8.67157 8.08813 8 8.75971 8 9.58813C8 10.4166 8.67157 11.0881 9.5 11.0881Z"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12 14.8882C12 13.7825 10.8795 12.8882 9.5 12.8882C8.12049 12.8882 7 13.7825 7 14.8882"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default MyAccountIcon;
