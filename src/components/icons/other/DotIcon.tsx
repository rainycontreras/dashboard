interface Props {
  color: string | undefined;
  size?: number;
}
/**
 * Dot icon
 * @param {string} color - Dot's color
 * @param {number} size - Dot's radius
 */
const DotIcon: React.FC<Props> = ({ color, size = 5 }) => {
  return (
    <svg width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="5" cy="5" r={size} fill={color} />
    </svg>
  );
};

export default DotIcon;
