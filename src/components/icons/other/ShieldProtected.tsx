/**
 * Shield Protected icon
 */

const ShieldProtected = () => {
  return (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M15.3189 10.342L11.1709 14.49L8.68188 12.001"
        stroke="#3ECE80"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M19.1 5.921C16.641 5.727 14.403 4.748 12.637 3.233C12.275 2.923 11.724 2.923 11.363 3.233C9.597 4.747 7.359 5.727 4.9 5.921C4.388 5.961 4 6.399 4 6.912V11.242C4 15.609 7.156 19.704 11.478 20.927C11.817 21.023 12.184 21.023 12.523 20.927C16.844 19.703 20 15.61 20 11.243V6.913C20 6.399 19.612 5.961 19.1 5.921Z"
        stroke="#3ECE80"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default ShieldProtected;
