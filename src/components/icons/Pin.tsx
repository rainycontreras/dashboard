import React from 'react';
import { DEFAULT_SVG_ICON_COLOR, IconProps } from './utils';

/**
 * Renders "Pin" icon
 * Figma design https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=240%3A648
 */
const PinIcon: React.FC<IconProps> = ({ color = DEFAULT_SVG_ICON_COLOR }) => {
  return (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M16.7894 15.2026C17.7111 15.6031 18.2631 16.113 18.2631 16.6683C18.2631 17.9561 15.2941 19 11.6316 19C7.96905 19 5 17.9561 5 16.6683C5 16.113 5.55204 15.6031 6.47368 15.2026"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <circle
        cx="11.6319"
        cy="9.42095"
        r="1.47368"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M7.21094 9.80311C7.21094 3.39894 16.053 3.39899 16.053 9.80311C16.053 11.9784 14.7934 13.822 13.6189 15.0682C12.8699 15.8629 12.4954 16.2603 11.6321 16.2603C10.7688 16.2603 10.3943 15.863 9.64524 15.0683C8.47065 13.8221 7.21094 11.9785 7.21094 9.80311Z"
        stroke={color}
        strokeWidth="1.5"
      />
    </svg>
  );
};

export default PinIcon;
