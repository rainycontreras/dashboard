import React from 'react';
import { DEFAULT_SVG_ICON_COLOR, IconProps } from './utils';

/**
 * Renders "Rupee" icon
 * Figma design https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=684%3A5456
 */
const RupeeIcon: React.FC<IconProps> = ({ color = DEFAULT_SVG_ICON_COLOR }) => {
  return (
    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M10.6404 16L7.54934 11.8395H5.5V10.2063H8.55693C9.67268 10.2063 10.2647 9.71347 10.333 8.72779H5.5V7.09456H10.333C10.2989 6.54441 10.1281 6.16619 9.82068 5.95988C9.52467 5.74212 9.10342 5.63324 8.55693 5.63324H5.5V4H14.5V5.63324H11.204V5.70201C11.4317 5.72493 11.6708 5.84527 11.9213 6.06304C12.1717 6.2808 12.3539 6.62464 12.4677 7.09456H14.5V8.72779H12.519C12.4165 9.47278 12.1546 10.1032 11.7334 10.6189C11.3235 11.1232 10.7429 11.4613 9.99146 11.6332L13.3899 16H10.6404Z"
        fill={color}
      />
    </svg>
  );
};

export default RupeeIcon;
