import React from 'react';
import { DEFAULT_SVG_ICON_COLOR, IconProps } from './utils';

/**
 * Renders "Copy" icon
 * Figma design https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=230%3A355
 */
const CopyIcon: React.FC<IconProps> = ({ color = DEFAULT_SVG_ICON_COLOR }) => {
  return (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M18.4006 9.6001H11.2006C10.3169 9.6001 9.60059 10.3164 9.60059 11.2001V18.4001C9.60059 19.2838 10.3169 20.0001 11.2006 20.0001H18.4006C19.2842 20.0001 20.0006 19.2838 20.0006 18.4001V11.2001C20.0006 10.3164 19.2842 9.6001 18.4006 9.6001Z"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M6.4 14.4H5.6C5.17565 14.4 4.76869 14.2314 4.46863 13.9314C4.16857 13.6313 4 13.2243 4 12.8V5.6C4 5.17565 4.16857 4.76869 4.46863 4.46863C4.76869 4.16857 5.17565 4 5.6 4H12.8C13.2243 4 13.6313 4.16857 13.9314 4.46863C14.2314 4.76869 14.4 5.17565 14.4 5.6V6.4"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default CopyIcon;
