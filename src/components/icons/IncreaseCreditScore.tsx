import React from 'react';
import { DEFAULT_SVG_ICON_COLOR, IconProps } from './utils';

/**
 * Renders "Increase Credit Score" icon
 * Figma design https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=1346%3A28105
 */
const IncreaseCreditScoreIcon: React.FC<IconProps> = ({ color = DEFAULT_SVG_ICON_COLOR }) => {
  return (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M16.0963 9.08008L15.7283 9.51208C13.6883 11.8961 10.9523 13.5841 7.9043 14.3441"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
      />
      <path
        d="M13.752 9.08008H16.096V11.4161"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M17.3333 20H6.66667C5.19378 20 4 18.736 4 17.1765V6.82353C4 5.264 5.19378 4 6.66667 4H17.3333C18.8062 4 20 5.264 20 6.82353V17.1765C20 18.736 18.8062 20 17.3333 20Z"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default IncreaseCreditScoreIcon;
