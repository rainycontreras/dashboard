import React from 'react';
import { DEFAULT_SVG_ICON_COLOR, IconProps } from './utils';

/**
 * Renders "DropDown" icon
 * Figma design https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=790%3A9527
 */
const DropDown: React.FC<IconProps> = ({ color = DEFAULT_SVG_ICON_COLOR }) => {
  return (
    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
      <g opacity="0.5">
        <path
          d="M6.66675 8.33325L10.0001 11.6666L13.3334 8.33325"
          stroke={color}
          strokeWidth="1.5"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </g>
    </svg>
  );
};

export default DropDown;
