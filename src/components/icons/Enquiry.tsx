import React from 'react';
import { DEFAULT_SVG_ICON_COLOR, IconProps } from './utils';

/**
 * Renders "Enquiry" icon
 * Figma design https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=1422%3A29066
 */
const EnquiryIcon: React.FC<IconProps> = ({ color = DEFAULT_SVG_ICON_COLOR }) => {
  return (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M5.5 11.7207C5.5 8.28509 8.28509 5.5 11.7207 5.5C15.1563 5.5 17.9414 8.28509 17.9414 11.7207C17.9414 15.1563 15.1563 17.9414 11.7207 17.9414C8.28509 17.9414 5.5 15.1563 5.5 11.7207ZM11.7207 4C7.45667 4 4 7.45667 4 11.7207C4 15.9847 7.45667 19.4414 11.7207 19.4414C13.5826 19.4414 15.2907 18.7822 16.6241 17.6846L19.0325 20.093C19.3254 20.3859 19.8003 20.3859 20.0932 20.093C20.3861 19.8001 20.3861 19.3252 20.0932 19.0323L17.6848 16.6239C18.7823 15.2905 19.4414 13.5825 19.4414 11.7207C19.4414 7.45667 15.9847 4 11.7207 4Z"
        fill={color}
      />
      <path
        d="M11.66 12.5V12.29C11.66 11.61 12.08 11.25 12.5 10.96C12.91 10.68 13.3199 10.32 13.3199 9.66003C13.3199 8.74003 12.58 8 11.66 8C10.74 8 10 8.74003 10 9.66003"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M11.6553 14.8899H11.6643"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default EnquiryIcon;
