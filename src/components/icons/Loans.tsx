import React from 'react';
import { DEFAULT_SVG_ICON_COLOR, IconProps } from './utils';

/**
 * Renders "Loans" icon
 * Figma design https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=1422%3A29282
 */
const LoansIcon: React.FC<IconProps> = ({ color = DEFAULT_SVG_ICON_COLOR }) => {
  return (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M3 13.8V21" stroke={color} strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
      <path
        d="M3 19.8H16.2C16.2 19.1635 15.9471 18.553 15.497 18.1029C15.047 17.6529 14.4365 17.4 13.8 17.4H10.8C10.8 16.7635 10.5471 16.153 10.097 15.7029C9.64696 15.2529 9.03651 15 8.39999 15H3"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path d="M7.7998 17.4001H10.7998" stroke={color} strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
      <path
        d="M18.592 16.8064C19.4639 16.1569 20.1423 15.282 20.5541 14.2758C20.9659 13.2696 21.0956 12.1701 20.9292 11.0957C20.7629 10.0213 20.3067 9.01252 19.6099 8.17796C18.9131 7.3434 18.0019 6.71461 16.9744 6.35921L18.4568 3.82322C18.5057 3.73988 18.5314 3.64502 18.5314 3.54842C18.5314 3.45181 18.5057 3.35695 18.4568 3.27362C18.4061 3.1895 18.3343 3.12003 18.2486 3.07206C18.1629 3.02408 18.0662 2.99925 17.968 3.00002H12.032C11.934 2.99953 11.8377 3.02449 11.7523 3.07245C11.6668 3.12041 11.5954 3.18973 11.5448 3.27362C11.4959 3.35695 11.4702 3.45181 11.4702 3.54842C11.4702 3.64502 11.4959 3.73988 11.5448 3.82322L13.0272 6.35921C11.8534 6.76501 10.835 7.52611 10.1134 8.5369C9.39172 9.54769 9.0026 10.758 9 12"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M13.5 9H14.3908C15.1319 9 15.7327 9.60126 15.7327 10.3396C15.7327 11.0794 15.1325 11.6792 14.3908 11.6792H13.7233L15.7327 13.9119"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path d="M14.1699 9H16.6259" stroke={color} strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );
};

export default LoansIcon;
