import React from 'react';
import { DEFAULT_SVG_ICON_COLOR, IconProps } from './utils';

/**
 * Renders "CreditScore" icon
 * Figma design https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=230%3A352
 */
const CreditScoreIcon: React.FC<IconProps> = ({ color = DEFAULT_SVG_ICON_COLOR }) => {
  return (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M12 20C16.4183 20 20 16.4183 20 12C20 7.58172 16.4183 4 12 4C7.58172 4 4 7.58172 4 12C4 16.4183 7.58172 20 12 20Z"
        stroke={color}
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M7.2002 12C7.2002 9.35995 9.3602 7.19995 12.0002 7.19995C12.7202 7.19995 13.4402 7.35995 14.0802 7.67995"
        stroke={color}
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M11.7588 14.2394L16.1588 9.35938"
        stroke={color}
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12.6082 14.2392C12.6082 14.7086 12.2276 15.0892 11.7582 15.0892C11.2888 15.0892 10.9082 14.7086 10.9082 14.2392C10.9082 13.7697 11.2888 13.3892 11.7582 13.3892C12.2276 13.3892 12.6082 13.7697 12.6082 14.2392Z"
        stroke={color}
        strokeWidth="1.5"
      />
    </svg>
  );
};

export default CreditScoreIcon;
