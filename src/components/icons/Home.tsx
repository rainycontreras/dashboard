import React from 'react';
import { DEFAULT_SVG_ICON_COLOR, IconProps } from './utils';

/**
 * Renders "Home" icon
 * Figma design https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=230%3A351
 * TODO: We also have another "Home" icon at https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=701%3A8539
 */
const HomeIcon: React.FC<IconProps> = ({ color = DEFAULT_SVG_ICON_COLOR }) => {
  return (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M6 14V18.8667C6 19.419 6.44772 19.8667 7 19.8667H9.26667C9.81895 19.8667 10.2667 19.419 10.2667 18.8667V15.6C10.2667 15.3171 10.379 15.0458 10.5791 14.8458C10.7791 14.6457 11.0504 14.5333 11.3333 14.5333H12.4C12.6829 14.5333 12.9542 14.6457 13.1542 14.8458C13.3543 15.0458 13.4667 15.3171 13.4667 15.6V18.8667C13.4667 19.419 13.9144 19.8667 14.4667 19.8667H16.7333C17.2856 19.8667 17.7333 19.419 17.7333 18.8667V14"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M4 12.5611L11.2462 5.31492C11.3448 5.21516 11.4621 5.13596 11.5915 5.0819C11.7209 5.02784 11.8598 5 12 5C12.1402 5 12.2791 5.02784 12.4085 5.0819C12.5379 5.13596 12.6552 5.21516 12.7538 5.31492L20 12.5611"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M15 7.07644V6H17.6667V9.74311"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default HomeIcon;
