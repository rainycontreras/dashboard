import React from 'react';
import { DEFAULT_SVG_ICON_COLOR, IconProps } from './utils';

/**
 * Renders "Check Circle" icon
 * Figma design https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=426%3A2837
 */
const CheckCircleIcon: React.FC<IconProps> = ({ color = DEFAULT_SVG_ICON_COLOR }) => {
  return (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M12 20C7.58133 20 4 16.4187 4 12C4 7.58133 7.58133 4 12 4C16.4187 4 20 7.58133 20 12C20 16.4187 16.4187 20 12 20Z"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path d="M16 10L11 15L8 12" stroke={color} strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );
};

export default CheckCircleIcon;
