import React from 'react';
import { DEFAULT_SVG_ICON_COLOR, IconProps } from './utils';

/**
 * Renders "Refer" icon
 * Figma design https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design?node-id=230%3A350
 */
const ReferIcon: React.FC<IconProps> = ({ color = DEFAULT_SVG_ICON_COLOR }) => {
  return (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M17.8671 12.5333V5.06667C17.8671 4.78377 17.7547 4.51246 17.5547 4.31242C17.3547 4.11238 17.0834 4 16.8005 4H7.20046C6.91756 4 6.64625 4.11238 6.44621 4.31242C6.24617 4.51246 6.13379 4.78377 6.13379 5.06667V12.5333"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M19.3876 11.5074C19.2872 11.5174 19.1915 11.555 19.1112 11.6159L13.3156 15.8115C12.9401 16.1056 12.477 16.2654 12 16.2654C11.5231 16.2654 11.0599 16.1056 10.6845 15.8115L4.8676 11.6159C4.78722 11.555 4.69155 11.5174 4.59117 11.5074C4.49078 11.4973 4.38957 11.5152 4.29871 11.5591C4.20842 11.6014 4.13224 11.6688 4.07927 11.7533C4.02631 11.8378 3.9988 11.9358 4.00004 12.0355V16.9333V17.9999C4.00004 19.1045 4.89547 19.9999 6.00004 19.9999H17.9787C19.0833 19.9999 19.9787 19.1045 19.9787 17.9999V16.9333V12.0355C19.9799 11.9358 19.9524 11.8378 19.8995 11.7533C19.8465 11.6688 19.7703 11.6014 19.68 11.5591C19.5892 11.5152 19.488 11.4973 19.3876 11.5074Z"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M10.5 7H11.3908C12.1319 7 12.7327 7.60126 12.7327 8.3396C12.7327 9.07944 12.1325 9.6792 11.3908 9.6792H10.7233L12.7327 11.9119"
        stroke={color}
        strokeWidth="1.17215"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path d="M11.1699 7H13.6259" stroke={color} strokeWidth="1.17215" strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );
};

export default ReferIcon;
