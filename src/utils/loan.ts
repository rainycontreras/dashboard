import { apiLoanEmiAmount, apiLoanInterestAndTenure } from 'src/api/loan';
import { log } from './log';
import { AccountDetails, Simulation, UserContext } from './type';

/**
 * Fill loan account's emi or tenure
 * @param {AccountDetails} account - Loan account to fill
 * @param {UserContext} userContext - User's session
 * @returns {AccountDetails}
 */
export async function fillEmiTerm(account: AccountDetails, userContext: UserContext): Promise<AccountDetails> {
  try {
    if (!account.emi && account.term && account.balance && account.interestRate) {
      let emiResponse = await apiLoanEmiAmount(account.balance, account.interestRate, account.term, userContext);
      if (emiResponse) {
        account.emi = Math.round(emiResponse);
      }
    } else if (!account.term && account.emi && account.sanctionAmount && account.interestRate) {
      let loanTenure = await apiLoanInterestAndTenure(account.balance, account.interestRate, account.emi, userContext);
      if (loanTenure) {
        account.term = Math.round(loanTenure.tenure);
      }
    }
  } catch (error) {
    log.error('Error in apiLoanEmiAmount', error);
  }
  return account;
}

/**
 * Calculate interest saved, aditional payments and total payments for loan's simulation at (5% emi, 10% emi, 15% emi)
 * @param {AccountDetails} loan - Loan to simulate
 * @param {UserContext} userContext - User's session
 * @returns {Simulation[]}
 */
export async function calculateLoanSimulations(loan: AccountDetails, userContext: UserContext): Promise<Simulation[]> {
  let simulations: Simulation[] = [];

  if (loan.emi && loan.balance) {
    const extraPaidAt5 = 0.05 * loan.emi;
    const extraPaidAt10 = 0.1 * loan.emi;
    const extraPaidAt15 = 0.15 * loan.emi;
    const newEMIAt5 = 1.05 * loan.emi;
    const newEMIAt10 = 1.1 * loan.emi;
    const newEMIAt15 = 1.15 * loan.emi;

    const interestSimulations = await Promise.all([
      apiLoanInterestAndTenure(loan.balance, loan.interestRate, loan.emi, userContext),
      apiLoanInterestAndTenure(loan.balance, loan.interestRate, newEMIAt5, userContext),
      apiLoanInterestAndTenure(loan.balance, loan.interestRate, newEMIAt10, userContext),
      apiLoanInterestAndTenure(loan.balance, loan.interestRate, newEMIAt15, userContext),
    ]);

    if (interestSimulations) {
      const originalInterestToPay = Math.round(interestSimulations[0]?.interestAmount ?? 0);
      const interestPaidAt5 = Math.round(interestSimulations[1]?.interestAmount ?? 0);
      const interestPaidAt10 = Math.round(interestSimulations[2]?.interestAmount ?? 0);
      const interestPaidAt15 = Math.round(interestSimulations[3]?.interestAmount ?? 0);
      const interestSavedAt5 = Math.round(originalInterestToPay - interestPaidAt5);
      const interestSavedAt10 = Math.round(originalInterestToPay - interestPaidAt10);
      const interestSavedAt15 = Math.round(originalInterestToPay - interestPaidAt15);

      simulations = [
        {
          extraPaid: 15,
          interestSaved: interestSavedAt15,
          additionalPayment: extraPaidAt15,
          totalPayment: newEMIAt15,
        },
        {
          extraPaid: 10,
          interestSaved: interestSavedAt10,
          additionalPayment: extraPaidAt10,
          totalPayment: newEMIAt10,
        },
        {
          extraPaid: 5,
          interestSaved: interestSavedAt5,
          additionalPayment: extraPaidAt5,
          totalPayment: newEMIAt5,
        },
      ];
    }
  }
  return simulations;
}
