import { useCallback } from 'react';
import { useHistory } from 'react-router-dom';

// export const URL_BACK_TO_HOME = '../dashboard/#/'; // Logged user's dashboard at the current root domain
// export const URL_GOLD_LOAN = '../dashboard/#/gold-loan-online'; // Link to "Gold Loan" application

/**
 * Disables "Back" button for current page
 * Usage: Call function in useEffect(  ,[]) or directly
 */
export function disableBackNavigation() {
  window.history.pushState(null, '', window.location.href);
  window.onpopstate = function () {
    window.history.go(1);
  };
}

/**
 * Navigates to the specified URL with options
 */
export function navigateTo(url: string, replaceInsteadOfPush = false, optionalTitle = '') {
  if (replaceInsteadOfPush) {
    window.history.replaceState(null, optionalTitle, url);
  } else {
    window.history.pushState(null, optionalTitle, url);
  }
}

/**
 * Hook to navigate using history from react-router-dom
 */
export function useNavigate() {
  const history = useHistory();

  return useCallback(
    (url: string, replacePath = false) => {
      if (replacePath) {
        history.replace(url);
      } else {
        history.push(url);
      }
    },
    [history]
  );
}
