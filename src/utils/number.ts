/**
 * Verify whether the given value is a number and it matches additional criteria.
 */
export function isValidNumber(value: number, minValue?: number, maxValue?: number): boolean {
  // Is finite number check
  if (!Number.isFinite(value)) {
    return false;
  }
  // Min/Max value check
  if (Number.isFinite(minValue) && value < Number(minValue)) {
    return false;
  }
  if (Number.isFinite(maxValue) && value > Number(maxValue)) {
    return false;
  }
  // We pass all check, so the value is valid
  return true;
}

/**
 * Parses string to get a number.
 * @returns {number} numeric value from the string, 0 in case of NaN, infinity, etc.
 */
export function parseNumber(value: string): number {
  const number = parseFloat(value);
  if (!number) return 0;
  return number;
}

/**
 * Converts "money currency" string into a number.
 * @param {string} value - "money currency" value
 * @returns {number} numeric value from the string, 0 in case of NaN, infinity, etc.
 */
export function moneyToNumber(value: string): number {
  const clearedValue = value.replace(/,/g, '');
  return parseNumber(clearedValue);
}

/**
 * Converts number to "money currency" string with commas, decimal separator, etc.
 * @param {string | number} value - string or number to process
 * @returns {string} value in in "money currency" format
 */
export function numberToMoney(value: string | number): string {
  let number: number;
  if (typeof value === 'string') {
    number = moneyToNumber(value);
  } else {
    number = Number(value);
  }

  if (number === undefined) return '';

  const result = number.toLocaleString('en-IN'); // Indian 10,00,000 format
  // console.log('numberToMoney()', value, result)
  return result;
}

export function formatMoney(value: number | undefined, defaultValue?: string): string | undefined {
  if (value === undefined) {
    return defaultValue;
  }
  return `₹ ${numberToMoney(value)}`;
}

/**
 * Format a number as percentage
 * @param {number | undefined} value  - Value to format
 * @param {string | undefined} defaultValue - Default value if value is undefined
 * @returns {number} - value formatted
 */
export function formatPercentage(value: number | undefined, defaultValue?: string): string | undefined {
  if (!value) {
    return defaultValue;
  }
  return `${value.toFixed(2)}%`;
}

/**
 * Format number
 * @param {number | undefined} value  - Value to format
 * @param {string | undefined} defaultValue - Default value if value is undefined
 * @returns {string} - value formatted
 */
export function formatNumber(value: number | undefined, defaultValue: string | undefined = ''): string {
  if (!value && value !== 0) {
    return defaultValue;
  }
  return `${value.toString()}`;
}

/**
 * Format number, firt number is converted to a integet using Math.round
 * @param {number | undefined} value  - Value to format
 * @param {string | undefined} defaultValue - Default value if value is undefined
 * @returns {string} - value formatted
 */
export function formatInt(value: number | undefined, defaultValue: string | undefined = ''): string {
  if (!value && value !== 0) {
    return defaultValue;
  }
  return `${Math.round(value).toString()}`;
}
