const LOG_LEVELS = ['trace', 'debug', 'info', 'warn', 'error', 'silent'];
const CURRENT_LOG_LEVEL = process.env.NEXT_PUBLIC_LOG_LEVEL || 'silent';

/**
 * Lightweight logger with minimal log level restrictions
 * @class Log
 */
class Log {
  private readonly _levelNumber: number;

  constructor(private readonly _levelName: string) {
    this._levelNumber = Log.nameToNumber(_levelName);
  }

  static nameToNumber(levelName: string = CURRENT_LOG_LEVEL): number {
    return LOG_LEVELS.indexOf(levelName?.toLowerCase());
  }

  get level() {
    return this._levelNumber;
  }

  get name() {
    return this._levelName;
  }

  public notify(logLevel: string, ...args: any[]): void {
    const level = Log.nameToNumber(logLevel);
    if (level < this._levelNumber) {
      return; // We don't need to notify
    }

    switch (level) {
      case 0: // trace
        console.trace(...args);
        break;
      case 1: // debug
        console.debug(...args);
        break;
      case 2: // info
        console.info(...args);
        break;
      case 3: // warn
        console.warn(...args);
        break;
      case 4: // error
        console.error(...args);
        break;
      default:
        // Do nothing
        break;
    }
  }

  public trace(...args: any[]): void {
    this.notify('trace', ...args);
  }
  public debug(...args: any[]): void {
    this.notify('debug', ...args);
  }
  public info(...args: any[]): void {
    this.notify('info', ...args);
  }
  public warn(...args: any[]): void {
    this.notify('warn', ...args);
  }
  public error(...args: any[]): void {
    this.notify('error', ...args);
  }

  public log(...args: any[]): void {
    this.notify(this._levelName, ...args);
  }
}

export const log = new Log(CURRENT_LOG_LEVEL);

// log.log('Log level:', CURRENT_LOG_LEVEL);
console.log('Log level:', CURRENT_LOG_LEVEL);
