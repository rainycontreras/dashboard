import { SortedReadonlyArray } from 'typescript';

// Helper to read object properties as object['name']
export type ObjectPropByName = Record<string, any>;

/**
 * Data for "Page Link" in SideBar and other UI elements
 */
export declare interface LinkToPage {
  title?: string;
  href?: string;
  icon?: string;
}

/**
 * Type for http request
 */
export interface ApiRequest {
  body?: string; // TODO: Maybe we also need to support {} and FormData
}

/**
 * Type for budget app api response
 */
export interface ApiResponse {
  status: number;
  message: string;
  error: boolean;
  data: any;
}
/**
 * Typo for Login api response
 */
export interface LoginResponse {
  sessionToken: string;
  customerProfile: CustomerProfile;
}
/**
 * Type for Dashboard Customer Profile
 */
export interface CustomerProfile {
  customerID: string;
  enteredFirstName: string;
  enteredLastName: string;
  enteredPanNumber: string;
  creditScore: number;
  emailAddress: string;
  personalLoanEligibility: string;
  dateOfLatestCreditScore: Date;
  referralCode: string;
}
/**
 * Type for generic api response
 */
export interface ResponseData<T> {
  data?: T;
}

/**
 * Type for client side user session
 */
export interface UserContext {
  userId?: string;
  sessionToken?: string;
}

/**
 * Type for each tier in progress bar chart
 */
export interface ColoredBarTier {
  label: string;
  min: number;
  max: number;
  color: string;
}
/**
 * Type for each part in arc chart
 */
export interface PathColor {
  key: string;
  path: string;
  color: string;
}

/**
 * Type for pointer in scoring component chart
 */
export interface ScoreValuePointer {
  pointerPath: string;
  fillPath: string;
  xPosition: number;
  yPosition: number;
}

export enum ScoringFactorImpact {
  High,
  Medium,
  Low,
}

export interface CreditHistoryReport {
  ReportData: ReportData;
  InquiryResponseHeader: InquiryResponseHeader;
}

export interface ReportData {
  AccountDetails: AccountDetails;
  AccountSummary: AccountSummary;
  EnquirySummary: EnquirySummary;
  IDAndContactInfo: IDAndContactInfo;
  Enquiries: Enquiry[];
  Score: Score;
}
export interface Score {
  Value: string;
}

export interface InquiryResponseHeader {
  Date: string;
}
export interface Enquiry {
  Amount: string;
  Date: string;
  Institution: string;
  RequestPurpose: string;
  Time: string;
  attributes: { [key: string]: string };
}
export interface AccountSummary {
  NoOfAccounts: string;
}
export interface EnquirySummary {
  Past24Months: string;
}
export interface AccountDetails {
  Account: Account[];
}

export interface IDAndContactInfo {
  AddressInfo: AddressInfo[];
  PersonalInfo: PersonalInfo;
}
export interface AddressInfo {
  Address: string;
  Postal: string;
  State: string;
}

export interface PersonalInfo {
  Name: PersonalInfoName;
}
export interface PersonalInfoName {
  FirstName: string;
  MiddleName: string;
}

export interface Account {
  AccountNumber: string;
  AccountStatus: string;
  AccountType: string;
  Balance: string;
  CreditLimit: string;
  DateClosed: string;
  DateOpened: string;
  DateReported: string;
  HighCredit: string;
  History48Months: { [key: string]: AccountPaymentHistory[] };
  Institution: string;
  LastPayment: string;
  LastPaymentDate: string;
  Open: string;
  OwnershipType: string;
  PastDueAmount: string;
  SanctionAmount: string;
  InstallmentAmount: string;
  InterestRate: string;
  RepaymentTenure: string;
}

export interface AccountDetails {
  accountType?: string;
  amountPastDue?: number;
  balance: number;
  creditLimit?: number;
  displayType: CreditScoreFactorDisplayType;
  dueFromLastPayment?: number;
  highCredit?: number;
  institution: string;
  lastPaymentDate?: Date;
  lastReportedDate: Date;
  totalMissedPayments: number;
  missedPayments30Days: number;
  missedPayments60Days: number;
  missedPayments90Days: number;
  monthlyPayment?: number;
  number: string;
  openedDate: Date;
  closedDate?: Date;
  paidAmount?: number;
  paidPercentage?: number;
  paymentCalendar: PaymentCalendar;
  paymentStatus: string;
  remarks?: string;
  responsibility?: string;
  term?: number;
  sanctionAmount?: number;
  status?: string;
  savings?: Savings;
  type?: string;
  worstPaymentStatus?: string;
  emi: number;
  installmentAmount: number;
  interestRate: number;
  lastPayment: number;
}

export interface AccountPaymentHistory {
  attributes: { [key: string]: string };
  PaymentStatus: string;
}

export interface CreditCardAccountAccumulator {
  balance: number;
  creditLimit: number;
  creditLimitMissed: number;
  percentage: number;
  showPercentage: boolean;
}

export interface CreditScoreFactor {
  displayType?: string;
  value?: number;
}

export enum PaymentCalendarStatus {
  NO_INFORMATION,
  OK,
  NO_PAYMENT,
  NEW,
  CLOSED,
}

export enum CreditScoreFactorDisplayType {
  MONEY,
  PERCENTAGE,
  ABSOLUTE,
}

export interface PaymentCalendar {
  years: number[];
  paymentCalendar: { [key: string]: { [key: string]: PaymentCalendarStatus } };
}

export interface CreditCardUtilization {
  balance: number;
  creditLimit: number;
  displayType: CreditScoreFactorDisplayType;
  utilization: number;
  accounts: AccountDetails[];
}
export interface PaymentHistory {
  onTimePayments: number;
  totalPayments: number;
  paymentHistory: number;
  accounts: AccountDetails[];
}
export interface AgeCreditHistory {
  ageCreditHistory: number;
  accounts: AccountDetails[];
}

export interface AgeCreditHistory {
  ageCreditHistory: number;
  accounts: AccountDetails[];
}

export enum AccountDetailFieldAligment {
  LEFT,
  RIGHT,
}

export interface CreditEnquiries {
  totalCreditEnquiries: number;
  enquiries: AccountEnquiry[];
}

export interface AccountEnquiry {
  id: string;
  date: Date;
  institution: string;
  purpose: string;
}

export interface AgeCreditHistoryFactor {
  ageAccounts: number;
  totalAccounts: number;
}

export interface CreditReport {
  personalInformation: PersonalInformation;
  mixBorrowing: MixBorrowing;
  enquiries?: AccountEnquiry[];
  accounts?: AccountDetails[];
}

export interface PersonalInformation {
  name?: string;
  employmentInfo?: EmploymentInfo;
  addresses?: Address[];
}
export interface EmploymentInfo {
  name: string;
  position: string;
  address: string;
}

export interface Address {
  address: string;
  postal: string;
  state: string;
}

export interface MixBorrowing {
  secure: number;
  unsecure: number;
}
export interface TotalAccounts {
  open: number;
  closed: number;
  total: number;
}
export interface AccountHeaderProps {
  account: AccountDetails;
  onOpenCloseDetails: () => void;
  detailsOpen: boolean;
}
export interface CreditScoreFactors {
  score?: number;
  scoreDate?: Date;
  creditCardUtilization?: CreditScoreFactor;
  paymentHistory?: CreditScoreFactor;
  negativeStatusAccounts?: CreditScoreFactor;
  ageCreditHistory?: CreditScoreFactor;
  creditInquires?: CreditScoreFactor;
  numberOfAccounts?: CreditScoreFactor;
}

export type CreditScoreFactorType =
  | 'creditCardUtilization'
  | 'paymentHistory'
  | 'negativeStatusAccounts'
  | 'ageCreditHistory'
  | 'creditInquires'
  | 'numberOfAccounts';

export interface CreditReportCompare {
  daysToUpdate: number;
  oldReport?: CreditScoreFactors;
  newReport?: CreditScoreFactors;
  status?: CreditReportCompareStatus;
}

export interface CreditReportCompareStatus {
  creditCardUtilization?: ScoreStatus;
  paymentHistory?: ScoreStatus;
  negativeStatusAccounts?: ScoreStatus;
  ageCreditHistory?: ScoreStatus;
  creditInquires?: ScoreStatus;
  numberOfAccounts?: ScoreStatus;
}
export interface ScoreStatus {
  label: string;
  color: string;
}

export interface CreditScoreFactorItem {
  label: string;
  accessor: CreditScoreFactorType;
  formatter(factor?: CreditScoreFactor): string | undefined;
}

export interface TableColumn {
  id: string;
  label: string;
}

export interface LoanInterestTenure {
  interestAmount: number;
  tenure: number;
}

export interface LoanSimulation {
  loans?: AccountDetails[];
  selectedLoan?: AccountDetails;
  simulations?: Simulation[];
}

export interface Simulation {
  extraPaid: number;
  interestSaved: number;
  additionalPayment: number;
  totalPayment: number;
}

export interface CustomerSalary {
  customerID: number;
  dateCreated: Date;
  dateModified: Date;
  salaryAmount: number;
  seqNo: number;
}

export interface LoanLastPayment {
  otherloanLastPayment: number;
  personalLastPayment: number;
  homeloanLastPayment: number;
  autoloanLastPayment: number;
}

export interface LoanRecommendations {
  savingAmount: number;
  accountType: string;
  loanBalance: number;
  sanctionAmount: number;
}

export interface Savings {
  amount?: number;
}
