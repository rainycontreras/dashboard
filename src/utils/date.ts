import { format as formatDateFn } from 'date-fns';
/**
 * Month names
 */
export const MONTH_NAMES = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

/**
 * Format a date value using pattern: MMM dd, yyyy
 * @param {date} value - Date to format
 * @param {string} defaultValue - Default value if date is undefined
 * @param {string} format - Pattern to format value
 * @returns {String} - Date formatted
 */
export function formatDate(
  value: Date | undefined,
  defaultValue?: string,
  format: string = 'MMM dd, yyyy'
): string | undefined {
  if (!value) {
    return defaultValue;
  }
  if (format === 'MMM dd, yyyy') {
    const day = value.toLocaleString('default', { day: '2-digit' });
    const month = value.toLocaleString('default', { month: 'short' });
    const year = value.toLocaleString('default', { year: 'numeric' });
    return `${month} ${day}, ${year}`;
  }

  return formatDateFn(value, format);
}

/**
 * Format duration in months to: 'Y Yrs M Mo'
 * @param {number} value - duration in months
 * @param {string} defaultValue - returned value if value is not present
 * @returns {string} - Duration formatted
 */
export function formatDuration(value: number | undefined, defaultValue: string = ''): string {
  if (!value) {
    return defaultValue;
  }
  const years = Math.floor(value / 12);
  const months = value - years * 12;
  const formatYears = years > 0 ? `${years} Yrs ` : '';
  const formatMonths = months > 0 ? `${months} Mo` : '';
  return `${formatYears}${formatMonths}`;
}
