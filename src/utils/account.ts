import { fetchCreditScoreHistory } from '@/api/customer';
import { APP_COLOR_STYLES } from '@/theme/AppColorStyles';
import { differenceInMonths, parse } from 'date-fns';
import parseISO from 'date-fns/parseISO';
import { MONTH_NAMES } from './date';
import { parseNumber } from './number';
import {
  Account,
  AccountDetails,
  AccountEnquiry,
  AccountPaymentHistory,
  AgeCreditHistoryFactor,
  CreditCardAccountAccumulator,
  ScoreStatus,
  CreditHistoryReport,
  CreditScoreFactor,
  CreditScoreFactorDisplayType,
  CreditScoreFactors,
  Enquiry,
  PaymentCalendar,
  PaymentCalendarStatus,
  UserContext,
} from './type';

/**
 * Group Account Payment History by Payment Status
 * @param {AccountPaymentHistory[]} accountPaymentHistory
 * @returns
 */
export function groupAccountPaymentHistoryByPaymentStatus(accountPaymentHistory: AccountPaymentHistory[]): {
  [key: string]: AccountPaymentHistory[];
} {
  return accountPaymentHistory.reduce(function (
    rv: { [key: string]: AccountPaymentHistory[] },
    x: AccountPaymentHistory
  ) {
    (rv[x.PaymentStatus] = rv[x.PaymentStatus] || []).push(x);
    return rv;
  },
  {} as { [key: string]: AccountPaymentHistory[] });
}

/**
 * Parse Equifax's Account Payment History into a Dashboard's payment calendar structure
 * @param {AccountPaymentHistory[]} accountPaymentHistory - Equifax's Account Payment History
 * @returns {PaymentCalendar} - Dashboard's payment calendar
 */
export function buildPaymentCalendar(accountPaymentHistory: AccountPaymentHistory[]): PaymentCalendar {
  const paymentCalendar: { [key: string]: { [key: string]: PaymentCalendarStatus } } = {};
  const years: number[] = [];

  for (const accountPayment of accountPaymentHistory) {
    const paymentDate = accountPayment.attributes?.key;
    if (paymentDate == null) continue;

    const monthYear = paymentDate.split('-');

    const month = MONTH_NAMES[(parseInt(monthYear[0]) ?? 1) - 1];

    const year = parseInt(monthYear[1]); // Convert to number to sort desc
    if (!years.includes(year)) {
      years.push(year);
    }

    let months = paymentCalendar[year];
    if (!months) {
      months = {};
      paymentCalendar[year.toString()] = months;
    }
    if (accountPayment.PaymentStatus === '000') {
      months[month] = PaymentCalendarStatus.OK;
    } else if (accountPayment.PaymentStatus === 'NEW') {
      months[month] = PaymentCalendarStatus.NEW;
    } else if (accountPayment.PaymentStatus === '*') {
      months[month] = PaymentCalendarStatus.NO_INFORMATION;
    } else if (accountPayment.PaymentStatus === 'CLSD') {
      months[month] = PaymentCalendarStatus.CLOSED;
    } else {
      months[month] = PaymentCalendarStatus.NO_PAYMENT;
    }
  }

  const descSortedYears = years.sort((a, b) => b - a);

  return { years: descSortedYears, paymentCalendar };
}

/**
 * Parse Equifax customer account into Dashboard customer account
 * @param {Account} account - Equifax customer account
 * @param {boolean} [isParsePaymentCalendar=true] - Controls whether or not the account's payment calendar is generated.
 * @returns {AccountDetails} - Customer account parsed
 */
export function parseAccountDetail(account: Account, isParsePaymentCalendar: boolean = true): AccountDetails {
  const displayType =
    account.CreditLimit && account.CreditLimit?.trim() !== ''
      ? CreditScoreFactorDisplayType.PERCENTAGE
      : CreditScoreFactorDisplayType.MONEY;

  const lastReportedDate = parseISO(account.DateReported);
  const openedDate = parseISO(account.DateOpened);
  const closedDate =
    account.AccountStatus?.toLowerCase() === 'current account' ? undefined : parseISO(account.DateClosed);
  const remarks = undefined;
  const responsibility = account.OwnershipType;
  const term = parseNumber(account.RepaymentTenure);
  const accountType = account.AccountType;

  // Values for field Highest Balance
  const highCredit = parseNumber(account.HighCredit);
  const sanctionAmount = parseNumber(account.SanctionAmount);

  const balance = parseNumber(account.Balance);
  const creditLimit = parseNumber(account.CreditLimit);

  const paidAmount = sanctionAmount - balance;

  const paidPercentage = balance ? paidAmount / balance : 0;

  const institution = account.Institution;

  const accountPaymentHistory = account.History48Months?.Month;
  // Group account payment history by payment status
  const historyByPaymentStatus = groupAccountPaymentHistoryByPaymentStatus(accountPaymentHistory);

  const totalMissedPayments =
    (historyByPaymentStatus['01+']?.length ?? 0) +
    (historyByPaymentStatus['31+']?.length ?? 0) +
    (historyByPaymentStatus['61+']?.length ?? 0) +
    (historyByPaymentStatus['91+']?.length ?? 0) +
    (historyByPaymentStatus['121+']?.length ?? 0) +
    (historyByPaymentStatus['181+']?.length ?? 0);

  const missedPayments30Days = historyByPaymentStatus['01+']?.length ?? 0;
  const missedPayments60Days = historyByPaymentStatus['31+']?.length ?? 0;
  const missedPayments90Days = historyByPaymentStatus['61+']?.length ?? 0;

  let paymentCalendar = undefined;
  if (isParsePaymentCalendar) {
    paymentCalendar = buildPaymentCalendar(accountPaymentHistory);
  }

  const dueFromLastPayment = account.LastPayment && parseNumber(account.LastPayment);
  const lastPaymentDate = account.LastPaymentDate && parseISO(account.LastPaymentDate);
  const paymentStatus = account.AccountStatus;
  const amountPastDue = account.PastDueAmount && parseNumber(account.PastDueAmount);
  const worstPaymentStatus = undefined;
  const monthlyPayment = undefined;

  const number = account.AccountNumber;
  const installmentAmount = parseNumber(account.InstallmentAmount);
  const lastPayment = parseNumber(account.LastPayment);
  const emi = account.InstallmentAmount ? installmentAmount : lastPayment;
  const interestRate = parseNumber(account.InterestRate);

  const status = account.AccountStatus;
  const type = account.AccountType;

  return {
    accountType,
    amountPastDue,
    balance,
    creditLimit,
    displayType,
    dueFromLastPayment,
    emi,
    highCredit,
    installmentAmount,
    interestRate,
    institution,
    lastPaymentDate,
    lastPayment,
    lastReportedDate,
    totalMissedPayments,
    missedPayments30Days,
    missedPayments60Days,
    missedPayments90Days,
    monthlyPayment,
    number,
    openedDate,
    paidAmount,
    paidPercentage,
    closedDate,
    paymentCalendar,
    paymentStatus,
    remarks,
    responsibility,
    sanctionAmount,
    status,
    type,
    term,
    worstPaymentStatus,
  } as AccountDetails;
}

export const ENQUIRY_PURPOSES: { [key: string]: string } = {
  '00': 'Other',
  '01': 'Auto Loan',
  '02': 'Housing Loan',
  '03': 'Property Loan',
  '04': 'Loan Against Shares / Securities',
  '05': 'Personal Loan',
  '06': 'Consumer Loan',
  '07': 'Gold Loan',
  '08': 'Education Loan',
  '09': 'Loan to Professional',
  '10': 'Credit Card',
  '11': 'Lease',
  '12': 'Overdraft',
  '13': 'Two-Wheeler Loan',
  '14': 'Non-Funded Credit Facilty',
  '15': 'Loan Aganist Bank Deposits',
  '16': 'Fleet Card',
  '17': 'Commercial Vehicle Loan',
  '18': 'Telco - Wireless',
  '19': 'Telco - Broadband',
  '20': 'Telco - Landline',
  '31': 'Secured Credit Card',
  '32': 'Used Car Loan',
  '33': 'Construction Equipment Loan',
  '34': 'Tractor Loan',
  '35': 'Corporate Credit Card',
  '3A': 'Auto Lease',
  '51': 'Business Loan',
  '52': 'Business Loan- Priority Sector- Small Business',
  '53': 'Business Loan - Priority Sector- Agriculture',
  '54': 'Business Loan - Priority Sector- Others',
  '55': 'Business Non-Funded Credit Facilty',
  '56': 'Business Non-Funded Credit Facilty - Priority',
  '57': 'Business Non-Funded Credit Facilty - Priority',
  '58': 'Business Non-Funded Credit Facilty - Priority',
  '59': 'Business Loan Aganist Bank Deposits',
  '60': 'Staff Loan',
  '8A': 'Disclosure',
};
/**
 * Parse Customer Equifax account's response to Credit Score Factor
 * @param {Account[]} accounts - Customer accounts from Equifax
 * @param {boolean} [useBalances=false] - Controls whether returns balance's usage percentage if possible or credit card's balance
 */
export function parseCreditCardUtilization(
  accounts: Account[],
  useBalances: boolean = false
): CreditScoreFactor | undefined {
  const creditCardAccounts = accounts.filter(
    (account) => account.AccountType === 'Credit Card' && account.Open === 'Yes'
  );
  if (creditCardAccounts.length < 1) {
    return undefined;
  }

  const creditCardAccountAccumulation = creditCardAccounts.reduce(
    (accumulator: CreditCardAccountAccumulator, account: Account) => {
      if (!account.CreditLimit) {
        accumulator.creditLimitMissed += 1;
      }
      accumulator.balance += parseNumber(account.Balance);
      accumulator.creditLimit += parseNumber(account.CreditLimit);
      return accumulator;
    },
    {
      balance: 0,
      creditLimit: 0,
      creditLimitMissed: 0,
      percentage: 0,
      showPercentage: false,
    }
  );

  const result: CreditScoreFactor = {};

  if (!useBalances && creditCardAccountAccumulation.creditLimitMissed === 0) {
    result.displayType = 'PERCENTAGE';
    result.value = Math.round(
      (creditCardAccountAccumulation.balance / creditCardAccountAccumulation.creditLimit) * 100
    );
  } else {
    result.displayType = 'MONEY';
    result.value = creditCardAccountAccumulation.balance;
  }

  return result;
}

/**
 * Calculates intermediary values for Payment History percentage value
 * @param {Account[]} accounts - Customer accounts from Equifax
 * @returns {any} payments - Object that includes number of on-time payments and total payments
 */
export function parseIntermediatePaymentHistory(accounts: Account[]): any | undefined {
  const accountsHistory = accounts.map((account) => account.History48Months?.Month ?? []).flat();

  // Group account history by payment status
  const historyByPaymentStatus = groupAccountPaymentHistoryByPaymentStatus(accountsHistory);
  const onTimePayments = historyByPaymentStatus['000']?.length ?? 0 + historyByPaymentStatus['*']?.length ?? 0;
  const totalPayments =
    historyByPaymentStatus['000']?.length ??
    0 + historyByPaymentStatus['01+']?.length ??
    0 + historyByPaymentStatus['31+']?.length ??
    0 + historyByPaymentStatus['61+']?.length ??
    0 + historyByPaymentStatus['91+']?.length ??
    0 + historyByPaymentStatus['121+']?.length ??
    0 + historyByPaymentStatus['181+']?.length ??
    0;

  return { onTimePayments, totalPayments };
}

/**
 * Parse Customer Equifax account's response to Payment History Factor
 * @param {Account[]} accounts - Customer accounts from Equifax
 */
export function parsePaymentHistory(accounts: Account[]): CreditScoreFactor | undefined {
  const { onTimePayments, totalPayments } = parseIntermediatePaymentHistory(accounts);
  const result: CreditScoreFactor = {};

  result.displayType = 'ABSOLUTE';
  result.value = totalPayments > 0 ? Math.round(((onTimePayments ?? 0) / totalPayments) * 100) : 0;

  return result;
}

/**
 * Parse Customer Equifax account's response to Negative Status Accounts
 * @param {Account[]} accounts - Customer accounts from Equifax
 */
export function parseNegativeStatusAccounts(accounts: Account[]): CreditScoreFactor | undefined {
  const accountsHistory = accounts
    .filter((account) => account.AccountStatus === 'Current Account')
    .map((account) => account.History48Months?.Month ?? [])
    .flat();

  // Group account history by payment status
  const historyByPaymentStatus = accountsHistory.reduce(function (
    rv: { [key: string]: AccountPaymentHistory[] },
    x: AccountPaymentHistory
  ) {
    (rv[x.PaymentStatus] = rv[x.PaymentStatus] || []).push(x);
    return rv;
  },
  {} as { [key: string]: AccountPaymentHistory[] });

  const result: CreditScoreFactor = {};

  const openAccounts =
    (historyByPaymentStatus['01+']?.length ?? 0) +
    (historyByPaymentStatus['31+']?.length ?? 0) +
    (historyByPaymentStatus['61+']?.length ?? 0) +
    (historyByPaymentStatus['91+']?.length ?? 0) +
    (historyByPaymentStatus['121+']?.length ?? 0) +
    (historyByPaymentStatus['181+']?.length ?? 0);

  const negativeAccounts =
    (historyByPaymentStatus['PWOS']?.length ?? 0) +
    (historyByPaymentStatus['RES']?.length ?? 0) +
    (historyByPaymentStatus['SET']?.length ?? 0) +
    (historyByPaymentStatus['DBT']?.length ?? 0) +
    (historyByPaymentStatus['WOF']?.length ?? 0) +
    (historyByPaymentStatus['WDF']?.length ?? 0) +
    (historyByPaymentStatus['SF']?.length ?? 0) +
    (historyByPaymentStatus['SFWD']?.length ?? 0) +
    (historyByPaymentStatus['WDWO']?.length ?? 0) +
    (historyByPaymentStatus['SWDW']?.length ?? 0);

  result.displayType = 'ABSOLUTE';
  result.value = openAccounts + negativeAccounts;

  return result;
}

/**
 * Parse Customer Equifax account's response to Age Credit History Factor
 * @param {Account[]} accounts - Customer accounts from Equifax
 */
export function parseAgeCreditHistory(accounts: Account[]): AgeCreditHistoryFactor | undefined {
  const loanAccounts = accounts.filter(
    (account) =>
      (((account.AccountType?.toLowerCase() === 'housing loan' ||
        account.AccountType?.toLowerCase() === 'property loan') &&
        parseNumber(account.Balance) > 0) ||
        (account.AccountType?.toLowerCase() !== 'housing loan' &&
          account.AccountType?.toLowerCase() !== 'property loan')) &&
      account.Open === 'Yes'
  );

  if (loanAccounts.length === 0) {
    return undefined;
  }

  const ageAccounts = loanAccounts
    .map((account) => {
      let valueTodayDate = new Date();
      let accounDateOpened = parseISO(account.DateOpened);

      return differenceInMonths(valueTodayDate, accounDateOpened);
    })
    .reduce((value: number, month: number) => value + month, 0);

  return { ageAccounts, totalAccounts: loanAccounts.length };
}

/**
 * Parse Customer Equifax account's response to Credit Inquires Factor
 * @param {CreditHistoryReport} report - Customer credit report from Equifax
 */
export function parseCreditInquires(report: CreditHistoryReport): CreditScoreFactor | undefined {
  const result: CreditScoreFactor = {};

  result.displayType = 'ABSOLUTE';
  result.value = parseNumber(report.ReportData.EnquirySummary?.Past24Months);
  return result;
}

/**
 * Parse Customer Equifax account's response to Number of Accounts Factor
 * @param {CreditHistoryReport} report - Customer credit report from Equifax
 */
export function parseNumberOfAccounts(report: CreditHistoryReport): CreditScoreFactor | undefined {
  const result: CreditScoreFactor = {};

  result.displayType = 'ABSOLUTE';
  result.value = parseNumber(report.ReportData.AccountSummary?.NoOfAccounts);
  return result;
}

/**
 * Check if account is open
 * @param {Account} account  - Account
 * @returns {boolean} - True if account is open
 */
export const ACCOUNT_OPEN = (account: Account): boolean =>
  (((account.AccountType?.toLowerCase() === 'housing loan' || account.AccountType?.toLowerCase() === 'property loan') &&
    parseNumber(account.Balance) > 0) ||
    (account.AccountType?.toLowerCase() !== 'housing loan' &&
      account.AccountType?.toLowerCase() !== 'property loan')) &&
  account.Open === 'Yes';

/**
 * Check if account is a secured account
 * @param {Account} account  - Account
 * @returns {boolean} - True if account is secured
 */
export const ACCOUNT_SECURED = (account: Account): boolean => {
  const type = account.AccountType?.toLowerCase();
  return (
    type === 'auto loan' ||
    type === 'housing loan' ||
    type === 'property loan' ||
    type === 'loan against shares/securities' ||
    type === 'gold loan' ||
    type === 'two-wheeler loan' ||
    type === 'loan against bank deposits' ||
    type === 'commercial vehicle loan' ||
    type === 'secured credit card' ||
    type === 'used car loan' ||
    type === 'tractor loan' ||
    type === 'business loan against bank deposits' ||
    type === 'auto lease'
  );
};

/**
 * Parse Customer Equifax enquiries's response to dashboard format
 * @param {Enquiry[]} enquiries - Equifax enquiries
 * @returns  {AccountEnquiry[]} - Equifax enquiries dashboard formatted
 */
export function parseAccountEnquiries(enquiries: Enquiry[] | undefined): AccountEnquiry[] | undefined {
  return enquiries?.map((enquiry) => {
    const date = enquiry.Date && enquiry.Date !== '' ? parseISO(enquiry.Date) : undefined;
    const purpose: string = ENQUIRY_PURPOSES[enquiry.RequestPurpose];
    return {
      id: `${enquiry.Institution}::${enquiry.Date}::${enquiry.attributes['seq']}`,
      date,
      purpose,
      institution: enquiry.Institution,
    } as AccountEnquiry;
  });
}

/**
 * Indexes to fetch credit report
 */
export const CREDITREPORT_CURRENT = 1;
export const CREDITREPORT_LAST = 2;

/**
 * Retrieve the customer's creditscore factors
 * @param {UserContext} userContext - Customer's session data
 * @param {number} reportIndex - Credit report's index to look for
 * @param {boolean} [creditCardUtilizationShowBalance=false] - For Credit Card Utilization factor controls whether shows balance's usage percentage if possible or credit card's balance
 * @returns {CreditScoreFactors} - Credit score factors
 */
export async function fetchCreditScoreFactors(
  userContext: UserContext,
  reportIndex: number,
  creditCardUtilizationShowBalance: boolean = false
): Promise<CreditScoreFactors> {
  let response = await fetchCreditScoreHistory(userContext, reportIndex);
  let creditCardUtilization = undefined;
  let paymentHistory = undefined;
  let negativeStatusAccounts = undefined;
  let ageCreditHistory = undefined;
  let creditInquires = undefined;
  let numberOfAccounts = undefined;
  let score = undefined;
  let scoreDate = undefined;

  if (response?.InquiryResponseHeader?.Date) {
    scoreDate =
      response.InquiryResponseHeader.Date !== ''
        ? parse(response.InquiryResponseHeader.Date, 'dd-MM-yyyy', new Date())
        : undefined;
  }

  if (response?.ReportData) {
    if (response.ReportData.Score?.Value) {
      score = parseNumber(response.ReportData.Score?.Value);
    }
    if (response.ReportData.AccountDetails?.Account) {
      const accounts = response.ReportData.AccountDetails.Account;
      creditCardUtilization = parseCreditCardUtilization(accounts, creditCardUtilizationShowBalance);
      paymentHistory = parsePaymentHistory(accounts);
      negativeStatusAccounts = parseNegativeStatusAccounts(accounts);

      const ageCreditHistoryFactor = parseAgeCreditHistory(accounts);
      ageCreditHistory =
        ageCreditHistoryFactor && ageCreditHistoryFactor.totalAccounts > 0
          ? {
              displayType: 'ABSOLUTE',
              value: Math.floor(ageCreditHistoryFactor.ageAccounts / ageCreditHistoryFactor.totalAccounts),
            }
          : undefined;

      creditInquires = parseCreditInquires(response);
      numberOfAccounts = parseNumberOfAccounts(response);
    }
  }
  return {
    score,
    scoreDate,
    creditCardUtilization,
    paymentHistory,
    negativeStatusAccounts,
    ageCreditHistory,
    creditInquires,
    numberOfAccounts,
  };
}

/**
 * Calculate age of credit history factor status based on variation
 * @param {number} variation - Difference between old and new credit score factor
 */
export function calculateAgeCreditHistoryComparisonStatus(variation: number = 0): ScoreStatus {
  let label = '';
  let color = '';
  if (variation > 10) {
    label = 'Excellent';
    color = APP_COLOR_STYLES.green;
  } else if (variation > 3 && variation <= 10) {
    label = 'Very Good';
    color = APP_COLOR_STYLES.androidGreen;
  } else if (variation >= 0 && variation <= 3) {
    label = 'Good';
    color = APP_COLOR_STYLES.maize;
  } else {
    label = 'Bad';
    color = APP_COLOR_STYLES.red;
  }
  return { label, color };
}

/**
 * Calculate credit enquiries factor status based on variation
 * @param {number} variation - Difference between old and new credit score factor
 */
export function calculateCreditEnquiriesComparisonStatus(variation: number = 0): ScoreStatus {
  let label = '';
  let color = '';
  if (variation > 5) {
    label = 'Bad';
    color = APP_COLOR_STYLES.red;
  } else if (variation > 3) {
    color = APP_COLOR_STYLES.marigold;
    label = 'poor';
  } else if (variation > 0) {
    label = 'Good';
    color = APP_COLOR_STYLES.maize;
  } else if (variation === 0) {
    label = 'Excellent';
    color = APP_COLOR_STYLES.green;
  } else if (variation < 2) {
    label = 'Very Good';
    color = APP_COLOR_STYLES.androidGreen;
  }

  return { label, color };
}

/**
 * Calculate payment history factor status based on variation
 * @param {number} variation - Difference between old and new credit score factor
 */
export function calculatePaymentHistoryComparisonStatus(variation: number = 0): ScoreStatus {
  let label = '';
  let color = '';

  if (variation >= 3) {
    label = 'Excellent';
    color = APP_COLOR_STYLES.green;
  } else if (variation >= 2) {
    label = 'Very Good';
    color = APP_COLOR_STYLES.green;
  } else if (variation >= 1) {
    label = 'Good';
    color = APP_COLOR_STYLES.green;
  } else if (variation <= -1) {
    label = 'Average';
    color = APP_COLOR_STYLES.maize;
  } else if (variation <= -4) {
    label = 'Poor';
    color = APP_COLOR_STYLES.red;
  } else if (variation === 0) {
    label = 'Good';
    color = APP_COLOR_STYLES.maize;
  }

  return { label, color };
}

/**
 * Calculate total accounts factor status based on variation
 * @param {number} variation - Difference between old and new credit score factor
 */
export function calculateTotalAccountsComparisonStatus(variation: number = 0): ScoreStatus {
  let label = '';
  let color = '';

  if (variation >= 20) {
    label = 'Excellent';
    color = APP_COLOR_STYLES.green;
  } else if (variation >= 11) {
    label = 'Very Good';
    color = APP_COLOR_STYLES.green;
  } else if (variation >= 6) {
    label = 'Good';
    color = APP_COLOR_STYLES.green;
  } else if (variation >= 0) {
    label = 'Good';
    color = APP_COLOR_STYLES.maize;
  } else if (variation <= -1) {
    label = 'Poor';
    color = APP_COLOR_STYLES.red;
  }

  return { label, color };
}

/**
 * Calculate score status based on score value
 * @param {number} value - Score value
 */
export function calculateScoreStatus(value: number = 0): ScoreStatus {
  let label = '';
  let color = '';

  if (value >= 750) {
    label = 'Excellent';
    color = APP_COLOR_STYLES.green;
  } else if (value >= 700) {
    label = 'Good';
    color = APP_COLOR_STYLES.marigold;
  } else if (value >= 600) {
    label = 'Average';
    color = APP_COLOR_STYLES.maizeScore;
  } else {
    label = 'Poor';
    color = APP_COLOR_STYLES.lightCarminePink;
  }

  return { label, color };
}
