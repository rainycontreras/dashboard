import { restClient } from './apiClient';
import { CustomerSalary, UserContext } from 'src/utils/type';

/**
 * Returns user's annual salary
 * @param {UserContext} userContext - Customer's session data
 * @returns {CustomerSalary | undefined}
 */
export async function apiSalaryAnnual(userContext: UserContext): Promise<CustomerSalary | undefined> {
  const ENDPOINT = `budget/api/0/getLatestSalary/${userContext.userId}`;
  const response = await restClient<CustomerSalary>('GET', ENDPOINT, undefined, userContext);
  return response;
}
