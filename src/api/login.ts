import { restClient } from './apiClient';
import { LoginResponse } from 'src/utils/type';

const ENDPOINT = 'budget/api/0/profile/session';

/**
 * Calls API to login a customer
 * @param {string} mobileNumber - customer's mobile number
 * @param {string} pinCode - customer's password
 * @returns {LoginResponse} object with .sessionToken and .customerProfile
 */
export async function login(mobileNumber: string, pinCode: string): Promise<LoginResponse> {
  return restClient<LoginResponse>('POST', ENDPOINT, JSON.stringify({ mobileNumber, pinCode }));
}

export default login;
