import { ApiResponse, ObjectPropByName, UserContext } from 'src/utils/type';
import { redirectToAuth } from './utils';

const BASE_URL = process.env.apiBasePath; // TODO: add/remove trailing slash

/**
 * Universal function to execute any HTTP call to our API
 * @param {string} method - the HTTP method (GET, POST, PUT, DELETE, etc.) to perform
 * @param {string} url - relative address of API endpoint
 * @param {string} body - data to to send as .body of request // TODO: Maybe we also need to support {} and FormData
 * @param {string} params - data to send in url as query params
 */
export async function restClient<T>(
  method: string,
  url: string,
  body?: string,
  userContext?: UserContext,
  params?: string
): Promise<T> {
  try {
    const headers: ObjectPropByName = {
      'content-type': 'application/json',
      // TODO: do we need 'Access-Control-Allow-Origin' here?
    };

    if (userContext?.sessionToken) {
      headers['mmk-auth'] = userContext.sessionToken;
    }

    const response = await fetch(`${BASE_URL}${url}${params ? `?${params}` : ''}`, {
      body: body, // TODO: Maybe we also need to support {} and FormData
      headers,
      method: method.toUpperCase(),
      mode: 'cors',
      // credentials: 'include', // TODO: Commenting this line allows to login from http://localhost:3000/ we need to solve the issue correctly
    });

    if (response.status === 401) {
      // If user is unauthorized redirect to Login page
      redirectToAuth();
    }

    if (response.status >= 200 && response.status < 400) {
      const resData: ApiResponse = await response.json();
      if (resData.error) {
        throw new Error(resData.message); // TODO: Make own Exception class for API errors
      }
      return resData.data as T;
    }

    // Response code is unsupported, so throw the error
    throw new Error('Unsupported response code'); // TODO: Make own Exception class for API errors
  } catch (error) {
    throw error; // TODO: maybe we need just console.error(error) here
  }
}

export default restClient;
