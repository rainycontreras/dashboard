import { restClient } from './apiClient';
import { LoanInterestTenure, LoanLastPayment, UserContext } from 'src/utils/type';

/**
 * Returns loan's emi amount
 * @param {number} principal - Loan's principal
 * @param {number} rateOfInterest - Loan's interest rate
 * @param {string} tenure - Loan's tenure
 * @param {UserContext} userContext - Customer's session data
 * @returns {number | undefined}
 */
export async function apiLoanEmiAmount(
  principal: number,
  rateOfInterest: number,
  tenure: number,
  userContext: UserContext
): Promise<number | undefined> {
  const ENDPOINT = 'budget/api/0/profile/savings/emiAmount';
  const params = new URLSearchParams({
    principal: principal.toFixed(0),
    rateOfInterest: rateOfInterest.toFixed(0),
    tenure: tenure.toFixed(0),
  }).toString();
  const response = await restClient<number>('GET', ENDPOINT, undefined, userContext, params);
  return response;
}

/**
 * Returns loan's interest rate & tenure
 * @param {number} principal - Loan's principal
 * @param {number} rateOfInterest - Loan's rate of interest
 * @param {number} emi - Loan's emi
 * @param {UserContext} userContext - Customer's session data
 * @returns {LoanInterestTenure | undefined}
 */
export async function apiLoanInterestAndTenure(
  principal: number,
  rateOfInterest: number,
  emi: number,
  userContext: UserContext
): Promise<LoanInterestTenure | undefined> {
  const ENDPOINT = 'budget/api/0/profile/savings/interestAndTenure';
  const params = new URLSearchParams({
    principal: principal.toFixed(0),
    rateOfInterest: rateOfInterest.toFixed(0),
    emiAmount: emi.toFixed(0),
  }).toString();
  const response = await restClient<LoanInterestTenure>('GET', ENDPOINT, undefined, userContext, params);
  return response;
}

/**
 * Returns customer's last payment
 * @param {UserContext} userContext - Customer's session data
 * @returns {LoanLastPayment | undefined}
 */
export async function apiLoanLastPayment(userContext: UserContext): Promise<LoanLastPayment | undefined> {
  const ENDPOINT = `budget/api/0/debtinterest/' + ${userContext.userId}`;
  const response = await restClient<LoanLastPayment>('GET', ENDPOINT, undefined, userContext);
  return response;
}
