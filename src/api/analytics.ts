import { LoanRecommendations, UserContext } from '@/utils/type';
import { restClient } from '.';

/**
 * Get home loan's recommendations
 * @param {UserContext} userContext - Customer's session data
 * @returns
 */
//TODO: Test this function, backend returns error 500
export async function homeLoanRecommendations(userContext: UserContext): Promise<LoanRecommendations | undefined> {
  const ENDPOINT = `budget/api/0/profile/getHomeLoanRecommendations`;
  const responseData = await restClient<LoanRecommendations>('GET', ENDPOINT, undefined, userContext);
  return responseData;
}
