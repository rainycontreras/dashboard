/**
 * Redirects user to the Login/Authentication page
 */
export function redirectToAuth() {
  const URL = '/?autologout=true'; // TODO: Make this configurable

  if (typeof window !== 'undefined') {
    window.location.href = URL;
  }
}
