import { restClient } from './apiClient';
import { CreditHistoryReport, CustomerProfile, ResponseData, UserContext } from 'src/utils/type';
import { CREDITREPORT_CURRENT } from '@/utils/account';

/**
 * Returns information about currently Logged User for API
 * @param {UserContext} userContext - current user session data
 * @returns {CustomerProfile|undefined} user profile as an object or undefined if user is not logged in
 */
// TODO: naming as fetchXxxYYY is not the best, we need to name function depending on our UI needs, not endpoint names. For example: api.info.me() is more clear
export async function fetchCustomerInfo(userContext: UserContext): Promise<CustomerProfile | undefined> {
  const ENDPOINT = 'budget/api/0/profile/customerInfo';
  const responseData = await restClient<ResponseData<CustomerProfile>>('GET', ENDPOINT, undefined, userContext);
  return responseData.data; // TODO: isn it just responseData?
}

/**
 * Fetch credit score History
 * @param {UserContext} userContext - Customer's session data
 * @param {number} reportIndex - Credit report's index to look for
 * @returns
 */
// TODO: Naming like api.info.creditScoreList() or api.info.creditScoreHistory() will be clear and shows that we perform some external HTTP call
export async function fetchCreditScoreHistory(
  userContext: UserContext,
  reportIndex: number = CREDITREPORT_CURRENT
): Promise<CreditHistoryReport | undefined> {
  const ENDPOINT = `budget/api/0/profile/creditScore/history?n=${reportIndex}`;
  // TODO: use cache
  const responseData = await restClient<CreditHistoryReport[]>('GET', ENDPOINT, undefined, userContext);
  return responseData[0];
}
