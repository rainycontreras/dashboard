/**
 * Custom, non MUI, colors space aka "App Color Styles"
 */
export type AppColorStyles = {
  primary: string;
  secondary: string;
  tertiary: string;
  grey: string;
  border: string;
  green: string;
  white: string;
  lightGrey: string;
  lightBlue: string;
  blueBorder: string;
  red: string;
  antiflashWhite: string;
  lightPeriwinkle: string;
  aliceBlue: string;
  ligthAliceBlue: string;
  marigold: string;
  crayola: string;
  lightCarminePink: string;
  androidGreen: string;
  maize: string;
  maizeScore: string;
  brightGray: string;
  oldLace: string;
  gamboge: string;
  greenCrayola: string;
  buttonBlue: string;
  ghostWhite: string;

  // Indicator Colors
  colorBar: {
    veryGood: string;
    good: string;
    neutral: string;
    bad: string;
    veryBad: string;
  };
};

/**
 * Application specific "App Color Styles" form Figma design
 * Figma at https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design
 */
export const APP_COLOR_STYLES: AppColorStyles = {
  primary: '#5562EB', // "Primary" in Figma
  secondary: '#1D2145', // "Secondary" in Figma
  tertiary: '#F6C765', // "Secondary" in Figma
  grey: '#676875', // "Grey" in Figma TODO: Warning, there is standard color with same name
  border: '#E8E9ED', // LOL, it is real "Border" color name in Figma :)
  green: '#3ECE80', // "Green" in Figma TODO: Warning, there is standard color with same name
  white: '#FFFFFF', // "White" in Figma TODO: Warning, there is standard color with same name
  lightGrey: '#F5F7FC', // "Light Grey" in Figma TODO: Warning, there is standard color with same name
  lightBlue: '#F0F5FF', // "Light Blue" in Figma TODO: Warning, there is standard color with same name
  blueBorder: '#D5E0F5', // LOL, it is real "Border Blue" color name in Figma :)
  red: '#E66262', // "Red" in Figma TODO: Warning, there is standard color with same name
  antiflashWhite: '#F0F0F1',
  lightPeriwinkle: '#C5CCDB',
  aliceBlue: '#EFF2FF',
  ligthAliceBlue: '#EFF2FF99',
  marigold: '#ECA336',
  crayola: '#F3AE1F',
  greenCrayola: '#1EBE67',
  lightCarminePink: '#E76262',
  androidGreen: '#AFC949',
  maize: '#EBC346',
  maizeScore: '#F9CF4A',
  brightGray: '#EDEEF2',
  oldLace: '#FFF5E5',
  gamboge: '#E88F0A',
  buttonBlue: '#1DA1F2',
  ghostWhite: '#F5F7FC',

  // Indicator Colors
  colorBar: {
    veryGood: '#3ECE80',
    good: '#AFC949',
    neutral: '#EBC346',
    bad: '#ECA336',
    veryBad: '#E66262',
  },
};
