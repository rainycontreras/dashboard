import { ThemeOptions } from '@mui/material/styles';
import { TypographyStyle } from '@mui/material/styles/createTypography';
import { AppColorStyles, APP_COLOR_STYLES } from './AppColorStyles';
import { AppTypographyVariants, APP_TYPOGRAPHY_VARIANTS } from './AppTypographyVariants';

/**
 * Add custom variables into MUI Theme for TypeScript
 * How to extending theme, see https://mui.com/customization/theming/#custom-variables
 * Adding & disabling variants, see https://mui.com/customization/typography/#adding-amp-disabling-variants
 */

declare module '@mui/material/styles' {
  interface Theme {
    app: AppColorStyles;
  }
  // allow configuration using `createTheme`
  interface ThemeOptions {
    app?: AppColorStyles;
  }

  interface PaletteOptions {
    app?: AppColorStyles;
  }

  interface TypographyVariants extends AppTypographyVariants {}

  interface TypographyVariantsOptions {
    h1?: TypographyStyle; // MUI also has this
    h2?: TypographyStyle; // MUI also has this
    body?: TypographyStyle;
    label1?: TypographyStyle;
    label2?: TypographyStyle;
    tiny?: TypographyStyle;
    text?: TypographyStyle;
    caption?: TypographyStyle; // MUI also has this
  }
}

// Update the Typography's variant prop options
declare module '@mui/material/Typography' {
  interface TypographyPropsVariantOverrides {
    // Enable Application's custom typography variants
    h1: true;
    h2: true;
    body: true;
    label1: true;
    label2: true;
    tiny: true;
    text: true;
    caption: true;
    // Turn off the default MUI variants
    h3: false;
    h4: false;
    h5: false;
    h6: false;
    subtitle1: false;
    subtitle2: false;
    body1: false;
    body2: false;
    button: false;
    overline: false;

    // "Old custom" variants
    // @deprecated
    tiny2: true;
    fieldValue: true;
    link: true;
    captionMenu: true;
    menu: true;
  }
}

/**
 * Font family form Figma
 */
const APP_FONT = ['IBM Plex Sans', 'sans-serif'].join(',');

/**
 * MUI theme colors are different for Light and Dark modes
 * Material Color Tool: https://material.io/resources/color/#!/?view.left=0&view.right=0&secondary.color=EF9A9A&primary.color=64B5F6
 */
const LIGHT_MODE_COLORS /* : AppThemePalette */ = {
  // common: CommonColors;
  // mode: PaletteMode;
  // contrastThreshold: number;
  // tonalOffset: PaletteTonalOffset;
  primary: {
    main: APP_COLOR_STYLES.primary,
    contrastText: '#FFFFFF',
  },
  secondary: {
    main: APP_COLOR_STYLES.secondary,
    contrastText: '#000000',
  },
  // error: PaletteColor;
  // warning: PaletteColor;
  // info: PaletteColor;
  // success: PaletteColor;
  // grey: Color;
  // text: TypeText;
  // divider: TypeDivider;
  // action: TypeAction;
  // background: TypeBackground;

  // "App Color Styles"
  app: APP_COLOR_STYLES,
};

const DARK_MODE_COLORS /* : AppThemePalette */ = {
  // common: CommonColors;
  // mode: PaletteMode;
  // contrastThreshold: number;
  // tonalOffset: PaletteTonalOffset;
  primary: {
    main: APP_COLOR_STYLES.primary,
    contrastText: '#000000', // TODO: should be white?
  },
  secondary: {
    main: APP_COLOR_STYLES.secondary,
    contrastText: '#000000', // TODO: should be white?
  },
  // error: PaletteColor;
  // warning: PaletteColor;
  // info: PaletteColor;
  // success: PaletteColor;
  // grey: Color;
  // text: TypeText;
  // divider: TypeDivider;
  // action: TypeAction;
  // background: TypeBackground;

  // "App Color Styles"
  app: APP_COLOR_STYLES,
};

/**
 * MUI theme config for "Light Mode"
 */
const LIGHT_THEME: ThemeOptions = {
  palette: {
    mode: 'light',
    ...LIGHT_MODE_COLORS,
  },
  typography: {
    fontFamily: APP_FONT,
    ...APP_TYPOGRAPHY_VARIANTS,
  },
};

/**
 * MUI theme config for "Dark Mode"
 */
const DARK_THEME: ThemeOptions = {
  palette: {
    mode: 'dark',
    ...DARK_MODE_COLORS,
  },
  typography: {
    fontFamily: APP_FONT,
    ...APP_TYPOGRAPHY_VARIANTS,
  },
};

export { LIGHT_THEME as default, LIGHT_THEME, DARK_THEME };
