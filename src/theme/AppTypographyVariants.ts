import { TypographyStyle } from '@mui/material/styles';
import { APP_COLOR_STYLES } from './AppColorStyles';

/**
 * Custom, non MUI, typography space aka "App Text Styles"
 */
export type AppTypographyVariants = {
  h1: TypographyStyle; // MUI also has this
  h2: TypographyStyle; // MUI also has this
  body: TypographyStyle;
  label1: TypographyStyle;
  label2: TypographyStyle;
  tiny: TypographyStyle;
  text: TypographyStyle;
  caption: TypographyStyle; // MUI also has this
};

/**
 * Application specific "Text Styles" form Figma design
 * Figma at https://www.figma.com/file/2mbmPDrUghamODj5mpcNAK/MMK-%2F-Product-Design
 */
export const APP_TYPOGRAPHY_VARIANTS: AppTypographyVariants = {
  h1: {
    // font-style: normal;
    // font-weight: 600;
    // font-size: 24px;
    // line-height: 31px;
    // color: #000000;
    color: APP_COLOR_STYLES.secondary, // TODO: Actually it should be some of .contrastText colors
    fontSize: '1.5rem',
    fontWeight: 600,
    lineHeight: `1.9375rem`, // TODO: Do we need this?
  },
  h2: {
    // font-style: normal;
    // font-weight: 600;
    // font-size: 18px;
    // line-height: 23px;
    // color: #000000;
    color: APP_COLOR_STYLES.secondary, // TODO: Actually it should be some of .contrastText colors
    fontSize: '1.125rem',
    fontWeight: 600,
    lineHeight: '1.4625rem', // TODO: Do we need this?
  },
  body: {
    // font-style: normal;
    // font-weight: normal;
    // font-size: 15px;
    // line-height: 22px;
    // color: #000000;
    color: APP_COLOR_STYLES.secondary, // TODO: Actually it should be some of .contrastText colors
    fontSize: '0.9375rem',
    fontWeight: 400,
    lineHeight: '1.375rem', // TODO: Do we need this?
  },
  label1: {
    // font-size: 15px;
    // line-height: 19px;
    // font-style: normal;
    // color: #000000;
    // Regular:
    // font-weight: normal;
    // SemiBold:
    // font-weight: 600;
    color: APP_COLOR_STYLES.secondary, // TODO: Actually it should be some of .contrastText colors
    fontSize: '0.9375rem',
    fontWeight: '600',
    lineHeight: '1.188rem', // TODO: Do we need this?
  },
  label2: {
    // font-style: normal;
    // font-size: 14px;
    // line-height: 18px;
    // color: #000000;
    // Regular:
    // font-weight: normal;
    // Medium:
    // font-weight: 500;
    // Semi Bold:
    // font-weight: 600;
    color: APP_COLOR_STYLES.secondary, // TODO: Actually it should be some of .contrastText colors
    fontSize: '0.875rem',
    fontWeight: '500',
    lineHeight: '1.125rem', // TODO: Do we need this?
  },
  tiny: {
    // font-style: normal;
    // font-weight: normal;
    // font-size: 12px;
    // line-height: 16px;
    // color: #000000;
    color: APP_COLOR_STYLES.secondary, // TODO: Actually it should be some of .contrastText colors
    fontSize: '0.75rem',
    fontWeight: 'normal',
    lineHeight: '1rem', // TODO: Do we need this?
  },
  text: {
    // font-style: normal;
    // font-size: 16px;
    // line-height: 21px;
    // color: #000000;
    // Regular:
    // font-weight: normal;
    // Semi Bold:
    // font-weight: 600;
    color: APP_COLOR_STYLES.secondary, // TODO: Actually it should be some of .contrastText colors
    fontSize: '1rem',
    fontWeight: 'normal', // '600',
    lineHeight: '1.3125rem', // TODO: Do we need this?
  },
  caption: {
    // font-style: normal;
    // font-size: 13px;
    // line-height: 20px;
    // color: #000000;
    // Regular:
    // font-weight: normal;
    // Semi Bold:
    // font-weight: 600;
    color: APP_COLOR_STYLES.secondary, // TODO: Actually it should be some of .contrastText colors
    fontSize: '0.8125rem',
    fontWeight: 'normal', // '600',
    lineHeight: '1.25rem', // TODO: Do we need this?
  },
};
