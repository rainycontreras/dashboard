My Money Karma dashboard app

## Getting Started

First, run the development server:

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Build production container

From root folder exec:

docker build -t <<TAG>> ./

Build enabled arguments:

- dashboard_basepath: Path for dashboard app. => http://hostname:port/basepath/dashboard
- api_baseurl: Base url for budget app, must ends with /, default value: /
- port: Port exposed by container, default value: 3000

## Folder structure

- /pages: public pages, navigation enabled
- /src/api: Functions to call api endpoints
- /src/components: React components without navigation
- /src/hooks: Custom hooks for ui logic
- /src/store: Global state management
- /src/utils: Common functions
